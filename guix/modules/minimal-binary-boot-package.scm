; based on https://guix.gnu.org/de/blog/2023/from-development-environments-to-continuous-integrationthe-ultimate-guide-to-software-development-with-guix/
(define-module (minimal-binary-boot-package)
  #:use-module (guix)
  #:use-module (guix git-download)   ;for ‘git-predicate’
  …)

(define-public minimal-binary-boot
  (package
    (name "minimal-binary-boot")
    (version "3.0.99-git")                          ;funky version number
    …))

;; Return the package object define above at the end of the module.
minimal-binary-boot
