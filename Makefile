# todo : put outputs into build subdir

all: seeds

.PHONY: clean clean-seed run-x86-linux
.PRECIOUS: build/arch-x86-linux/hexPloader1.bin
.PRECIOUS: build/arch-x86-linux/cipf-hexP.bin

srcDir   = ./src
buildDir = ./build
seedDir  = ./seed

clean:
	rm -rf $(buildDir)

clean-seed: clean
	rm -rf $(seedDir)

src_x86     = $(srcDir)/arch-x86
build_x86   = $(buildDir)/arch-x86
buildTools  = $(buildDir)/tools
seed_x86    = $(seedDir)/arch-x86
seed_x86linux    = $(seedDir)/arch-x86-linux
src_x86linux     = $(srcDir)/arch-x86-linux
build_x86linux   = $(buildDir)/arch-x86-linux

seeds: Makefile seed-x86-linux seed-x86-bios

seed-x86-bios: $(seed_x86)/monitor-octal-keyboard.s16
seed-x86-bios: $(seed_x86)/monitor-octet-keyboard.s16
seed-x86-bios: $(seed_x86)/monitor-octal-serial.s16
seed-x86-bios: $(seed_x86)/monitor-octal-serialInit.s16
seed-x86-bios: $(seed_x86)/cipf.m16.rev-octal
seed-x86-bios: $(seed_x86)/cipf.m16.rev-octet
seed-x86-bios: $(seed_x86)/cipf.s16

seed-x86-linux: $(seed_x86)/monitor-octal.s32
seed-x86-linux: $(seed_x86)/cipf.m32.rev-octal
seed-x86-linux: $(seed_x86)/cipf.s32
seed-x86-linux: $(seed_x86)/cipf.elf
seed-x86-linux: $(seed_x86linux)/hexPloader0.elf

$(seedDir)/%: $(buildDir)/% Makefile
	mkdir -p $(@D)
	cp $< $@

#########################################################################################

$(build_x86)/%.m16: $(src_x86)/%.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D bios16=1 -D monitorBinary=1 -f bin -o $@ -l $@.lst $< -m x86
	yasm  -Worphan-labels -D bios16=1 -D monitorBinary=1 -f elf -g dwarf2 -o $@-dbg $< -m x86

$(build_x86)/%.s16: $(src_x86)/%.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D bios16=1 -U monitorBinary $(FDEFS) -f bin -o $@ -l $@.lst $< -m x86
	yasm  -Worphan-labels -D bios16=1 -U monitorBinary $(FDEFS) -f elf -g dwarf2 -o $@-dbg $< -m x86

# StartTextLinuxAfterMonitor = 0x08048091
StartT = 0x08048091
$(build_x86)/%.m32: $(src_x86)/%.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D linux32=1 -D monitorBinary=1 -f elf -g dwarf2 -o $@-dbg  $< -l $@.lst -m x86
	ld -m elf_i386 --omagic --no-dynamic-linker -static -o $@-bin -Ttext $(StartT) $@-dbg
	objcopy -v --only-section=.text --output-target binary  --image-base $(StartT) $@-bin $@
# objdump -b binary -m i386 -hD build/arch-x86/1cf.m32 | less
# hexdump -C build/arch-x86/1cf.m32
# gdb: break 62; condition 2 $edi == 0x8048099; set *(char)0x8048091 = 0x00

# LEN=$$(wc -c  < build/arch-x86-linux/monitor-octal-x86.mb); BASE=$$(printf "0x%x" $$(( 0x8048090 + $$LEN - 1)) # 0x8048060 + sizeof monitor - 1

$(build_x86)/%.s32: $(src_x86)/%.asm Makefile $(buildTools)/set-elf-text-writeable
	mkdir -p $(@D)
	yasm -Worphan-labels -D peekHexCodes=1 -D linux32=1 -U monitorBinary -f elf -g dwarf2 -o $@.o -l $@.lst $< -m x86
	ld --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $@.tmp $@.o
	objcopy --writable-text --set-section-flags .text=CONTENTS,ALLOC,LOAD,CODE $@.tmp $@.tmp2
	./$(buildTools)/set-elf-text-writeable $@.tmp2
	mv $@.tmp2 $@
	rm $@.tmp
	:
	yasm -Worphan-labels -D peekHexCodes=1 -D linux32=1 -U monitorBinary -f elf -o $@.o -l $@.lst $< -m x86
	ld -s -O 9 --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $@.tmp $@.o
	objcopy --writable-text --set-section-flags .text=CONTENTS,ALLOC,LOAD,CODE $@.tmp $@.tmp2
	./$(buildTools)/set-elf-text-writeable $@.tmp2
	mv $@.tmp2 $@-strip
	strip $@-strip
	rm $@.tmp
# objcopy --writable-text does not work for elf ...
	mkdir -p $(@D)
	yasm -Worphan-labels -D peekHexCodes=1 -D linux32=1 -U monitorBinary -f elf -g dwarf2 -o $@.o -l $@.lst $< -m x86

$(build_x86)/%.elf: $(src_x86)/%.asm Makefile
	mkdir -p $(@D)
	yasm -Worphan-labels -D elf=1 -f bin -m x86 -o $@ -l $@.lst $<
	chmod +x $@

$(build_x86)/%.bin: $(src_x86)/%.asm Makefile
	mkdir -p $(@D)
	yasm -Worphan-labels -f bin -m x86 -o $@ -l $@.lst $<

$(build_x86linux)/%.bin: $(src_x86linux)/%.asm $(src_x86linux)/baseAdr.asm Makefile
	mkdir -p $(@D)
	yasm -Worphan-labels -f bin -m x86 -o $@ -l $@.lst $<

# objdump -D build/arch-x86-linux/hexPloader1.bin -m i386 -b binary --adjust-vma=0x14bf1014
# objdump -D build/arch-x86-linux/cipf-hexP.bin -m i386 -b binary --adjust-vma=0xdbf014e --start-address=0xdbf0164

$(build_x86)/%.hexP: $(build_x86)/%.bin Makefile
	mkdir -p $(@D)
	python src/encode16.py 512 < $< > $@

$(build_x86linux)/%.hexP: $(build_x86linux)/%.bin Makefile
	mkdir -p $(@D)
	python src/encode16.py 512 < $< > $@


# the bag of monitors:
$(build_x86)/monitor-octal.s32: $(src_x86)/monitor-octal.asm Makefile $(buildTools)/set-elf-text-writeable
	mkdir -p $(@D)
	yasm -Worphan-labels -D linux32=1 -f elf -g dwarf2 -o $@.o -l $@.lst $< -m x86
	ld --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $@.tmp $@.o
	objcopy --writable-text --set-section-flags .text=CONTENTS,ALLOC,LOAD,CODE $@.tmp $@.tmp2
	./$(buildTools)/set-elf-text-writeable $@.tmp2
	mv $@.tmp2 $@
	rm $@.tmp
# objcopy --writable-text does not work for elf ...

%-keyboard.s16: DEFS= -D keyboard=1
%-serial.s16: DEFS= -D serial=1 -U serialInit -U serialErrorCheck
%-serialInit.s16: DEFS= -D serial=1 -D serialInit=1 -U serialErrorCheck
%-serialCheck.s16: DEFS= -D serial=1 -D serialInit=1 -D serialErrorCheck=1
%-serial-boot.s16: DEFS= -D serial=1 -U serialInit -U serialErrorCheck -D bootSector=1


$(build_x86)/monitor-octal%.s16: $(src_x86)/monitor-octal.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D bios16=1 $(DEFS) $(FDEFS) -f bin -o $@ -l $@.lst $< -m x86
	yasm  -Worphan-labels -D bios16=1 $(DEFS) $(FDEFS) -f elf -g dwarf2 -o $@-dbg $< -m x86

$(build_x86)/monitor-octet%.s16: $(src_x86)/monitor-octet.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D bios16=1 $(DEFS)  -f bin -o $@ -l $@.lst $< -m x86
	yasm  -Worphan-labels -D bios16=1 $(DEFS) -f elf -g dwarf2 -o $@-dbg $< -m x86

#########################################################################################

$(buildTools)/set-elf-text-writeable: tools/set-elf-text-writeable.c Makefile
	mkdir -p $(@D)
	gcc -o $@ $<

$(buildDir)/%.fw: $(srcDir)/%.fs Makefile
	mkdir -p $(@D)
	sed -e 'x;/^$$/b start;/^koan$$/b koan;/^strip/b strip;/^WS$$/b WS;/^LC$$/b LC; : start;s/^/koan/; : koan;x;/^\\ end of koan$$/b k2s;d; : k2s;s/^.*$$/strip/;x;d; : strip;x;/^\\ ONE-NEWLINE$$/b 1NL;/^\\ ONE-SPACE$$/b 1SP;/^\\ WHITE-SPACE$$/b S2WS;s/^\\ //;s/\\ [^\n]*$$//g;s/ *//g;H;d; : 1NL;x;s/^strip\|[\n]*//g;p;s/^.*$$/strip/;x;d; : 1SP;s/^.*$$/ /;H;d; : S2WS;s/^.*$$/WS/;x;s/^strip\|[\n]*//g;p;d; : WS;x;/^\\ LINE-COMMENT$$/b W2L;s/^\\ //;s/\\ .*$$//g;s/ *$$//;/^$$/d;p;d; : W2L;s/^.*$$/LC/;x;d; : LC;x;p;d' $< > $@.tmp

	mv $@.tmp $@

# old sed -e 's/\\ .*$$//' -e 's/( [^)]*)//g' -e '/^ *$$/d' -e 's/  */ /g' -e 's/  *$$//' -e 'w$@' $< > /dev/null

$(buildDir)/%.rev-octal: $(buildDir)/%  Makefile ./tools/reverse-octal.py
	mkdir -p $(@D)
	python -u ./tools/reverse-octal.py 512 < $< > $@.tmp
	mv $@.tmp $@

$(buildDir)/%.rev-octet: $(buildDir)/% Makefile ./tools/reverse-octet.py
	mkdir -p $(@D)
	python -u ./tools/reverse-octet.py 512 < $< > $@.tmp
	mv $@.tmp $@

.PHONY: run-x86-linux run-x86-linux-monitor test-x86-linux qemu-2wf

run-x86-linux: $(seed_x86)/cipf2mbbf.fw seed-x86-linux Makefile
	cat $< $(src_x86)/cipf-init1.fs - | $(seed_x86)/cipf.s32

test-x86-linux: $(seed_x86)/cipf2mbbf.fw seed-x86-linux Makefile
	cat $< $(src_x86)/cipf2mbbf.fs - | $(seed_x86)/cipf.s32

run-x86-linux-monitor: $(seed_x86)/cipf2mbbf.fw seed-x86-linux Makefile
	cat $(seed_x86)/cipf.m32.rev-octal $< - | $(seed_x86)/monitor-octal-x86-linux.s32

.PHONY: qemu-cipf% qemu-octal%

QFLAGS =
%-debug: QFLAGS += -monitor vc -s -S
QSFLAGS = -chardev socket,id=s0,path=$(buildDir)/socket-io -serial chardev=s0
# How can we feed files as console input or serial input to qemu?
# TODO try:
# -curses
# -no-fd-bootchk (drop 0x55, 0xAA)
# -chardev vc,id=m0 -mon chardev=m0
# -chardev socket,id=s0,path=./serial-io
# -serial chardev=s0
# -nographic -chardev vc,id=m0 -mon chardev=m0
qemu-cipf%: $(seed_x86)/cipf-boot.s16 Makefile
	qemu-system-i386 -drive format=raw,file="$<" $(QFLAGS)

%.img: %.s16 Makefile
	dd if=$< of=$@-bootsector bs=512
	dd if=/dev/zero of=$@-zeros bs=512 count=511
	cat $@-bootsector $@-zeros > $@
	rm -f $@-bootsector $@-zeros

qemu-octal-serial%: $(build_x86)/monitor-octal-serial-boot.img Makefile
	qemu-system-i386 -drive format=raw,file="$(build_x86)/monitor-octal-serial-boot.img" $(QFLAGS) $(QSFLAGS) & echo pid of qemu is: "$$!"
	@echo connect gdb with: "target remote localhost:1234"
	@echo set architecture i8086
	cat $(seed_x86)/cipf.m16.rev-octal - | nc -lU $(buildDir)/socket-io
	kill $$!

qemu-octal%: $(seed_x86)/monitor-octal-keyboard-boot.s16 Makefile
	qemu-system-i386 -drive format=raw,file="$<" $(QFLAGS)

qemu-octet%: $(seed_x86)/monitor-octet-keyboard-boot.s16 Makefile
	qemu-system-i386 -drive format=raw,file="$<" $(QFLAGS)

$(build_x86linux)/%.s32: $(src_x86linux)/%.asm Makefile $(buildTools)/set-elf-text-writeable
	mkdir -p $(@D)
	yasm -Worphan-labels -D peekHexCodes=1 -D linux32=1 -U monitorBinary -f elf -g dwarf2 -o $@.o -l $@.lst $< -m x86
	ld --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $@.tmp $@.o
	objcopy --writable-text --set-section-flags .text=CONTENTS,ALLOC,LOAD,CODE $@.tmp $@.tmp2
	./$(buildTools)/set-elf-text-writeable $@.tmp2
	mv $@.tmp2 $@
	rm $@.tmp
	:
	yasm -Worphan-labels -D peekHexCodes=1 -D linux32=1 -U monitorBinary -f elf -o $@.o -l $@.lst $< -m x86
	ld -s -O 9 --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $@.tmp $@.o
	objcopy --writable-text --set-section-flags .text=CONTENTS,ALLOC,LOAD,CODE $@.tmp $@.tmp2
	./$(buildTools)/set-elf-text-writeable $@.tmp2
	mv $@.tmp2 $@-strip
	strip $@-strip
	rm $@.tmp
# objcopy --writable-text does not work for elf ...
	mkdir -p $(@D)
	yasm -Worphan-labels -D peekHexCodes=1 -D linux32=1 -U monitorBinary -f elf -g dwarf2 -o $@.o -l $@.lst $< -m x86

$(build_x86linux)/%.elf: $(src_x86linux)/%.asm $(src_x86linux)/baseAdr.asm Makefile
	mkdir -p $(@D)
	yasm -Worphan-labels -D elf=1 -f bin -m x86 -o $@ -l $@.lst $<
	chmod +x $@

build/arch-x86-linux/base2f.mix: build/arch-x86-linux/hexPloader1.hexP  \
		build/arch-x86-linux/cipf-hexP.hexP src/arch-x86-linux/cipf2f32.fs
	mkdir -p $(@D)
	( cat build/arch-x86-linux/hexPloader1.hexP \
          printf B ;  \
	  cat build/arch-x86-linux/cipf-hexP.hexP ; \
          printf Z ; \
          cat src/arch-x86-linux/cipf2f32.fs ) > $@
