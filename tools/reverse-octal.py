# Copyright (C) 2021 Dr. Stefan Karrmann
# Distribution and usage licenced under GPL-3.
import os, sys
size = int(sys.argv[1])
buffer = os.read(sys.stdin.fileno(), size)
for i in range(len(buffer), size): # pad end with zeros
    sys.stdout.write("000")
for i in range(len(buffer) - 1, -1, -1):
    os.write(sys.stdout.fileno(), format(ord(buffer[i]), '03o'))
