# 0w4tf - zero words, four token forth

All we need to assemble new forth words is a creating word `c` and `0 1+
2* ! @ psp@ -` and some addresses of variables, i.e. `here state
latest`. We provide the latter on the initial stack. In fact we can
reduce this further, since

    : dup psp@ @ ;
    : 0 psp@ dup - ;
    : cell psp@ psp@ - ;
    : cell+ 0 cell - - ;
    : over psp@ cell+ @ ;
    : 2* 0 over - - ;
    : 1+ <pick -1> - ;

Of course, we need -1 additionally on the initial stack.

As long as we don't have `: ; code` etc. we have to spell out these
definitions every time. That's some hard work, but with every new word
it gets easier and easier.

To get a new byte onto the stack we do, e.g.:

    0 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ \ 0xff

Well, that's our initial way to write numbers using binary digits in
our preForth source code.

We provide most of the basic words as execution tokens (xt in Forth lingo)
on the initial stack. As we want to define words, we need an initial
word in our preForth. As we only need one word, we start with an empty
dictionary and default to the one creating word. That saves us the
dictionary header and turns the 'not-found-code' to an essential part
of our bootstrapping.

On more trick is to be able to compile the default word into new
words. By this we reuse the word parseer for `: code alias` etc.

Todo be continued...