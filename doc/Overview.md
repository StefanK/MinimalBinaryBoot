I'm not a forth programmer. In the 90s I first read about forth. Now,
in the 20s I've implemented my first tiny forth nucleus and monitors,
which can load octal code into memory.

# Is reverse octal code source code?

We can debate endlessly, if reverse octal code is really source code.
An alternative to reverse octal code is, to use 2wf or 0w4tf directly
as binary seed. Then the first input is forth source code without
comments. We will provide all these seeds.

Well, some accept hex0 as source code. It's hex numbers with
whitespace and line comments. The step to reverse octal code is small,
IMHO. We have to strip the comments, the whitespace. Then we convert
hex numbers to octal numbers. Finally, we reverse the byte sequence.

The conversion from reverse octal code to hex code is analoge. Both
conversions can easily be done manually, by pen and paper, or simple
scripts.

Moreover, there is no cpu which executes octal code
directly, AFAIK. Therfore, octal code is not binary machine code.

# About forth
IMHO, forth is really a special language. Its machine modell is simple,
so are its inner and outer interpreter. This allows us to write really
small forth interpreters.

On the other hand, forth is easily extendable, has control structures,
so that we can write complex programs and abstractions. It's a
fascinating mixture between low level and structured programming.

There are several reasons for this:
1. Forth syntax is really simple, i.e. words separated by white space.
2. Forth is interactive. I.e. it has an interpretation state and a
   compilation state and can switch between both everywhere.
3. The routines in each state are easily extendable in forth itself.

Therefore, we can start really small and extend the inital system by
forth code step by step.

# Design space
## General rambling

To read source code, we must be able to read one or more bytes. Then
we must interpret one or more bytes of input, i.e. recognize it and
do what it means. That's the minimal stage 1 interpreter of
planckforth, i.e. `: interpret BEGIN key find execute AGAIN ;` We can
even fuse this into one assembler function. NB, it uses one character
words only.

From another starting point, we can use unary numbers using big
numbers and store one number, which contains all bytes we need. Then,
we must plug this into the interpreter, i.e. update latest and
here. Thats two words `1 \ 1+  ( bignat -- bignat+1 )` and
 `u \ : update store-bignum here @ @ execute`.

The drawback is that a bignum library is not small, AFAIK. We can
factor the bignat into bytes with one more word, i.e. `, \ : c,0 ( c --
0 ) c, 0 ;`. We have to change the final update, i.e. `u \ : update
here @ cell - execute ;`

Another drawback is that unary numbers are rather unreadable. It's a
more or less long string of `1`. That's not really readable source
code.

Ignoring the drawbacks, we see that we can have a tiny byte
interpreter with only two (or three) words. Neither elegant nor byte
saving are:
- The update is only used once.
- We need dictionary entries for `1 , u`.
- Unary numbers are difficult to read.

Since binary numbers are much easier to read than unary numbers the
word `2 \ 2* ( n -- 2*n )` would increase readability of our source
code much.

Since `c,`

## Startup

At startup of our bootstrapping forth we don't need the compile mode,
as long as we have `word find , execute`. We can define the `state`
variable later and on top of it, we can write a stage 2 interpreter.
This saves us the variable and the code path for compiling.

## Default word

If we start with an empty dictionary, we must provide a default word
as a result of an unsuccessful find. The default word must set up
1. the link,
2. the word name (or word charachter, which can save us the length
   byte)
3. the cfa, which it takes from the initial stack

Therfore we can use indirect threaded code with an initial dictionary
layout as:

    +------+---+-----+-----+
    | link | c | cfa | pfa |
    +------+---+-----+-----+
     cell   byte cell  cells

This saves us the length byte and the code to compare it etc. It
further saves us the jump or call op-code for direct threaded code
(DTC/jump or call) or subroutine threaded code.

Later on, we can change our dictionary layout arbitrarily. If we are
careful, we can reuse some execution tokens or copy some code.

We can obtain `key` from the default word by wrapping it into a new
word: `: key here @ dup default here @ cell- 1- c@ swap here ! ;`

## `@ ! d -` and default

With these fore words (`d` for data stack pointer) and the addresses
of `latest here` and the value `-1` on the initial stack, we can use
(and later define) all we need:
- `: dup  d @ ;`
- `: cell d d - ;`
- `: 0 d dup - ;`
- `: cell+ 0 cell - - ;`
- `pick` by `p cell+ ... @` provides us with copies of the values from
   the initial stack

NB:
- We cannot calculate 1 or -1 from `d @ ! -` alone. Therefore, we
  need it on our initial stack. (Well, maybe by chance somewhere in
  the binary code.)

# Minimal forth
2wf as well as 0w4tf fits easily in a boot sector of 512 octets (aka bytes).
Thus, they are small by themself but huge in comparison to the octal monitor.

Both forth read their input, which defines step by step the normal
forth words.

## 2wf rambling and roadmap

2wf is a really small forth interpreter. It uses direct threaded code
(DTC), common tail code sharing, and locations in the op-codes to
store some variables. Well, it's only a maybe minimal forth boot
nucleus or a kind of preForth. But we can extend it by an input of
words (in forth lingo) and get a more and more normal forth
environment.

2wf (two word forth) has only two regular words, i.e.
1. | ( bar ) which behaves as if `: | : POSTPONE [ latest
   @ reveal ;` i.e. colon and lbrack without hiding the word it
   defines. In particular, it does not enter compilation mode but
   stays interpretating.
2. , ( comma ) which writes the top of stack into here and advances here.

Both are not immediate.

That's obviously not enough to create a full forth interpreter on its own.
Therfore, we have some execution tokens (xt) on the initial stack.
First of all, we have [;] , which is a stripped down ; , i.e. it compiles exit.
But [;] does not unhide the word, it does not switch to
interpretation mode, and it is not (yet) immediate.

NB: We define [;] by `| [;] , [;]` i.e. we use it, while it is not yet
completly defined. In other words, it completes its own definition
while avoiding an endless recursive loop.

The next xt are "ordinary" actions `@ ! - nand sp@`. We need them to read and set
memory storage, to get the cell size, 0 and 1.
This set is enough to set the immediate flag for [;] and to compile
([) and set it immediate and compile ([) ; in this sequence.
- We need an immediate [;] to complete the compilation of ([) .
    - We cannot compile other words by `,` yet, as we cannot get the xt
      of an input word onto the parameter stack. We don't have ' yet.
- As soon as ([) is fully defined, but not earlier, we can leave
  compilation state.
- (]) is just a convenient abbreviation to enter compilation state.

Both, ([) and (]) , need the address of the variable state on the
stack. To set words immediate we need the address of the variable
latest on the stack.

The next xt on the inital stack are `rp@ key emit ?exit`. We need them to define
`lit >r r> tail dup? =0 branch ?branch execute`.
On this base we can define: `latest state immediate hide reveal [ ] : ;`
Now, we can define the control structures and loops,
e.g. `if then else begin again until while repeat case` etc.pp.

Up to this, we need to strip our forth comments by hand or by e.g. sed.
With these control structures and the word key we define the comment
words, i.e. \ and a nested ( , at the start of 2wf-init1.fs

We still have no usual outer interpreter. E.g., it does not interpret
or compile numbers. But now we can build
1. our own outer interpreter
2. a basic, but extendable, assembler in forth.

We use the assembler to redefine some words more efficiently.
That is not really necessary for bootstrapping, but it gives us a
faster forth interpreter.

## 0w4tf rambling and roadmap

While debugging `?exit` I noticed, that we must not put it into a
colon word. First, I solved this by adding the wordsize to the return
stack pointer (bp). Second, I rewrote it's colon definition into a
near jump in forth code. This avoids the additional bytes in the
binary. Then I take a break and during my jogging, I got an
enlightment. If I can write an jump op-code into here-space, then I
can write arbitrary machine codes into here-space. That's a kind of bit
assembler, as we have no number-input at start. All we need for this
is:
1. a defining word
2. `psp@`
3. `@`
4. `!`
5. ` -`
6. `-1`
7. here'sAddress
8. state'sAddress
9. latest'sAddress

We can provide the items 2 to 9 compactly on the inital stack. This
leaves us with 1w4tf (one word 4 execution tokens forth). A moment
later, I realised that we don't need the word header for the defining
word. We can jump to it by default, i.e. if the input word is not
found in the dictionary. So, we arrive at 0w4tf.

The defining default word take one execution token (xt) from the
parameter stack, one word from the input and defines a new dictonary
entry for it, which jumps to the xt. Notably, it does not enter
compile state and does not hide the new word.

NB: On x86 the jmp offset is relative. Therfore, we must precalculate
the xt, which we put into the inital stack.

We use `:codeJmpTo` as the undefined word, but we can use any other
word as long as it is undefined in our dictonary.

As long as we cannot define colon words, we have to spell everything
out using our four primitives. Here are some examples, if we had `: ;`:

    : dup   psp@ @ ;
    : cell  psp@ psp@ - ;
    : 0     psp@ dup - ;
    : -cell  0 cell - ;
    : cell+ -cell - ;
    : cell- cell - ;
    : drop  dup - - ;
    : over  psp@ cell - @ ;
    : 2*    0 over - - ;
    : 1+    ( get -1 from the inital stack ) - ;
    : swap over over psp@ cell+ cell+ cell+ ! psp@ cell+ ! ;

With `0 1+ 2*` we can calculate every byte. With `!` we can store it
and with `psp@ @` etc. we can push any value from our inital stack on
top. With this basic tools we can create new words (by `:codeJmpTo`)
and (re)write their machine code.

Obviously, we start with the longest and most used words, i.e.:
1. 2*
2. 1+
...


# Minimal forth rambling

Our question is:

> What are the primitives for a minimal forth?

IMHO there is no unique answer. There will be serveral small set which
may be minimal depending on the basic machine.

## Choice in 2wf

1. | a stripped down : ( colon )
2. ,
3. [;] s stripped down ; ( semi )
4. @
5. !
6. --
7. nand
8. sp@
9. rp@
10. key
11. emit
12. ?exit
13. state
14. latest
15. here

That's 12 primitives and 3 addresses of variables. I have found no way
to get rid of `?exit` without another primitive, e.g. `=0`. We need a
method to collapse all non-zero values to one representation, e.g. -1.

`?exit` is efficient as it fall through to either next or `exit`.

The stripped down colon, i.e. | , could factored into `parse-name create `
... I assume, that that will take more bytes in the binary code.

Other useful words are [ and ] , but we can define them using our
minimal set.

Another useful word is ' . Together with , we can compile words
interactivly. I.e. we don't need compile state. Moreover, `find-name`
is part of ' and of the interpreter. Therfore, we could share the
code. But no code is even shorter.

NB: If [ is immediate at start, all other immediate words are just
syntactic sugar. I.e. you can replace an immediate `FOO` always by
`[ foo ]' where `foo` is the non immediate word of `FOO`. Only
`POSTPONE` and friends would be more difficult, as we have to provide
the immediate flag in another way, e.g. as an parameter.

## Choice in 0w4tf
1. a defining word
2. psp@
3. @
4. !
5. `-`
6. -1
7. here'sAddress
8. state'sAddress
9. latest'sAddress

We use our bit assembler to define the code words:
1. `2*` needed up to 7 times for each byte
2. `1+` needed up to 7 times for each byte
3. `over` needed for each byte and more
4. `0` needed for each byte and more
5. `cell` needed to get here etc. from the stack
6. `next,` needed once for ever code word
7. `here` needed to write the machine code into here space
8. `state` needed to switch state
9. `latest` needet to hide, reveal words and set it immediate

Next, we want to define `:`
1. `lit`
2. `exit`
3. `docolon`
...
4. `c!` to set state
5. `immediate` ( nt -- )
6. `[` with `latest @ immediate`


### Tricks

As soon as we have an immediate `[` we have a poor man's `'` (tick) as:

    ] docolon [ here @ cell - dup @ swap here !

Our state variable is a byte. We need either ( `and` `or` ) or `c!` )
to change it. Then we can enter compile state by:

    0 state c!

or

    state @ \ get state with `cell 1-` additional bytes
    0 0 1+ - \ -1
    0 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ 2* 1+ \ 0xFF
    - and state !

But we must not do it before we have an immediate `[`!

We can transform our default word into `:` by:

    psp@ ( dummy ) :codeJumpTo :
      here @ cell - 0 1+ - \ point to CFA>
      call-op-code over !  \ 0xE8
      1+ here !            \ store new here
      ] docolon            \ compile docolon 's absolute address
      [ here @ cell - @ here @ - here @ cell - ! \ make it relative
      ] \ compile the words for : NB: It's almost the same!
       psp@ ( dummy ) :codeJumpTo
         here @ cell - 0 1+ - \ point to CFA>
         call-op-code over !  \ 0xE8
         1+ here !            \ store new here
         lit docolon          \ get docolon 's absolute address
         here @ 0 cell - - -  \ make it relative
         here @ !             \  compile it
         here @ 0 cell - - here ! \ adjust here
         ]                    \ switch to compile state
         latest @ hide        \ hide the new word during compilation
         exit [               \ epilogue of :

    where call-op-code is either
    0 1* 2* 1+ 2* 1+ 2*    2* 1+ 2*    2*    2*
    or during compilation
    lit [ 0 1* 2* 1+ 2* 1+ 2*    2* 1+ 2*    2*    2*
        here !
        here @ 0 cell - - here ! ]

We do it in two steps. The first is `:noHide`, using this we define
`hide reveal` and then `:`.

** mb2-header
Based on "New Gforth Header" by Bernd Paysan and Anton Ertl. The
difference is, that we moved the cfa into the virtual table (vt). We avoid
having a separate vt for each primitive by design. We provide a
primitive ~call-primitive~

+-----------------+
| pad             | 0 / cell-1 bytes
| name            | 1 / 2^(ld(cell)-2) bytes
| flags'n'len     | 1 cell
| link            | 1 cell
+-----------------+ ^ only for named words; v enough for noname words
| vt              | 1 cell ; target for xt/nt
| parameter-field | 0 / EndOfMemory cells
+-----------------+

flags'n'len: alias-bit immediate-bit compile-only-bit length
+-- xts in vt ---+
| cfa            | ( addr:dtc )
| compile,       | ( xt -- ; compiles xt into here space )
| (to)           | ( x xt -- )
| defer@         | ( xt1 -- xt2 )
| does/extra     | ( i*x addr:body -- j*x )
| name>interpret | ( nt -- nt|0 ; 0 if compile-only )
| name>compile   | ( nt -- xt1 xt2 )
| name>string    | ( nt -- c-addr u )
| name>link      | ( nt1 -- nt2|0 ; 0 if unnamed word )
+----------------+

+- ! -------------+
| pad             | 0 / cell-1 bytes
| !               | 1 / 2^(ld(cell)-2) bytes
| 1               | 1 cell
| link            | 1 cell
| vt:prim         | 1 cell
| cfa             | 1 cells
+-----------------+

code exec-primitive
012 2 2  2 2 2121 % \ 83
01212 2  2 2 2 2  % \ C0
c                 % \ cell add cbx, <WordSize>
001-              % \ FF
0 2 2 21 21212121   \ 1F
c2+               % \ 23 if 16 bit; 27 if 32bit jmp [ebx]

code execute ( i*x xt -- j*x )
0 212 21 212 2121 % \ 5B pop ebx
0 212 21 212 2121 % \ 8B
0 2 2 2  2 2 2121 % \ 03 mov eax, [ebx]
\ ITC-next with cbx and without lodsCell (i.e. lodsw or lodsd, respectivly):
001-              % \ FF
0 2 2 21 21212121   \ 1F
0 2 212  2 2 2 2  % \ 20 jmp [cax]
