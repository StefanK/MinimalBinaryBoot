# History of Origin of Minimal Binary Boot or m2b

I stumbled over guix and found it quite interesting. For me, it was a
generalization of Dan Bernsteins `/package` system. I updated s6 [1]
for it in my copy and I would like to make it its second init
system; but this project is postponed.

[1] https://skarnet.org/software/s6/

Later on, I found some annoncement about the binary seed size.

I found sectorforth, sectorlisp, jonesforth, miniforth and some
comments, that we can do sectorforth smaller.

I started with the ideas of miniforth dropped the compression and
minimized the wordset to `: , exit @ ! nand - psp@ rsp@`. Here, I
extend the idea of `miniforth` to provide the addresses of internal
variables via the initial stack to execution tokens. That allows me,
to shrink the dictionary to two entries, i.e. `: ,`. Note, that both
have the length one, which minimize the binary sizes. With these two
words we can take the other words from the stack by our bootstrapping
input source, e.g. `: ; , ; : - , ;` and so on. This works great, but
not for `exit`, it only exits its own colon definition and not that of
the calling word. I fixed it by additional assembler instructions. But
these additional bytes naggs me. Later on, I fixed it by patching the
exit word with assembler code in forth. That saved me the few bytes
again.

After that tiny success of saving a few bytes, I took a break and went
jogging. During this, I got a insight: If I can write assembler
machine code in forth for exit, than I can do it for every word. That
idea gives birth to `1w4tf` (one word 4 token forth). Before I could
start to implement it, I reduced it to `0w4tf` (zero words 4 token
forth), by using the default case of find as the only defining word.

Then I read about PlankForth at [2].

[2] PlanckForth, https://forth-ev.de/wiki/res/lib/exe/fetch.php/vd-archiv:4d2021-01.pdf,
    page 7

The next step was to reduce the length of the words to one character
and change the dictionary layout. I dropped the length field. These
changes reduce the binary size of my forth, which I named `1cf`, one
character forth.

Again I took a break and went  jogging. While jogging I developed the
idea to drop the linked list dictionary in favour of an simple table
which is indexed by the character. The stage 0 interpreter is now down
to three instructions for 32 bit on i386:

    Interpreter:
      call key
      mov  esi, Return2Interpreter
      jmp  [edi + ebx * CellSize]
    Return2Interpreter:
      dd   Interpreter

NB: The 8068 cannot jump to `di + bx * CellSize`. We have to shift bx
first by 1 and then jump to `di + bx`.

TODO:
https://sourceforge.net/projects/mecrisp/