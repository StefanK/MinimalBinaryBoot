# About Forth
IMHO, Forth is a language which differs from most other programming languages.

I first read about Forth in the 90s. But I never used it up to the 20s.

First, it uses a very simple syntax and machine modell. I.e.:
- Forth code consists of characters, words in forth lingo, separated by white space.
- It uses two stacks and a dictionary, which contains the definitions of the words.
- It's outer interpreter can be very simple:
  1. It reads input according to the simple syntax word by word.
  2. It compiles or interprets each word on its own.
  3. It starts again with 1.

On the other hand, we can define domain specific languages (DSL) in
forth. Moreover, we can do structured programming and define
abstractions. But we can also define new primitives in machine code.
This gives us very much flexibility, i.e.:
- We can implement a new interpeter.
- We can start a new dictionary. The words in it may use another kind
  of implementation, e.g.:
  - ITC - indirect threaded code
  - DTC - direct threaded code either with jump or call code words (we
    use the latter)
  - STC - subroutine threaded code
  - TTC - token threaded code
  - segment addressed words
  - ... etc.pp. ...
- We can start small and use forth source code to bootstrap our own
  interpreter.

# OISF - One Instruction Set Forth
For Von-Neumann machines we have an
[OISC](https://en.wikipedia.org/wiki/One-instruction_set_computer). Of
course we can define an OISF, i.e. a one-instruction set Forth. E.g.
   : OISF
     CASE
       0 OF (:) ENDOF
       1 OF @ ENDOF
       2 OF ! ENDOF
       3 OF - ENDOF
       4 OF psp@ ENDOF
       ...
     END-CASE
But then the interpreter must at least interpret one digit
numbers. AFAICS, its binary code is not shorter than, e.g. 0w4tf.

On the other hand, we can read in a sequence of e.g. `subleq a b c`
instructions and generate the first words from this. But this needs
some kind of monitor to load the code. The code itself is either
binary addresses or their e.g. octal representation. But then we are
back to the octal monitor. Moreover, subleq programming is
not as easy as forth programming.

IMHO it is an intellectual challenge, but not a way to further reduce
binary size.

# Essential parts and syntactic sugar
## Compile mode
As long as we have the words `'` and `,`, we can compile words from
#interpretation mode. Therefore, the compile mode is just convenient
#and common sugar.

## Immediate words
AFAICS we need in Forth only one immediate word, i.e. `[`. Then we can
substitute every immeditate word `FOO` by `[ foo ]` if foo is the
non-immediate version of `FOO`. For parsing words, we have to put the
parsed area in front of the closing bracket.

Therefore, all immediate words, besides `[` are syntactic sugar or a
convenience feature.

That `IMMEDIATE` itself is immediate is mostly only useful for writing
words in this style `: FOO IMMEDIATE ... ;` instead of `: FOO ... ; IMMEDIATE`
or `: FOO [ IMMEDIATE ] ... ;`.

## Hide and reveal
The words `hide` and `reveal` are necessary for redefining words on
top of its older version and for recursion. Thus, it's only convention
if `:` hides its new word until `;`. It's not essential.

Alternativly, we can get the execution token of an immediate word,
e.g. I, by `' I` before we start the new colon word.
