;;; Copyright (C) 2023 Dr. Stefan Karrmann
;;; Distributed unter GPL-3 licence.
;;; convert base16 data into binary data
;;; whitespace is all characters below A
;;; line comment starts with any character above P
;;; 0123456789ABCDEF
;;; ABCDEFGHIJKLMNOP

%define ToMem
CPU i386
BITS 32

%define WordSize 4
%define CAX  eax
%define CBX  ebx
%define CCX  ecx
%define CDX  edx
%define CSP  esp
%define CBP  ebp
%define CSI  esi
%define CDI  edi
%define LODSWS  lodsd
%define STOSWS  stosd
%define DWS dd
%define WSword dword

;  Based on http://www.muppetlabs.com/~breadbox/software/tiny/revisit.html


baseAdr0 equ 0x31008000
;; default: org     0x08048000
org     baseAdr0
SECTION .all start=baseAdr0
ehdr:
        db      0x7F, "ELF", 1, 1, 1    ; e_ident
        times 9 db      0
        dw      2                       ; e_type
        dw      3                       ; e_machine
        dd      1                       ; e_version
        dd      _start                  ; e_entry
        dd      phdr - $$               ; e_phoff
        dd      0                       ; e_shoff
        dd      0                       ; e_flags
        dw      ehdrsz                  ; e_ehsize
        dw      phdrsz                  ; e_phentsize
phdr:
        dd      1                       ; e_phnum       ; p_type
                                        ; e_shentsize
        dd      0                       ; e_shnum       ; p_offset
                                        ; e_shstrndx
        ehdrsz  equ     $ - ehdr
;;        dd      $$                      ; p_vaddr 000xyyyy page aligned little endian
;;        ;; most significant bit must be 0, i.e. vaddr < 0x80000000
;;        dd      $$                      ; p_paddr vvwwxxyy unspecified
;;        dd      filesize                ; p_filesize
;;        dd      filesize+hereSize       ; p_memsz 00wwxxyy
        dw      0x8000                     ; p_vaddr 0000 = xxyy
        db      0      ; p_addr ww
doExit:
        xor     EAX, EAX              ; 31C0 p_vaddr vv p_paddr yy
        xor     EBX, EBX              ; 31DB p_addr xxww
        cmp     EAX, filesize         ; p_paddr vv p_filesize , aka jump over filesize
        inc     EAX                   ; 40 p_memsize yy
        int     0x80                  ; CD80 p_memsize xx ww
        db      0                     ; p_memsize vv=00 ;; maybe 1
        dd      7                     ; p_flags = RWX
        dd      0x1000                ; p_align i.e. page align
        phdrsz  equ     ($ - phdr)

  ; your program here

        ;; c.f. https://unix.stackexchange.com/questions/421750/where-do-you-find-the-syscall-table-for-linux
%define syscall_exit    1 ; oder 64 oder 1
%define syscall_read    3 ; oder 63
%define syscall_write   4 ; oder 64 oder 1

nextOctet:
        push    EAX
readOctet:
        ;; linux read(0, message, 1)
        ;; needs EAX, EBX, ECX, EDX

        push    EAX             ; we store the input buffer on the stack
        push    ESP
        pop     ECX
	xor     EBX, EBX          ; hex: 31DB file descriptor 0 is stdin
        push    byte 1
        pop     edx
        push    byte syscall_read
        pop     EAX
	int     0x80              ; hex: CD80 invoke operating system to do the write
        ;; xor   eax,eax        ; we assume return code 1, i.e. one byte read, here
	pop     EAX

jmp0:
        or      EBP, EBP
        jnz     comment
next:
        cmp     al, 'Z'
        je      short doExit
        sub     al, 'A'                 ; hex: 80EB30
        jl      short readOctet         ; skip "white space"
        cmp     al, 'P'-'A'
        jg      short comment
handleBase16:
        xchg    EAX, EBX
        pop     EAX
        shl     EAX, 4
        or      al, bl
        or      ah, ah
        jz      short nextOctet
writeOctet:
        ;; linux read(0, message, 1)
        ;; needs EAX, EBX, ECX, EDX
        push    EAX
        push    ESP
        pop     ECX
	push    byte 1          ; file descriptor 1 is stdout
        pop     EBX
	mov     EDX, EBX          ; hex: 89DA
	push    syscall_write
        pop     EAX
	int     0x80              ; hex: CD80 invoke operating system to do the write
start2:
        push    byte 1          ; push EAX ?
        jmp     short readOctet

comment:
        mov     EBP, EDI
        cmp     al, 0x0A
        jne     short readOctet
_start:
        xor     EBP, EBP
        jmp     short start2

        filesize      equ     $ - $$
