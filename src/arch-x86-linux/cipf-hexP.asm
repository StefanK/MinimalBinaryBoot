; cipf - character indexed pre-forth
; Copyright 2022 Dr. Stefan Karrmann
; Licenced under GPL-3.

; Register usage:
; SP = parameter stack pointer (grows downwards)
; BP = return stack pointer (grows downwards)
; SI = execution pointer
; DI = indirection table
;
; This Forth, i.e. cipf, uses direct threaded code (DTC) with call
; type. It has as its dictionary a simple lookup table for words of
; one character length.

CPU i386
BITS 32

%define CellSizeShift   2
%define CAX  eax     ; cell size register ax/eax, ff.
%define CBX  ebx
%define CCX  ecx
%define CDX  edx
%define CSP  esp
%define CBP  ebp
%define CSI  esi
%define CDI  edi
%define LODScell  lodsd
%define STOScell  stosd
%define Dcell dd
%define WordCell dword
%define saveLinuxStack        1

%define CellSize  (1 << CellSizeShift)

%define NEXT    jmp short next

;; linux default: org     0x08048000
%include "baseAdr.asm"
org     baseAdr2      ; generates 0x0000 at start!
SECTION .all start=baseAdr2, align=1
        jmp     start2

startStack:

  Dcell   Code
  Dcell   PSPFetch
  Dcell   Fetch
  Dcell   Substract
  Dcell   Store
  ; Dcell   ~0      ; true or -1
  ; Dcell   HERE
  ; indirectionTable = Here + CellSize

  PSP0  equ  $ - CellSize
endOfStack:

start2:

        mov     CDI, indirectionTable

        mov     CAX, startStack
        xchg    CAX, CSP
        mov     [CDI], CAX


stackbottom equ $ & ~ (CellSize - 1) ; align down

; NB: We can copy call key, recalculate the offset and get the key
; word independent of the cell size.

; First, initialize the word code by itself and its character name in
; the input and its address on initial stack:
Code:
  call  key
  pop   CAX
; save xt to indirection table
  mov   [edi+ebx*CellSize], eax

; fall through:

InterpreterLoop:
  call  key
  mov   CSI, .return
  jmp   [EDI+EBX*CellSize]

.return:
  Dcell   InterpreterLoop

; returns
; CBX = key
; clobbers CAX, CBX, CCX, CDX
key:

  push  byte 3
  push  esp
  pop   ecx                     ; store byte directly onto the stack
  pop   eax                     ; linux syscall 3
  push  byte 1
  pop   edx                     ; length is 1 byte
  xor   ebx, ebx
  push  ebx
  int   0x80
  pop   ebx
  ret

; Define anonymous forth words:

PSPFetch:                       ; psp@ ( -- n )  3 bytes op-code
  push  CSP
  NEXT

Fetch:                          ; @ ( addr -- val ) 5 bytes op-code
  pop   CBX
  push  WordCell [CBX]
  NEXT

;;; We need a code size of the right number of bytes. We can achieve
;;; this for other cpu by additional nop-codes, rearranging next,
;;; etc.pp. Later, in cipf2f*, we can skip the nop-codes by adjusting
;;; the value of - in the character indirection table as soon as we
;;; can do arithmetic in cipf.
Substract:                      ; - ( n1 n2 -- n1-n2 ) 4 bytes op-code
  pop   eax
  sub   [esp], eax
;  NEXT                                             3 bytes op-code
next:
  LODScell
  jmp   CAX

Store:                          ; ! ( val addr -- ) 5 bytes op-code
  pop   CBX
  pop   WordCell [CBX]
  NEXT

  lastChar      equ '~'

  filesize      equ     $ - $$

HERE    equ  (Store + 3*CellSize)

indirectionTable        equ (HERE + CellSize)
linuxStack              equ (HERE + CellSize)
startHERE               equ (indirectionTable + (lastChar + 1) * CellSize)
