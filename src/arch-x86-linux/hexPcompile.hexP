\ Copyright (C) 2023 Dr. Stefan Karrmann
\ Distributed unter GPL-3 licence.
\ Convert base16 data into binary data.
\ Whitespace is all characters below A,
\ line comment starts with any character above P,
\ Z exits the compiler and starts the loaded program.
\ 0123456789ABCDEF
\ ABCDEFGHIJKLMNOP

\ hexP-code  \ virtual-adr: hex-code  # assembler code
             \                        # ehdr:
HP           \ 0x09BF1000: 7F         # db 0x7F
EFEMEG       \ 0x09BF1001: 454C46     # db "ELF"
ABABAB       \ 0x09BF1004: 010101     # db 1, 1, 1
AAAAAAAAAA   \ 0x09BF1007: 0000000000 # times 5 db 0
AAAAAAAAA    \ 0x09BF100C: 00000000   # times 4 db 0
ACAA         \ 0x09BF1010: 0200       # dw 2
ADAA         \ 0x09BF1012: 0300       # dw 3
ABAAAAAA     \ 0x09BF1014: 01000000   # dd 1
INIAAADB     \ 0x09BF1018: 8A800031   # dd _start
CMAAAAAA     \ 0x09BF101C: 2C000000   # dd phdr - $$
AAAAAAAA     \ 0x09BF1030: 00000000   # dd 0
AAAAAAAA     \ 0x09BF1024: 00000000   # dd 0
DEAA         \ 0x09BF1028: 3400       # dw ehdrsz
CAAA         \ 0x09BF102A: 2000       # dw phdrsz
             \                        # phdr:
ABAAAAAA     \ 0x09BF102C: 01000000   # dd 1
AAAAAAAA     \ 0x09BF1030: 00000000   # dd 0
             \                        # ehdrsz equ ($-ehdr)
AAIA         \ 0x09BF1034: 0080       # dw 0x8000
AA           \ 0x09BF1036: 00         # db 0
             \                        # doExit:
DBMA         \ 0x09BF1037: 31C0       # xor eax, eax
DBNL         \ 0x09BF1039: 31DB       # xor ebx, ebx
DNJBAAAAAA   \ 0x09BF103B: 3D8E000000 # cmp eax, filesize
EA           \ 0x09BF1040: 40         # inc eax
MNIA         \ 0x09BF1041: CD80       # int 0x80
AA           \ 0x09BF1043: 00         # db 0
AHAAAAAA     \ 0x09BF1044: 07000000   # dd 7
AABAAAAA     \ 0x09BF1048: 01000000   # dd 0x1000
             \                        # phdrsz equ ($-phdr)
             \                        # nextOctet:
FA           \ 0x09BF104C: 50         # push eax
             \                        # readOctet:
FA           \ 0x09BF104D: 50         # push eax
FE           \ 0x09BF104E: 54         # push esp
FJ           \ 0x09BF104F: 59         # pop ecx
DBNL         \ 0x09BF1050: 31DB       # xor ebx, ebx
GKAB
FK
GKAD         \ 0x09BF1052: 6A03       # push byte 3
FI           \ 0x09BF1054: 58         # pop eax
MNIA         \ 0x09BF1055: CD80       # int 0x80
FI           \ 0x09BF1057: 58         # pop eax
             \                        # jmp0:
AJON         \ 0x09BF1058: 09ED       # or ebp, ebp
HFCI         \ 0x09BF105A: 7526       # jnz comment
             \                        # next:
DMFK         \ 0x09BF105C: 3C5A       # cmp al, 'Z'
HENE         \ 0x09BF105E: 74D5       # je short doExit
CMEB         \ 0x09BF1060: 2C41       # sub al, 'A'
HMOG         \ 0x09BF1062: 7CE7       # jl short readOctet
DMAP         \ 0x09BF1064: 3C0F       # cmp al, 'P' - 'A'
HPBM         \ 0x09BF1066: 7F1A       # jg short comment
             \                        # handleBase16:
JD           \ 0x09BF1068: 93         # xchg eax, ebx
FI           \ 0x09BF1069: 58         # pop EAX
MBOAAE       \ 0x09BF106A: C1E004     # shl EAX, 4
AINI         \ 0x09BF106D: 08D8       # or al, bl
AIOE         \ 0x09BF106F: 08E4       # or ah, ah
HENG         \ 0x09BF1071: 74D7       # jz short nextOctet
             \                        # writeOctet:
FA           \ 0x09BF1073: 50         # push EAX
FE           \ 0x09BF1074: 54         # push ESP
FJ           \ 0x09BF1075: 59         # pop ECX
GKAB         \ 0x09BF1076: 6A01       # push byte 1
FL           \ 0x09BF1078: 5B         # pop EBX
IJNK         \ 0x09BF1079: 89DA       # mov EDX, EBX
GKAE         \ 0x09BF107B: 6A04       # push 4
FI           \ 0x09BF107D: 58         # pop EAX
MNIA         \ 0x09BF107E: CD80       # int 0x80
             \                        # start2:
GKAB         \ 0x09BF1080: 6A01       # push byte 1
OLMG         \ 0x09BF1082: EBC7       # jmp short readOctet
             \                        # comment:
IJPN         \ 0x09BF1084: 89FD       # mov EBP, EDI
DMAK         \ 0x09BF1086: 3C0A       # cmp al, 0x0A
HFMA         \ 0x09BF1088: 75C1       # jne short readOctet
             \                        # _start:
DBON         \ 0x09BF108A: 31ED       # xor EBP, EBP
OLPC         \ 0x09BF108C: EBF0       # jmp short start2
             \ 0x09BF108E:            # filesize equ $ - $$
Z            \ exit hexPcompile
