;;; Copyright (C) 2023 Dr. Stefan Karrmann
;;; Distributed unter GPL-3 licence.
;;; convert base16 data into binary data
;;; whitespace is all characters below A
;;; line comment starts with any character above P
;;; 0123456789ABCDEF
;;; ABCDEFGHIJKLMNOP


CPU i386
BITS 32

;  Based on http://www.muppetlabs.com/~breadbox/software/tiny/revisit.html

%include "baseAdr.asm"

;; c.f. https://unix.stackexchange.com/questions/421750/where-do-you-find-the-syscall-table-for-linux
%define syscall_exit    1 ; oder 64 oder 1
%define syscall_read    3 ; oder 63
%define syscall_write   4 ; oder 64 oder 1


;; default: org     0x08048000
;; This code is position independant (PIC).
org     0                       ; baseAdr2
SECTION .all                    ; start=baseAdr2, align=1
  ; your program here

_start:
start2:
        push    byte 1          ; push eax ?
        db      0x3C            ; i.e. cmp     al, <push eax>-opCode
        ; jmp readOctet

nextOctet:
        push    eax
readOctet:
        ;; linux read(0, message, 1)
        ;; needs eax, ebx, ecx, edx

        push    eax             ; we store the input buffer on the stack
        push    esp
        pop     ecx
	xor     ebx, ebx          ; hex: 31DB file descriptor 0 is stdin
        push    byte 1
        pop     edx
        push    byte syscall_read
        pop     eax
	int     0x80              ; hex: CD80 invoke operating system to do the write
        ;; xor   eax,eax        ; we assume return code 1, i.e. one byte read, here
	pop     eax
jmp0:
        or      ebp, ebp
        jz      next
comment:
        mov     ebp, edi
        cmp     al, 0x0A
        jne     short readOctet
next:
        cmp     al, 'Z'
        je      short doExit
        sub     al, 'A'                 ; hex: 80EB30
        jl      short readOctet         ; skip "white space"
        cmp     al, 'P'-'A'
        jg      short comment
        xor     ebp, ebp
handleBase16:
        xchg    eax, ebx
        pop     eax
        shl     eax, 4
        or      al, bl
        or      ah, ah
        jz      short nextOctet
writeOctet:
        ;; linux read(0, message, 1)
        ;; needs eax, ebx, ecx, edx
        push    eax
        push    esp
        pop     ecx
	push    byte 1          ; file descriptor 1 is stdout
        pop     ebx
	mov     edx, ebx          ; hex: 89DA
	push    syscall_write
        pop     eax
	int     0x80              ; hex: CD80 invoke operating system to do the write
        jmp     short start2
doExit:
        xor     eax, eax
        xor     ebx, ebx
        inc     eax
        int     0x80

        filesize      equ     ($ - $$)

        times ((filesize - 0x4B)*0xFFFFFFF) dd 0xDeadBeef
