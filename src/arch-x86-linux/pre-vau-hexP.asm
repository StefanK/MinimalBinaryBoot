; pre vau scheme
; Copyright 2023 Dr. Stefan Karrmann
; Licenced under GPL-3.

;;; objdump -D build/arch-x86-linux/pre-vau-hexP.bin -m i386 -b binary --adjust-vma=0x0abf1042
; Register usage:
; SP = parameter stack pointer (grows downwards)
;; References:
;; R. Kent Dybvig, Simon Peyton Jones, Amr Sabry∗: A Monadic Framework for Delimited Continuations, https://legacy.cs.indiana.edu/~dyb/pubs/monadicDC.pdf
;; https://web.wpi.edu/Pubs/ETD/Available/etd-090110-124904/unrestricted/jshutt.pdf

;; CESK-machine https://legacy.cs.indiana.edu/ftp/techreports/TR202.pdf
;; state is (expr,env,store,pc,mc) with start (expr0,env0,0,0)
;; we drop store and add [mode-]eval?, which replaces the (ret val)-continuation
;; So, our state is:  (eval?,expr,env,pc,mc)
;; we adapt the rules to vau-calculus:

;; E(#t,x,env,pc,mc) = (#f,(env x),0,pc,mc) ; return value of variable
;; E(#t,(M.Ns),e,pc,mc) = (#t,M,e,(Arg0 Ns e pc),mc)
;; E(#t,v,env,pc,mv) = (#f,v,0,pc,mc)       ; return (immediate) value

;; app:
;; E(#f,(vprim f),e0,(Arg0 Ns e pc),mc) = (f) ; vau primitive
;; E(#f,(vau fs body e1),e0,(Arg0 Ns e pc),mc)
;;  = (#t,body,e1[fs := (e.Ns)],pc,mc)        ; vau abstraction
;; E(#f,l,e0,(Arg0 Ns e pc),mc) = (#f,l,e0,(Args (().Ns) e pc),mc)

;; args:
;; E(#f,v,e0,(Args (vs.N.Ns) e pc),mc) = (#t,N,0,e,(Args ((v.vs).Ns) e pc),mc)
;; E(#f,v,e0,(Args (vs.()) e pc),mc)
;;   = (f')                                 if f == vprim f'
;;   = (#t,body,e1[fs := (tail vs')],pc,mc) if f == (lambda fs body e1)
;;   where vs' = (reverse (cons v vs)); f = (head fs'); f = vprim f'

;;; Calling conventions
;;; forth with two stacks: or DTC
;;; push args
;;; xchg ebp, esp
;;; call f
;;; xchg ebp, esp

;;; cdecl:
;;; push ebp
;;; mov  ebp, esp
;;; push args
;;; call f
;;; add  esp, n
;;; mov esp, ebp
;;; pop ebp

;;; remark on calling conventions
;;; cdecl is more flexible with regard to the number of arguments
;;; It's ebp only usefull if we use alloca() directly or indirectly.

;;; simple calling convention:
;;; push argn         callee uses:
;;; push arg0           mov eax, [esp+staticOffset+dynamicOffset]
;;; call
;;; add esp, n
;;; we use one or two tag bits, i.e.:
;;; xxx1 for numbers
;;; xx00 for a pair with car and cdr, or first and second pointers
;;; xx10 for complex value which xx points to an info header
        nil     equ     0xFFFFFFFE
        true    equ     0xFFFFFFFC
        false   equ     0xFFFFFFFA
        contArg0 equ    0

        tagMask equ     3
        tagInfo equ     2
        tagNum  equ     1
        tagPair equ     0

        CPU i386
        BITS 32

        WordSize        equ 4   ; bytes
        WordShift       equ 2   ; bits

%macro PUSH 1
        %assign stackOffset (stackOffset + WordSize)
        push    %1
%endmacro

%macro POP 1
        %assign stackOffset (stackOffset - WordSize)
        pop    %1
%endmacro

%define RESETstackOffset SETstackOffset(0)
%macro SETstackOffset 1
        %assing stackOffset %1
%endmacro

%define STACK(o) [esp+o+stackOffset]

;;; linux default: org     0x08048000
%include "baseAdr.asm"

        memSize equ 0x01000beb
        toEndAdr        equ (heapStart + (memSize + baseAdr0 - heapStart))
        heapSize        equ (memSize + baseAdr0 - heapStart)
        heapSizeHalf    equ (heapSize / 2)
        osStack equ baseAdr0
        expr0   equ osStack + WordSize
        expr1   equ expr0 + WordSize
        expr2   equ expr1 + WordSize
;;; assert expr2 + WordSize < evalLoop
org     baseAdr2      ; generates 0x0000 at start!
SECTION .all start=baseAdr2, align=1
        mov     [osStack], esp
evalLoop:                       ;todo: refacotr to evalStep; jmp evalLoop
        call    evalStep
        jmp     evalLoop
alloc0:                         ; ( ax:size -- bx:ptr )
                                ; simple, i.e. no check if full
        mov     ebx, [free]
        add     eax, ebx
        mov     [free], eax
        ret                     ; result in ebx

doAlloc:
        call    [alloc]

cons:
        push    2*WordSize
        pop     eax             ; short for mov eax, 2*WordSize
        call    doAlloc
        mov     eax, [expr0]
        mov     [ebx], eax
        mov     eax, [expr1]
        mov     [ebx+WordSize], eax
        mov     [expr0], ebx
        ret

%define CAR(r)  mov r, [r]
%define CDR(r)  mov r, [r+WordSize]
car:                            ; ( ax:pair -- ax:car )
        CAR(eax)
        ret
cdr:                            ; ( ax:pair -- ax:cdr )
        CDR(eax)
        ret
%macro isPair 2
        test    %1, tagMask
        jnz     %2
%endmacro

evalStep:
tryEval:
        mov     eax, true
        cmp     [evalp], eax
        jne      notEval
trySymbol:
        mov     ebx, [expr]
        mov     eax, [ebx]
        cmp     eax, (symbol_info + tagInfo)
        jne     tryPair
doLookup:
;;; E(#t,x,env,pc,mc) = (#f,(env x),0,pc,mc) ; return value of variable
        mov     eax, false
        mov     [evalp], eax
.nextFrame:
.getFrame:
        mov     eax, [env]
        isPair  eax, .noFrame
        push    eax
        CDR     (eax)
        mov     [env], eax      ; (set! env (cdr env))
        mov     eax, [esp+WordSize]
        CDR     (eax)
        mov     [expr1], eax    ; vals
        pop     eax
        CAR     (eax)
        mov     [expr0], eax    ;vars
.varsPair:
        isPair  eax, .tryTail
        CAR     (eax)
.symbolCmp:
        cmp     eax, [expr]
        jnz     .next
.found:
        xor     eax, eax
        mov     [env], eax
        mov     eax, [expr1]
        CAR     (eax)
        ret                     ; found it
.next:
        mov     eax, [expr1]
        CDR     (eax)
        mov     [expr1], eax
        mov     eax, [expr0]
        CDR     (eax)
        mov     [expr0], eax
        jmp     .varsPair
.tryTail:
        cmp     eax, [expr]
        jnz     .nextFrame
        mov     [expr1], eax
        ret                     ; found it as rest-list
.noFrame:                       ; symbol not found
;;; todo: call a delimited continuation
        mov     eax, nil
        mov     [evalp], eax
        ret
tryPair:
;; E(#t,(M.Ns),e,pc,mc) = (#t,M,e,(Arg0 Ns e pc),mc)
        test    eax, tagMask
        jnz     doAtom
        push    3*WordSize
        pop     eax
        call    doAlloc
        push    ebx             ; save adr of new pair(-block)
        xor     eax, eax
        mov     [ebx], eax      ; contArg0
        add     ebx, 2*WordSize
        mov     [ebx-WordSize], ebx
        mov     eax, [expr]
        call    cdr
        mov     [ebx], eax
        add     ebx, 2*WordSize
        mov     [ebx-WordSize], ebx
        mov     eax, [env]
        mov     [ebx], eax
        mov     eax, [pc]
        mov     [ebx+WordSize], eax
        pop     eax
        mov     [pc], eax
        mov     eax, [expr]
        call    car
        mov     [expr], eax
        ret
doAtom:
;; E(#t,v,env,pc,mc) = (#f,v,0,pc,mc)       ; return (immediate) value
;; fall through to partial continuation
.todoTryPCThenMC:
        mov     eax, pc
        CAR     (eax)
        test    eax, eax        ; eax == contArg0
        jz      .pcPairOrNil
        mov     ebx, [expr]
        mov     ebx, [ebx]
        cmp     ebx, closure_info_vprim
        jnz     .tryVau
.vprimTodo:
        ret
;; E(#f,vprim f,env,(Arg0 Ns e pc),mc) = (clo-body f)
;; E(#f,vau f,env,(Arg0 Ns e pc),mc) = E(#t,(clo-body f),
;;                                       (env-extend
;;                                         (frame (clo-formals f)
;;                                                (cons e Ns))
;;                                                (clo-env f)),pc,mc)
.tryVau:
        cmp     ebx, closure_info_vau
        jnz     .doLambda
.doVau:
        ;; build new env:
        mov     eax, 3*2*WordSize ; alloc 3 pairs
        call    alloc
        mov     eax, [pc]
        CDR     (eax)
        push    eax             ;Ns
        CDR     (eax)
        CAR     (eax)
        mov     [ebx], eax      ; (set-car vals e)
        pop     eax
        mov     [ebx+1*WordSize], eax ;(set-cdr vals Ns)
        mov     eax, [expr]
        push    eax
        mov     eax, [eax+1*WordSize] ;clo-formals
        mov     [ebx+2*WordSize], eax
        mov     [ebx+3*WordSize], ebx ;frame ready
        add     ebx, 2*WordSize
        mov     [ebx+2*WordSize], ebx ;set-car new-env frame
        mov     eax, [esp+WordSize]   ;copy top of stack
        mov     eax, [eax+3*WordSize] ;clo-env
        mov     [ebx+3*WordSize], eax ; new-env ready
        mov     [env], eax
        pop     eax
        add     eax,2*WordSize
        mov     [expr], eax
        ret
.doLambda:
.doLambdaTodo:
;; E(#f,l*,env,(Arg0 Ns e pc),mc) = E(#f,l*,env,((().Ns) e pc),mc)
        ;; build new pc:
        mov     eax, 2*2*WordSize ; alloc 2 pairs
        call    alloc
        mov     eax, nil
        mov     [ebx+1*WordSize], eax      ;set-car (vs.Ns) nil
        mov     eax, [pc]
        CDR     (eax)
        push    eax             ;(Ns.e.pc)
        CAR     (eax)
        mov     [ebx+1*WordSize], eax ;set-cdr (vs.Ns) Ns
        mov     [ebx+2*WordSize], ebx ;set-car new-pc (vs.Ns)
        pop     eax
        mov     [ebx+3*WordSize], eax ;set-cdr new-pc (e.pc)
        ret
.pcPairOrNil:
        isPair  eax, .doCont
.applyLambdaOrLprim:
.tryLambda:
        cmp     ebx, closure_info_lambda
        jnz     .doLPrim
.doLambdaTODOwithReverse:
;; E(#f,v,env,((vs.N.Ns) e pc),mc) = E(#t,N,e,(((v.vs).Ns) e pc,mc))
;; E(#f,v,env,((vs.()) e pc),mc)
;;        = E(#t,(clo-body f),(env-extend
;;                              (frame (clo-formals f)
;;                                     (cdr (reverse vs)))
;;                              (clo-env f)),pc,mc)
;;    where f = (last vs) if (lambda? f)
;;        = (clo-body f)
;;    where f = (last vs) if (lprim? f)
.doLPrim:
.doLPrimTodo:
.doCont:
;; E(#f,v,env,nil,nil) = v       ; return (immediate) value
;; E(#t,v,env,nil,prompt:mc) = E(#t,v,env,nil,mc)
;; E(#t,v,env,nil,(pc.mc)) = E(#t,v,env,pc,mc)
        mov     eax, false
        mov     [evalp], eax
        mov     eax, nil
        mov     [env], eax
        ret
notEval:
tryApplication:
        mov     ebx, [pc]
.todo:
tryVprim:
;; E(#f,(vprim f),e0,(Arg0 Ns e pc),mc) = (f) ; vau primitive
        mov     ebx, [expr]
        mov     eax, [ebx]
        cmp     eax, (closure_info_vprim + tagInfo)
        jne     tryVau
.todo:
tryVau:
;; E(#f,(vau fs body e1),e0,(Arg0 Ns e pc),mc)
;;  = (#t,body,e1[fs := (e.Ns)],pc,mc)        ; vau abstraction
        cmp     eax, (closure_info_vau + tagInfo)
        jne     tryLambda
.todo:
tryLambda:
;; E(#f,l,e0,(Arg0 Ns e pc),mc) = (#f,l,e0,(Args (().Ns) e pc),mc)
.todo:
;; E(#f,v,e0,(Args (vs.N.Ns) e pc),mc) = (#t,N,0,e,(Args ((v.vs).Ns) e pc),mc)
;; E(#f,v,e0,(Args (vs.()) e pc),mc)
;;   = (f')                                 if f == vprim f'
;;   = (#t,body,e1[fs := (tail vs')],pc,mc) if f == (lambda fs body e1)
;;   where vs' = (reverse (cons v vs)); f = (head fs'); f = vprim f'

memCopyBytes:
    ; Input:
    ; edi - Destination address
    ; esi - Source address
    ; ecx - Number of bytes to copy

        cld                ; Set the direction flag to forward (copy from the beginning to the end)
        rep movsb          ; Copy ecx bytes from esi to edi

        ret                ; Return from the function
memCopyWords:
    ; Input:
    ; edi - Destination address (pointer to the destination buffer)
    ; esi - Source address (pointer to the source buffer)
    ; ecx - Number of 32-bit words to copy (size of the memory block in bytes / 4)

    cld                ; Set the direction flag to forward (copy from the beginning to the end)
    rep movsd          ; Copy ecx doublewords (32-bit words) from esi to edi

    ret                ; Return from the function

memEqual:
    ; Input:
    ; edi - Pointer to the first string
    ; esi - Pointer to the second string
    ; ecx - Length of the strings to compare

        cld                 ; Set the direction flag to forward (compare from the beginning to the end)
        xor eax, eax        ; Clear EAX (used for storing the result)
        repe cmpsb          ; Compare bytes in DS:ESI with bytes in ES:EDI until ECX = 0 or mismatch

    ; The result is stored in the FLAGS register after the rep cmpsb instruction:
    ;   - If ECX = 0, both strings are equal (ZF = 1)
    ;   - If a mismatch occurs, ZF = 0

        setz al             ; Set AL to 1 (true) if ZF is set (strings are equal); AL = 0 (false) otherwise
        ret

readChar:
        .todo:
                                ; Load the next character from the input
        mov eax, 3         ; sys_read system call number
        mov ebx, 0         ; file descriptor (stdin)
        mov ecx, edi       ; pointer to the buffer
        mov edx, 1         ; read one character at a time
        int 0x80           ; invoke the system call

        cmp eax, 0         ; Check for end of file (EOF)
        je done_parse      ; If it's the end, finish parsing

        defaultSymLen   equ     32
parse:
        xor     ebx, ebx        ; counter for parenthese balance
        .skipWhitespace:
        call    readChar
        cmp     al, ' '
        jle     .skipWhitespace
        cmp     al, ';'
        jne     .readToken
        .findNewLine:
        call    readChar
        cmp     al, 0x10        ;'\n'
        jne     .findNewLine
        jmp     .skipWhitespace
        .readToken:
        cmp     al, '('         ; start of a list
        je      .open_paren
        cmp     al, ')'         ; end of a list
        je      .close_paren
        .readSymbol:
        mov     eax, defaultSymLen+2*WordSize
        call    alloc
        mov     long [ebx], symbol_info
        mov     ebx, 2*WordSize
        push    ebx             ;save adr of begin of word
        mov     edi, ebx        ; pointer to input buffer
        mov     [ebx], al
        .open_paren:
    ; Increment the parentheses balance counter
    inc ebx
    mov byte [esi], al ; Copy the '(' to the output buffer
    inc esi
    inc edi
    jmp parse_sexp

        .close_paren:
    ; Decrement the parentheses balance counter
    dec ebx
    mov byte [esi], al ; Copy the ')' to the output buffer
    inc esi
    inc edi
        jmp parse_sexp
        ;; unclear
        parse_sexp:
        done_parse:

.PrintList:
	mov	$'(',%al
2:	push	(%bx,%si)
	mov	(%si),%si
	call	.PutObject
	mov	$' ',%al
	pop	%si				# restore 1
	test	%si,%si
	js	2b				# jump if cons
	jz	4f				# jump if nil
	mov	$249,%al			# bullet (A∙B)
	call	.PutObject
4:	mov	$')',%al
	jmp	PutChar

.ifPrint:
	xchg	%di,%si				# Print(x:si)
	test	%di,%di
	jnz	PrintObject			# print newline for empty args
	mov	$'\r',%al

.PutObject:					# .PutObject(c:al,x:si)
.PrintString:					# nul-terminated in si
	call	PutChar				# preserves si

PrintObject:					# PrintObject(x:si)
	test	%si,%si				# set sf=1 if cons
	js	.PrintList			# jump if not cons
.PrintAtom:
	lodsb
	test	%al,%al				# test for nul terminator
	jnz	.PrintString			# -> ret
	ret


;;; data
symbol_info:
symbol_info_info:       dd info1_info_info
symbol_info_evacuate:   dd 0
symbol_info_scan:       dd 0
symbol_info_eq:         dd 0

;;; closure common layout
;;; info
;;; formals
;;; body
;;; env
closure_info_vprim:
closure_info_vprim_info:       dd info0_info_info
closure_info_vprim_evacuate:   dd 0
closure_info_vprim_scan:       dd 0
closure_info_vprim_eq:         dd 0
closure_info_vprim_size:       dd 0

closure_info_vau:
closure_info_vau_info:       dd info0_info_info
closure_info_vau_evacuate:   dd 0
closure_info_vau_scan:       dd 0
closure_info_vau_eq:         dd 0
closure_info_vau_size:       dd 0

closure_info_lprim:
closure_info_lprim_info:       dd info0_info_info
closure_info_lprim_evacuate:   dd 0
closure_info_lprim_scan:       dd 0
closure_info_lprim_eq:         dd 0
closure_info_lprim_size:       dd 0

closure_info_lambda:
closure_info_lambda_info:       dd info0_info_info
closure_info_lambda_evacuate:   dd 0
closure_info_lambda_scan:       dd 0
closure_info_lambda_eq:         dd 0
closure_info_lambda_size:       dd 0

id_info:
id_info_info            dd info0_info_info
id_info_evacuate:       dd 0
id_info_scan:           dd 0
id_info_eq:             dd 0
id_info_size:           dd WordSize

info0_info:                     ; object which size depends only on
                                ; its type
info0_info_info:        dd info0_info_info
info0_info_evacuate:    dd 0
info0_info_scan:        dd 0
info0_info_eq:          dd 0
info0_info_size:        dd 5*WordSize

info1_info:                     ; object which size depends on its value
info1_info_info:        dd info0_info_info
info1_info_evacuate:    dd 0
info1_info_scan:        dd 0
info1_info_eq:          dd 0
info1_info_size:        dd 4*WordSize

evalp:  dd      true
memFree:
free:   dd      heapStart
expr:   dd      0
env:    dd      0
partialContinuatione:
pc:     dd      0
metaContinuation:
mc:     dd      0
alloc:  dd      alloc0

toSpace:        dd heapStart
toEnd:          dd toEndAdr ; (heapStart + heapSizeHalf)
fromSpace:      dd toEndAdr ; (heapStart + heapSizeHalf)

heapStart:
        dd      baseAdr2        ; debug
        dd      memSize
        dd      toEndAdr
;        dd      heapSize
;        dd      heapSizeHalf
