;;; Copyright (C) 2023 Dr. Stefan Karrmann
;;; Distributed unter GPL-3 licence.
;;; convert base16 data into binary data
;;; whitespace is all characters below A
;;; line comment starts with any character above P
;;; 0123456789ABCDEF
;;; ABCDEFGHIJKLMNOP

CPU i386
BITS 32

%define WordSize 4

;  Based on http://www.muppetlabs.com/~breadbox/software/tiny/revisit.html

%include "baseAdr.asm"
;; default: org     0x08048000
org             baseAdr0

SECTION .all start=baseAdr0
buffer  equ     baseAdr1
ehdr:
        db      0x7F, "ELF", 1, 1, 1    ; e_ident
        times 9 db      0
        dw      2                       ; e_type
        dw      3                       ; e_machine
        dd      1                       ; e_version
        dd      _start                  ; e_entry
        dd      phdr - $$               ; e_phoff
        dd      0                       ; e_shoff
        dd      0                       ; e_flags
        dw      ehdrsz                  ; e_ehsize
        dw      phdrsz                  ; e_phentsize
phdr:
        dd      1                       ; e_phnum       ; p_type
                                        ; e_shentsize
        dd      0                       ; e_shnum       ; p_offset
                                        ; e_shstrndx
        ehdrsz  equ     $ - ehdr
;;        dd      $$                      ; p_vaddr 000xyyyy page aligned little endian
;;        ;; most significant bit must be 0, i.e. vaddr < 0x80000000
;;        dd      $$                      ; p_paddr vvwwxxyy unspecified
;;        dd      filesize                ; p_filesize
;;        dd      filesize+hereSize       ; p_memsz 00wwxxyy
        dw      (orgOffset * 0x0100)      ; p_vaddr 0010 = yyxx
_start:
        mov     edi, buffer           ; BF[*] p_vaddr wwvv p_paddr yyxxww
        db      0x3D
        dd      filesize
        ;cmp     eax, dword filesize    ; p_paddr vv p_filesize , aka jump over filesize
jmpLoad1:
        ;; we will overwrite the offset
        ;; while writing the buffer
        jmp     short readOctet          ; EB[xx] p_memsize yy xx
offset_0x42:
;; hack to throw an error, if the sizeOfElf changes:
        times (($ - $$ - offsetElfJmp)*0xFFFFFFFF) dd 0xDeadBeef
        times (($ - $$ - 0x42)*0xFFFFFFFF) dd 0xDeadBeef
        dw      0x0100                ; p_memsize wwvv=0001
        dd      7                     ; p_flags = RWX
        dd      0x1000                ; p_align i.e. page align
phdrsz  equ     ($ - phdr)

;; c.f. https://unix.stackexchange.com/questions/421750/where-do-you-find-the-syscall-table-for-linux
%define syscall_exit    1 ; oder 64 oder 1
%define syscall_read    3 ; oder 63
%define syscall_write   4 ; oder 64 oder 1

;; your program here:
nextOctet:
        push    eax
        ;; NB: Linux initilizes it with 0. Therfore we need an initial
        ;; B in our source code.

readOctet:
        ;; linux read(0, message, 1)
        ;; needs eax, ebx, ecx, edx
	xor     ebx, ebx          ; hex: 31DB file descriptor 0 is stdin
	mov     edx, ebx          ; hex: 89DA
	inc     edx               ; hex: 42  1 = number of bytes
        push    eax
        push    esp
        pop     ecx             ; we use the stack as the input buffer
        push    syscall_read
        pop     eax
	int     0x80              ; hex: CD80 invoke operating system to do the write
        ;; We don't check the return code in eax. We assume 1.
        pop     eax

next:
        sub     al, 'A'                 ; hex: 80EB30
        xchg    eax, ebx

handleBase16:
        pop     eax
        shl     eax, 4
        or      al, bl
        or      ah, ah
        jz      short nextOctet
        stosb                   ; es:[edi], al
start2:
        push   byte 1
        jmp    short jmpLoad1

        filesize      equ     $ - $$
