        sizeOfLoad1     equ     (0x38)
        ;; sizeOfElf       equ     (0x4C)
        offsetElfJmp    equ     (0x42)
        offsetLoad1     equ     (offsetElfJmp - sizeOfLoad1)
        movOpCode       equ     (0xBF)
        orgOffset       equ     (0x10)
        baseAdr0        equ     (  offsetLoad1 * 0x01000000 \
                                 + movOpCode * 0x00010000 \
                                 + orgOffset * 0x00000100 \
                                 )
        baseAdr1        equ     (baseAdr0 + offsetLoad1)
        baseAdr2        equ     (baseAdr1 + sizeOfLoad1)
