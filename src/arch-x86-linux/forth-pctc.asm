; Copyright 2022 Dr. Stefan Karrmann
; Licenced under GPL-3.
; Register usage:
; SP = parameter stack pointer (grows downwards)
; BP = return stack pointer (grows downwards)
; SI = execution pointer
; nasm
; Program header

        %include "baseAdr.asm"

        CPU i386
        BITS 32

        WordBitShift    equ     2
        WordSize        equ     (1 << WordBitShift)


        %ifdef elf
        ;;  Based on http://www.muppetlabs.com/~breadbox/software/tiny/revisit.html

        ;; org     0x08048000 linux default
        org     baseAdr0

        hereSize        equ     0x10000 ; 64kiB
ehdr:
        db      0x7F, "ELF", 1, 1, 1    ; e_ident
        times 9 db      0
        dw      2                       ; e_type
        dw      3                       ; e_machine
        dd      1                       ; e_version
        dd      _start                  ; e_entry
        dd      phdr - $$               ; e_phoff
        dd      0                       ; e_shoff
        dd      0                       ; e_flags
        dw      ehdrsz                  ; e_ehsize
        dw      phdrsz                  ; e_phentsize
phdr:   dd      1                       ; e_phnum       ; p_type
                                        ; e_shentsize
        dd      0                       ; e_shnum       ; p_offset
                                        ; e_shstrndx
        ehdrsz        equ     $ - ehdr
        dd      $$                      ; p_vaddr 000xyyyy
        dd      $$                      ; p_paddr xxxxyyyy
        dd      filesize                ; p_filesize
        dd      filesize+hereSize       ; p_memsz 00xxyyyy
        ; dw      0                       ; p_vaddr 0000
        ; mov     eax, startStack         ; p_vaddr yyyy p_addr xxxxyy
        ; cmp     eax, dword filesize     ; p_addr yy    p_filesize
        ; jmp     part2                   ; filesize
        ; dw      1 ; dd      filesize+hereSize           ; p_memsz 00xxyyyy at least 65k
        dd      7                       ; p_flags = RWX
        dd      0x1000                  ; p_align i.e. page align
        phdrsz        equ     $ - phdr

        %else
        org baseAdr2
        %endif ; elf

        %define xt2pfa(adr) (xt_ %+ adr + WordSize)

        %macro NEXT 0
        lodsd
        jmp     eax
        %endm

        %xdefine last 0

        ;; HEADER label
        ;; HEADER label, headerVTnoname
        ;; HEADER over, headerVTcolon, 2
        ;; HEADER '!', fetch, headerVTprim, 3
        %macro  HEADER  1-4 headerVTprim
        %if %0 == 1             ; noname with default headerVT
        align   WordSize
xt_%1:
        dd      headerVTnoname
        %endif
        %if %0 == 2             ; noname with headerVT as parameter
        align   WordSize
xt_%1:
        dd      %2
        %endif
        %if %0 == 3
        times   %3      db  'Z'
        %xdefine %%name   " %+ %1 %+ "
        %error "This does not work!"
        db      %%name
        %strlen %%len   %%name
        %%prealign      equ $
        align   WordSize
        times   (($-%%prealign)*0xFFFFFFFF)     dq      0xdeadbeefdeadbeef
        dd      %%len
        dd      last
        %xdefine        last    xt_%1
xt_%1:
        dd      %2
        %endif
        %if %0 == 4
        times   %4      db  'Z'
        db      %1
        %strlen %%len   %1
        %%prealign      equ $
        align   WordSize
        times   (($-%%prealign)*0xFFFFFFFF)     dq      0xdeadbeefdeadbeef
        dd      %%len
        dd      last
        %xdefine        last    xt_%2
xt_%2:
        dd      %3
        %endif
        %endm

_start:
        mov     ebp, initial_return_stack
        mov     esi, interpreter
        NEXT
        ;lodsd
        ;jmp     eax

        align   WordSize
interpreter:
        dd      xt2pfa(zero), xt2pfa(1plus), xt2pfa(drop)
        dd      xt2pfa(call), xt2pfa(tick)
        dd      xt2pfa(execute)
        dd      xt2pfa(branch)
        dd      interpreter - $

        HEADER  '!', store, headerVTprim, 3
        pop     ebx
        pop     dword [ebx]
        NEXT

        HEADER  '@', fetch, headerVTprim, 1
        pop    ebx
        push   dword [ebx]
        NEXT

        HEADER  'psp', psp, headerVTprim, 3
        push    esp
        NEXT

        HEADER  '-', sub, headerVTprim, 3
        pop     eax
        sub     [esp], eax
        NEXT

        HEADER  '1+', 1plus, headerVTprim, 3
        pop     eax
        inc     eax
        push    eax
        NEXT

        HEADER  '2*', 2mul, headerVTprim, 0
        pop     eax
        shl     eax, 2
        push    eax
        NEXT

        HEADER  'execute', execute, headerVTprim, 1
        pop     ebx             ;ebx := xt
        mov     eax, [ebx]      ;eax := headerVT
        jmp     [eax]           ;jmp cfa
        ;jmp     eax           ;jmp cfa

        HEADER  'lit@exec', litFetchExec, headerVTprim, 3
        lodsd                   ;eax := pfa
        push    dword [eax]     ;push xt
        ;;push    eax
        jmp     xt2pfa(execute)

        HEADER  'dodefer', dodefer, headerVTprim, 0
        add     ebx, WordSize
        push    dword [ebx]
        jmp     xt2pfa(execute)

        HEADER  'dodoes', dodoes, headerVTprim, 3
        add     ebx, WordSize
        push    ebx
        add     eax, 4*WordSize
        jmp     xt2pfa(litFetchExec)

        HEADER  'call', call, headerVTprim, 3
        lodsd                   ;eax := xt
call_docolon:
        xchg    esp, ebp
        push    esi             ;rsp.push esi
        xchg    esp, ebp
        xchg    eax, esi        ;esi := xt
        NEXT

        HEADER  'docolon', docolon, headerVTprim, 3
        xchg    eax, ebx
        add     eax, WordSize
        jmp     call_docolon

        HEADER  'lit', lit, headerVTprim, 3
        lodsd
        push    eax
        NEXT

        HEADER  'lit@', litFetch, headerVTprim, 3
        lodsd
        push    dword [eax]
        NEXT

        HEADER  'drop', drop, headerVTprim, 2
        pop     eax
        NEXT

        HEADER  '2drop', 2drop, headerVTprim, 3
        pop     eax
        pop     eax
        NEXT

        HEADER  'branch', branch, headerVTprim, 1
        add     esi, [esi]
        NEXT

        HEADER  '0branch', 0branch, headerVTprim, 0
        pop     eax
        test    eax, eax
        jz      xt2pfa(branch)
        lodsd                   ;skip offset
        NEXT

        HEADER  'exit', exit, headerVTprim, 3
        xchg    esp, ebp
        pop     esi
        xchg    esp, ebp
        NEXT

        HEADER  "'", tick, headerVTcolon, 3
        dd      xt2pfa(litFetchExec), xt2pfa(parsename)
        dd      xt2pfa(litFetchExec), xt2pfa(perceive)
        dd      xt2pfa(litFetchExec), xt2pfa(Qtoken2xt)
        dd      xt2pfa(exit)

        HEADER  'parse-name', parsename, headerVTdefer, 2
        dd      xt_parseName0
        HEADER  'perceive', perceive, headerVTdefer, 0
        dd      xt_perceive0
        HEADER  '?token>xt', Qtoken2xt, headerVTdefer, 3
        dd      xt_noop

        db      'ZZZZperc'
        HEADER  perceive0, headerVTnoname
        ;; ( d-txt -- nt ; in general: k*x tt | d-txt 0 )
        dd      xt2pfa(lit), xt2pfa(latest)
        .loop:                  ; BEGIN
        dd      xt2pfa(fetch), xt2pfa(dup), xt2pfa(toR)
        dd      xt2pfa(lit), 2*WordSize, xt2pfa(sub)
        dd      xt2pfa(dup), xt2pfa(fetch), xt2pfa(dup)
        dd      xt2pfa(toR), xt2pfa(sub), xt2pfa(fromR)
        dd      xt2pfa(twoOver), xt2pfa(strEq)
        dd      xt2pfa(0branch)
        dd      .notFound - $   ;IF
        dd      xt2pfa(2drop), xt2pfa(fromR), xt2pfa(exit)
        .notFound:              ;THEN
        dd      xt2pfa(fromR), xt2pfa(cell), xt2pfa(sub)
        dd      xt2pfa(branch)
        dd      .loop - $
        ;; no exit as it ends in AGAIN

        HEADER  noop, headerVTnoname
        dd      xt2pfa(exit)

        HEADER  '>r', toR, headerVTprim, 2
        pop     eax
        xchg    esp, ebp
        push    eax
        xchg    esp, ebp
        NEXT

        HEADER  'r>', fromR, headerVTprim, 1
        xchg    esp, ebp
        pop     eax
        xchg    esp, ebp
        push    eax
        NEXT

        HEADER  'streq', strEq, headerVTprim, 2
        ;; ( d-txt0 d-txt1 -- 1|0|-1 )
        xor     edx, edx        ;edx := 0
        pop     eax             ;len1
        pop     ebx             ;&str1
        pop     ecx             ;len0
        cmp     eax, ecx
        pop     eax             ;&str0
        jne     .notEQ
        xchg    eax, esi
        xchg    ebx, edi
        repe    cmpsb
        xchg    eax, esi
        xchg    ebx, edi
        jne     .notEQ
        dec     edx             ;edx := -1
        .notEQ:
        push    edx
        NEXT

        HEADER  'streqIC', strEqIC, headerVTprim, 0
        ;; ( d-txt0 d-txt1 -- 1|0|-1 )
        xor     edx, edx        ;edx := 0
        pop     eax             ;len1
        pop     ebx             ;&str1
        pop     ecx             ;len0
        cmp     eax, ecx
        pop     eax             ;&str0
        jne     .notEQ
        xchg    eax, esi
        xchg    eax, edi
        .loop:
        test    ecx, ecx        ;ecx != 0 ?
        jz      .eqRestore
        dec     ecx
        lodsb
        cmp     al, 'a'
        jl      .notLower0
        cmp     al, 'z'
        jg      .notLower0
        sub     al, 'a'-'A'
        .notLower0:
        mov     ah, al

        xchg    ebx, esi
        lodsb
        cmp     al, 'a'
        jl      .notLower1
        cmp     al, 'z'
        jg      .notLower1
        sub     al, 'a'-'A'
        .notLower1:

        cmp     ah, al
        je     .loop

        xchg    edi, esi
        jmp     .notEQ
        .eqRestore:
        xchg    edi, esi
        dec     edx             ;edx := -1
        .notEQ:
        push    edx
        NEXT

        db      'ZZZZpars'
        HEADER  parseName0, headerVTnoname
        ;; ( " word" -- d-txt )
        dd      xt2pfa(litFetch), xt2pfa(here)
        .whiteSpace:
        dd      xt2pfa(nextChar), xt2pfa(dup), xt2pfa(lit), ' ' + 1
        dd      xt2pfa(lower)
        dd      xt2pfa(0branch)
        dd      .whiteSpaceEnd - $
        dd      xt2pfa(drop)
        dd      xt2pfa(branch)
        dd      .whiteSpace - $
        .whiteSpaceEnd:
        dd      xt2pfa(cComma)
        dd      xt2pfa(nextChar), xt2pfa(lit), ' '
        dd      xt2pfa(over), xt2pfa(lower)
        dd      xt2pfa(0branch)
        dd      .endOfString - $
        dd      xt2pfa(branch)
        dd      .whiteSpaceEnd - $
        .endOfString:
        dd      xt2pfa(drop)
        dd      xt2pfa(litFetch), xt2pfa(here)
        dd      xt2pfa(over), xt2pfa(sub) ; ( &:str u:len )
        dd      xt2pfa(over), xt2pfa(lit), xt2pfa(here), xt2pfa(store)
        dd      xt2pfa(exit)

        HEADER  'dup', dup, headerVTprim, 1
        mov     eax, [esp]		; duplicate top of stack
        push    eax
        NEXT

        HEADER  '<', lower, headerVTprim, 0
        xor     eax, eax
        pop     ebx
        pop     ecx
        cmp     ecx, ebx
        jnl     .False
        dec     eax
        .False:
        push    eax
        NEXT

        HEADER  'next-char', nextChar, headerVTprim, 2
        push    byte 3          ;sycall write
        push    esp
        pop     ecx             ; store byte directly onto the stack
        pop     eax             ; linux syscall 3
        push    byte 1
        pop     edx             ; length is 1 byte
        xor     ebx, ebx        ; fd is 0, i.e. stdin
        push    ebx
        int     0x80
        NEXT

        ;; : c, here @ dup 1+ here ! ! ;
        HEADER  'c,', cComma, headerVTprim, 2
        mov     ebx, [xt2pfa(here)]
        pop     eax
        mov     [ebx], al
        inc     ebx
        mov     [xt2pfa(here)], ebx
        NEXT

        HEADER  'here', here, headerVTvar, 1
        dd      initial_here

;;; non-elementary words
        ;; : 0 psp psp @ - ;
        HEADER '0', zero, headerVTprim, 3
        push    0
        NEXT

        ;; : over psp cell + @ ;
        HEADER  'over', over, headerVTprim, 3
        mov     eax, [esp+1*WordSize]
        push    eax
        NEXT

        ;; : 2dup over over ;
        ;; : 2over psp cell 2* + @ psp cell 2* + @ ;
        HEADER  '2over', twoOver, headerVTprim, 3
        mov     eax, [esp+3*WordSize]
        push    eax
        mov     eax, [esp+3*WordSize]
        push    eax
        NEXT

        HEADER  'swap', swap, headerVTprim, 3
        pop     eax
        pop     ebx
        push    eax
        push    ebx
        NEXT

        ;; : cell psp psp - ;
        HEADER  'cell', cell, headerVTprim, 1
        push    byte WordSize
        NEXT

        ;; : variable create 1 cells allot ;
        HEADER  'dovar', dovar, headerVTprim, 2
        add     ebx, WordSize
        push    ebx
        NEXT

        ;; ? : value create 1 cells allot ;
        HEADER  'dovalue', dovalue, headerVTprim, 2
        add     ebx, WordSize
        push    dword [ebx]
        NEXT

        HEADER  'doprim', doprim, headerVTprim, 2
        add     ebx, WordSize
        jmp     ebx

        HEADER  'emit', emit, headerVTprim, 3
        push    byte 4
        pop     eax             ;syscall 4 is write
        push    byte 1
        pop     ebx             ;file descriptor 1 is stdout
        push    byte 1
        pop     edx             ;length 1
        push    esp             ;put buffer on stack
        pop     ecx
        int     0x80
        NEXT

        HEADER  'here@', hereFetch, headerVTcolon, 3
        dd      xt2pfa(litFetch), xt2pfa(here), xt2pfa(exit)

;;; latest must be, as the name says it, latest:

        HEADER  'latest', latest, headerVTvar, 2
        dd      last

; +-----------------+
; | pad             | 0 / cell-1 bytes
; | name            | 1 / 2^(8*cell-2) bytes
; | length          | 1 cell
; | link            | 1 cell
; +=================+ ^^^ only for named words; vvv enough for noname words
; | vt              | 1 cell ; target for xt/nt
; | parameter-field | 0 / EndOfMemory cells
; +-----------------+

; todo: future for garbage collection
; +-----------------+
; | pad             | 0 / cell-1 bytes
; | name            | 1 / 2^(8*cell-2) bytes
; | length          | 1 cell
; | link            | 1 cell
; +=================+ ^^^ only for named words; vvv enough for noname words
; | info&vt         | 1 cell ; target for xt/nt
; | size-field      | 0 / 2^(8*cell) bytes
; | parameter-field | 0 / EndOfMemory cells
; +-----------------+


; +---- xts in vt ---+
; |0  cfa            | ( addr:code-field-dtc )
; |1  compile,       | ( xt -- ; D: semantic of xt )
; |2  pfs!           | ( i*x xt -- ) aka (to) defer!
; |3  pfs@           | ( xt -- i*x   ) aka defer@ value
; |4  does/extra     | ( i*x addr:body -- j*x )
; |5  name>interpret | ( nt -- nt|0 ; 0 if compile-only )
; |6  name>compile   | ( nt -- xt1 xt2 )
; |7  name>string    | ( nt -- d-txt u )
; |8  name>link      | ( nt1 -- nt2|0 ; 0 if unnamed word )
; +------------------+
; | wordlist:        |
; |9  reveal         | ( nt a:self -- )
; |10 resolve        | ( d-txt a:self -- nt tt-nt | d-txt 0 )
; |11 eq             | ( d-txt0 d-txt1 -- t/f )
; |12 traverse       | ( i*x xt a:self -- j*x )
; |13 hide           | ( d-txt a:self -- )
; +------------------+


        align WordSize
headerVTprim:
        dd      xt2pfa(doprim)
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0

headerVTcolon:
        dd      xt2pfa(docolon)
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0

headerVTnoname:
        dd      xt2pfa(docolon)
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0

headerVTdefer:
        dd      xt2pfa(dodefer)
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0

headerVTvar:
        dd      xt2pfa(dovar)
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0
        dd      0

        filesize        equ     $ - $$
bottom_return_stack:
        return_stack_size       equ     0x100
        initial_return_stack    equ     bottom_return_stack + return_stack_size
        initial_here            equ     initial_return_stack

;;; minimal word set, cf.
;;; https://wiki.forth-ev.de/doku.php/en:words:kernel_embedded:minimum_word_set
;;; - dodoes (¿  or call for pctc?)
;;; - @ !
;;; - >r r>
;;; + or 2*
;;; NAND
;;; ?branch (or 0branch)
;;; execute
;;; ;S
