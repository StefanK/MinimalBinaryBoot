;;; Copyright (C) 2023 Dr. Stefan Karrmann
;;; Distributed unter GPL-3 licence.
;;; convert base16 data into binary data
;;; whitespace is all characters below A
;;; line comment starts with any character above P
;;; 0123456789ABCDEF
;;; ABCDEFGHIJKLMNOP

CPU i386
BITS 32

%define WordSize 4
%define CAX  eax
%define CBX  ebx
%define CCX  ecx
%define CDX  edx
%define CSP  esp
%define CBP  ebp
%define CSI  esi
%define CDI  edi
%define LODSWS  lodsd
%define STOSWS  stosd
%define DWS dd
%define WSword dword

;  Based on http://www.muppetlabs.com/~breadbox/software/tiny/revisit.html

;; default: org     0x08048000
%include "baseAdr.asm"

;; c.f. https://unix.stackexchange.com/questions/421750/where-do-you-find-the-syscall-table-for-linux
%define syscall_exit    1 ; oder 64 oder 1
%define syscall_read    3 ; oder 63
%define syscall_write   4 ; oder 64 oder 1

org     baseAdr1
SECTION .all    start=baseAdr1, align=1
start2:
        push    byte 1
        db      0x3C            ; i.e. cmp al, <push EAX>-op-code
nextOctet:
        push    EAX

readOctet:
        ;; linux read(0, message, 1)
        ;; needs EAX, EBX, ECX, EDX
	xor     EBX, EBX          ; hex: 31DB file descriptor 0 is stdin
	mov     EDX, EBX          ; hex: 89DA
	inc     EDX               ; hex: 42  1 = number of bytes
        push    EAX
        push    ESP
        pop     ECX
        push    syscall_read
        pop     eax
	int     0x80              ; hex: CD80 invoke operating system to do the write
        ;; We don't check the return code in eax. We assume 1.
        pop     EAX
jmp0:
        or      EBP, EBP
        jz      next
comment:
        mov     EBP, EDI
        cmp     al, 0x0A
        jne     short readOctet
        xor     EBP, EBP
next:
        cmp     al, 'Z'
        je      short buffer
        sub     al, 'A'
        jl      short readOctet           ; skip "white space"
        cmp     al, 'P'-'A'
        jg     short comment
handleBase16:
        xchg    EAX, EBX
        pop     EAX
        shl     EAX, 4
        or      al, bl
        or      ah, ah
        jz      short nextOctet
        stosb                   ; es:[EDI], al
        jmp     short start2

        filesize      equ     $ - $$
buffer:
;; hack to throw an error, if the sizeOfLoad1 changes:
        times (($ - $$ - sizeOfLoad1)*0xFFFFFFFF) dd 0xDeadBeef
