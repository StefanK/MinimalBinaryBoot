(
We assume a wordsize of at least 16 bit and
size of word == size of pointer.

The basic memory building block is a double word. This gives us two
bits for tagging the data:
+n--------10+n--------10+
| car     00| cdr     00|
| imm-int 01| imm-o   11| immediate data in car, other are #f #t #nil #ignore #inert char
| infoPtr 10| data0..n  | n in 2*N
| fwdPtr  10| Ptr     00|
| valueP  10| xxxxxxxxxx| one char, #f, #t
+n--------10+n--------10+

Rational: Pairs will be quite common in scheme, therefore they get the
basic representation with tag == %00, such that we can dereference
them directly. The immediate value allows almost normal integers, we
reserve only two bits. The most generalt type is that with an info
pointer. Since all data must be aligned to double words, at least, the
payload must be an even number of words or one padding word.

The info-struct:
+----------+
| infoPtr  |
| scan     |  a:toSpaceScanArea --
| evacuate |  a:fromSpace --
| param    | e.g. len
+----------+

basic info struct is:
+---------------------+
| @ basic info struct |
| scan-basic          | 3 0 ?DO dup points2FromSpace IF dup copy THEN cell+ LOOP
| evacutate-basic     | 3 0 ?DO dup points2FromSpace IF dup copy THEN cell+ LOOP
| 0                   |
+---------------------+

First, we use the Cheney garbage collector.
We could also use the garbage collector of Baker for serial real time gc.

With only two bits for tagging we have no space for the mark bit for a
mark-and-X garbage collector, e.g.:
+n--------10+n--------10+
| car     0m| cdr     00|
| imm     1m| cdr     00|
| infoPtr !m| data0..n  | n in 2*N
+n--------10+n--------10+
A solution would be a bitmap block which contains the mark bits. For n
double words we need n/bitsPerWord words.

The root set is a single variable scm>root, which points to a list of
the form (internals current-continuation to-eval).
For simplicity internals is a list of the form (atom0 atom1 ...)
)

struct
    \ not dynamic: ptr% field info>infoPtr
    ptr% field info>scan ( a:toSpace -- )
    ptr% field info>evacuate ( a:fromSpace -- )
    ptr% field info>evaluate ( a:lisp a:env a:cont -- a:res a:cont )
    ptr% field info>display ( a:lisp -- )
end-struct info%

create info-symbol info% size allot
\ | info-symbol | octet-len | octets ... | pad |

: symbol-scan ( a:toSpace -- )
    cell+ dup @ cell+ 1- cell/ 1+ cells + scm>scan !
; ' symbol-scan info-symbol info>scan !

: symbol-evacuate ( a:fromSpace -- )
    scm>free @ over
    cell+ @ cell+ 1- cell/ 1+ 1+ cells dup >r cmove>
    r> scm>free +!
; ' symbol-evacuate info-symbol info>evacuate !

: symbol-evaluate ( a:lisp a:env a:cont -- a:res a:cont )
    >r lookup r>
; ' symbol-evaluate info-symbol info>evaluate !

' 0 alias nil
: #f      %00111 ;
: #t      %01011 ;
: #ignore %01111 ;
: #inert  %10011 ;
: tag-mask   %11 ;
: null? ( a:lisp -- f )
    0= ;
: pair? ( a:lisp -- f )
    tag-mask and 0= ;
: atom? ( a:lisp -- f )
    pair? not ;
: char? ( a:lisp -- f )
    [ $FF00 invert ]l and tag-mask = ;
: #f?   ( a:lisp -- f )
    #f = ;
: #t?   ( a:lisp -- f )
    #t = ;
: #ignore?   ( a:lisp -- f )
    #ignore = ;
: #inert?   ( a:lisp -- f )
    #inert = ;
: car  ( a:lisp -- a:lisp )
    @ ;
: cdr ( a:lisp -- a:lisp )
    cell+ @ ;
: caar ( a:lisp -- a:lisp )
    @ @ ;
: cdar ( a:lisp -- a:lisp )
    @ cell+ @ ;

variable scm>error-cont

: lookup-frame ( a:lisp a:vars a:vals a:env a:cont -- a:val a:cont )
    >r
    BEGIN
        over null?
        IF dup null?
            IF 3drop rdrop scm>error-cont @ exit THEN
            -rot 2drop dup caar over cdar rot
        ELSE
            2 pick car eq?
            IF drop car nip nip r> exit THEN
            rot cdr rot cdr rot
        THEN
    AGAIN
;

: lookup ( a:lisp a:env a:cont -- a:val a:cont )
    2>r scm-nil dup 2r> lookup-frame ;

create info-vau info% size allot
\ | info-vau | pt | env-symbol | body |
: vau-scan ( a:toSpace -- )
    4 1 ?DO
        ...
        LOOP
;
: vau-evacuate ( a:fromSpace -- )
;
: vau-evaluate ( a:lisp a:env a:cont -- )
;


variable scm>max#mem
variable scm>toSpace
variable scm>fromSpace
variable scm>free
variable scm>root
variable scm>scan

: get-tag ( a:adr -- u:tag )
    @ #3 and ;

: mask-tag ( a:adr0 -- a:adr1 )
    [ #3 invert ]l and ;

defer cheney-gc

: scm>hasSpace ( u -- t/f )
    2* cells  scm>free @ +   scm>toSpace @ scm>max#mem @ +  < ;

: scm>alloc ( u:#pairs -- a:toSpace )
    2* cells
    dup scm>hasSpace 0= IF
        cheney-gc
        dup scm>hasSpace 0= IF OOM throw THEN
    THEN
    scm>free @ swap over + scm>free !
;

: evacuate-pair ( a:fromSpace0 )
    2 cells scm>alloc ( a:fromSpace0 a:toSpace )
    over @ over !
    cell+ swap cell+ @ swap !
;

: evacuate ( a:fromSpace -- )
    dup get-tag
    2 = IF \ infoPtr
        dup @ info>evacuate @ execute
    ELSE \ pair
        evacuate-pair
    THEN
;
: scan-cxr ( a:adr0 -- a:adr1 )
    dup @ get-tag
    0= IF \ not integer but pointer
        dup @ ( a:adr0 a:adr-cxr )
        dup scm>fromSpace @ dup scm>max#mem @ +
        within ( x a b -- f )
        IF \ fromSpace
            evacuate
        THEN
    THEN
    cell+
;

:noname \ cheney-gc ( -- )
    \ flip
    scm>toSpace @
    scm>fromSpace @
    dup scm>free !
    dup scm>scan !
    scm>toSpace !
    scm>fromSpace !
    scm>root @ evacuate
    BEGIN
        scm>scan @ dup scm>free @ < WHILE
            dup get-tag 2 = IF \ infoPtr
                @ mask-tag info>scan @ execute
            ELSE scan-cxr scan-cxr info>scan !\ pair
            THEN
    REPEAT
; IS cheney-gc

: make-scheme ( u.mem )
    dup alloc dup
    scm>free !
    scm>toSpace !
    dup alloc scm>fromSpace !
    scm>max#mem !
;

\ primitive ( i*x env continuation0 -- j*x continuation1 )
: prim-cons ( a:car a:cdr env c0 -- a:pair c0 )
    >r drop 1 scm>alloc
    2dup cell+ ! !
    r>
;
: prim-vau ( a:pt a:env a:body env c0 -- a:vau c0 )

(
 (define quote (vau (x) _ x))
 (define get-env (vau _ e e))
 (define list (vau x e (eval x e)))
 (define car (vau (x . _) e (eval x e)))
 (define cdr (vau (_ . x) e (eval x e)))
 (define cons (vau (a b) e (eval (quote (a . b)) e)))

 (define vau ; depends on eval
   (vau (pt env body) e0
        (list (quote vau) pt env body)))

 (define lambda
   (vau (pt body) e0
        (vau args e1
             (eval body
                   (eval (car (list (get-env)
                                    (eval (list define pt (eval args e1))
                                          (get-env))))
                         (eval ((vau _ _ (get-env))) e0))))))
 (define wrap  ; as long as we don't need unwrap
   (vau (f) e0
        (vau args e1
             (eval (list (eval f e0) (eval args e1)) e0))))

 (define lambda2
   (vau (pt body) e0
        (wrap (eval (list vau (eval pt e0) _ (eval body e0))))))

;; sk
 (define (eval expr env)
   (cond
    ((symbol? expr) (lookup expr env)) ; symbol
    ((atom? expr) expr)               ; eval to self
    (#t (let ((f (eval (car expr) env)))
          (cond
           ((atom? f) (apply f (cdr expr) env)) ; primitive or compiled
           ((eq? (car f) 'vau)             ; (vau pt env body)
            (eval body
                  (make-frame (cons (cadr f) (caddr f))
                              (cons (cdr expr) env)
                              env))))))))
 (define (eval-cont expr env cont)
   (cond
    ((symbol? expr) (cont (lookup expr env))) ; symbol
    ((atom? expr) (cont expr))                ; eval to self
    (#t (eval-cont (car expr) env
              (lambda (f)
                   (cond
                    ((atom? f) (apply f (cdr expr) env cont)) ; primitive or compiled
                    ((eq? (car f) 'vau)             ; (vau pt env body)
                     (eval (cadddr f)
                           (make-frame (cons (cadr f) (caddr f))
                                       (cons (cdr expr) env)
                                       env)
                           cont))))))))

 (define (eval-loop (c-val))
     (eval-loop ((car c-val) (cadr c-val))))

 (define (eval-step expr env cont)
   (cond
    ((symbol? expr) (list cont (lookup expr env))) ; symbol
    ((atom? expr) (list cont expr))                ; eval to self
    (#t (eval-step (car expr) env
         (lambda (f)
           (cond
            ((atom? f) (apply f (cdr expr) env cont)) ; primitive or compiled
            ; most of the times (list cont (f env (cdr expr)))
            ((eq? (car f) 'vau)             ; (vau pt env body)
             (eval-step
              (cadddr f)                ;body
              (make-frame (cons (cadr f) (caddr f)) ;parameter-tree environment
                          (cons (cdr expr) env)     ;argument-tree environment
                          env)
              cont))))))))

 (define (make-frame param-tree arg-tree parent-env)
   (cons (new-frame param-tee arg-tree (cons '() '())) parent-env))

 (define (new-frame pt at pas)
   (cond
    ((or
      (eq? '_ pt)
      (and (eq? '() pt) (eq? '() at)) ; redundant
      (and (atom? pt) (eq? pt at)))
     pas)
    ((and (pair? pt) (pair? at))
     (new-frame (cdr pt) (cdr at) (new-frame (car pt) (car at) pas)))
    (else (error "non matching parameter and argument trees"))))

 (define (lookup sym e k)
   (lookup-frame sym '() '() env k))
 (define (lookup-frame sym vars vals env k) ; todo: src-loc
   (if (pair? vars)
       (if (equal? (car vars) sym)
           (k (car vals))
         (lookup-frame sym (cdr vars) (cdr vals) env k))
     (if (pair? env)
         (lookup-frame sym (caar env) (cdar env) (cdr env) k)
       (error "Variable not found"))))
;https://blog.theincredibleholk.org/blog/2013/11/27/continuation-passing-style-interpreters/

 ; https://jeapostrophe.github.io/conferences/2013-tfp/proceedings/tfp2013_submission_20.pdf
 (define (eval e a)
   (cond
    ((atom e) (assoc e a)) ; symbol
    ((atom (car e)) ; (fn . args ) i.e. primitive
     (eval (cons (assoc (car e) a)
                 (cdr e))
           a))
    ((eq (caar e) 'vau) ; (('vau pt env body) . args )
     (eval (caddar e)
           (cons (cons (cadar e) 'env)
                 (append (pair (cadar e) (cdr e))
                         a))))))

def eval(x, env):
    "Evaluate an expression in an environment."
    if isa(x, Symbol):    # variable reference
        return env[x]
    elif isa(x, list):    # procedure call
        proc = eval(x[0], env)
        if hasattr(proc, '__call__'):
            return proc(env,*x[1:])
        raise ValueError("%s = %s is not a procedure" %
                        (to_string(x[0]),to_string(proc)))
    return x # literal constant

def vau(clos_env, vars, call_env_sym, body):
    def closure(call_env, *args):
        new_env = Env(zip(vars, args), self.clos_env)
        new_env[call_env_sym] = call_env
        return eval(self.body, new_env)
    return closure

def defvar(v,var,e):
    val = eval(e, v)
    v[var] = val
    return val # return #inert

def setvar(v,var,e):
    val = eval(e, v)
    env.find(var)[var] = val
    return val

def cond(v,*x):
    for (p, e) in x:
        if eval(p, v):
            return eval(e, v)
    raise ValueError("No Branch Evaluates to True")

def sequence(v,*x):
    for e in x[:-1]: eval(e, v)
    return eval(x[-1], v)

def wrap(v,p):
    p = eval(p,v)
    return lambda v,*x: p(v,*[eval(expr,v) for expr in x])

global_env = Env({
    '+': lambda v,x,y:eval(x,v) + eval(y,v),
    '-': lambda v,x,y:eval(x,v) - eval(y,v),
    '*': lambda v,x,y:eval(x,v) * eval(y,v),
    '/': lambda v,x,y:eval(x,v) / eval(y,v),
    '>': lambda v,x,y:eval(x,v) > eval(y,v),
    '<': lambda v,x,y:eval(x,v) < eval(y,v),
    '>=': lambda v,x,y:eval(x,v) >= eval(y,v),
    '<=': lambda v,x,y:eval(x,v) <= eval(y,v),
    '=': lambda v,x,y:eval(x,v) == eval(y,v),
    'eq?': lambda v,x,y:
    (lambda vx,vy: (not isa(vx, list)) and (vx == vy))(eval(x,v),eval(y,v)),

    'cons': lambda v,x,y:[eval(x,v)]+eval(y,v),
    'car': lambda v,x:eval(x,v)[0],
    'cdr': lambda v,x:eval(x,v)[1:],
    'list': lambda v,*x:[eval(expr, v) for expr in x],
    'append': lambda v,x,y:eval(x,v)+eval(y,v),
    'len':  lambda v,x:len(eval(x,v)),
    'symbol?': lambda v,x:isa(eval(x,v),Symbol),
    'list?': lambda v,x:isa(eval(x,v),list),
    'atom?': lambda v,x:not isa(eval(x,v), list),
    'True':  True,
    'False': False,
    'if':  lambda v,z,t,f: eval((t if eval(z,v) else f), v),
    'cond':  cond,
    'define': defvar,
    'set!':  setvar,
    'vau':  vau,
    'begin': sequence,
    'eval': lambda v,e,x: eval(eval(x,v),eval(e,v)),
    'wrap': wrap
})
)
: read ( -- o )
;
: eval ( o0 -- o1 )
;
: print ( o -- )
;

: repl ( -- ) \ needs make-schem
    BEGIN read eva

:
