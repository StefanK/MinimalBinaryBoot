; Copyright (C) 2021 Dr. Stefan Karrmann
; Distributed unter GPL-3 licence.
; 0x30 or 48 bytes of x86 binary code
; Based on https://niedzejkob.p4.team/bootstrap/miniforth/
; comfort extension
; - use first two bytes as size

CPU i386

%ifdef bios16
  %undef linux32
  %ifdef serial
    %undef keyboard
  %else
    %define keyboard
    %undef serialInit
    %undef serialErrorCheck
  %endif
%else
  %define linux32
%endif

%ifdef linux32
  BITS 32
  %define WordSize 4
  %define EAX  eax
  %define EBX  ebx
  %define ECX  ecx
  %define EDX  edx
  %define ESP  esp
  %define EBP  ebp
  %define ESI  esi
  %define EDI  edi
  %define LODSWS  lodsd
  %define STOSWS  stosd
  %define DWS dd
  %define WSword dword
%endif ; linux32

%ifdef bios16
  BITS 16
  %define WordSize 2
  %define EAX  ax
  %define EBX  bx
  %define ECX  cx
  %define EDX  dx
  %define ESP  sp
  %define EBP  bp
  %define ESI  si
  %define EDI  di
  %define LODSWS  lodsw
  %define STOSWS  stosw
  %define DWS dw
  %define WSword word
%endif ; bios16

bufferSize      equ 512  ; a boot block - Adjust to taste. Beware of fenceposting.
PageSize        equ 0x1000

%ifdef linux32
SECTION .data
  resb  PageSize * 0x20         ; for stack of loaded code

SECTION .text   write
%endif ; linux32

%ifdef bios16
SECTION .text
; On x86, the boot sector is loaded at 0x7c00 on boot. In segment
; 0x0500, that's 0x7700 (0x0050 << 4 + 0x7700 == 0x7c00).
%ifdef ORG
ORG   0x7700
%endif ; ORG
; Set CS to a known value by performing a far jump. Memory up to
; 0x0500 is used by the BIOS. Setting the segment to 0x0500 gives
; the monitor and its loaded code an entire free segment to work with.
  jmp   0x0050:_start           ; hex: EAXXXX5000
%endif ; bios

global _start
_start:
;  we do not use the stack, linux initializes the segment registers
%ifdef bios16
  ;  we do not use the stack, but of this:
  push  cs                      ; hex:
  pop   es                      ; hex: 07 - needed by stosb

%ifdef serialInit
  mov ax, 0x00e3        ; ah = 0xe3 = 0b111000111
  ; i.e. bit2 1 and 0: 8 bit word length
  ;      bit 2       : 1 stop bit
  ;      bits 3 and 4: no parity
  ;      bits 5 to 7: baud rate 9600
  xor dx, dx
  int 0x14
%endif ; serialInit

%endif ; bios16
%ifdef linux32
message:                        ; we store the input octet in the initial code.
%endif ; linux32

  std                           ; hex: FD
  mov   EDI, buffer + bufferSize - 1 ; hex: bfFE010000 ; Adjust to taste. Beware of fenceposting.
output_loop:

; initialize collector of input with shifting flag:
%ifdef linux32                  ; linux read clobbers ax, bc, cx, dx
  push  byte 1
;  mov   esi,1                   ; hex: BB0100 ; collector of input with ready flag
%endif ; linux32
%ifdef bios16
  xor   bx, bx
  inc   bx
  ;mov   bx, 1                   ; hex: BB0100
  ;mov   bl, 1                   ; hex: B301
%endif ; bios16

input_loop:

%ifdef keyboard
  mov   ah, 0                      ; hex: b400
  int   0x16                       ; hex: cd16
%endif ; keyboard
%ifdef serial
.serial:
  mov   ah,1                      ; hex: b401
  xor   cx, cx                     ; hex: 31c9
  int   0x14                      ; hex: cd14
%ifdef serialErrorCheck
  test  ah, ah                  ; TODO make this error check sense?
  jnz   .serial
%endif ; serialErrorCheck
%endif ; serial

%ifdef linux32
        ;; c.f. https://unix.stackexchange.com/questions/421750/where-do-you-find-the-syscall-table-for-linux
        %define syscall_exit    1 ; oder 64 oder 1
        %define syscall_read    3 ; oder 63
        %define syscall_write   4 ; oder 64 oder 1

  ; read(0, message, 1)
  xor   EBX, EBX                ; hex: 31DB file handle 0 is stdin
  mov   EDX, EBX                ; hex: 89DA
  inc   EDX                     ; hex: 42  1 = number of bytes
  push  EAX
  push  ESP
  pop   ECX
  push  byte syscall_read
  pop   EAX
  int   0x80                    ; hex: CD80 invoke operating system to do the write
  ;; We don't check the return code in eax. We assume 1.
  pop     EAX
%endif ; linux32

  sub   al, '0'                 ; hex: 80EB30

%ifdef linux32
  xchg  EAX, EBX
  pop   EAX
  shl   EAX, 3
  or    al, bl
  push  EAX
  or    ah, ah
  jz    short input_loop
  stosb
  pop   EAX
  jmp   short output_loop
%else
%if 0
  xchg    EAX, EBX
  shl   EAX, 3
  or    al, bl
  or    ah, ah
        mov     EBX, EAX
  jz    short input_loop
  stosb
  jmp   short output_loop
%else
  shl   EBX, 3                  ; hex: C1E003
  or    bl, al                   ; hex: 08D3
  or    bh, bh                  ; hex: 08E4
  ;cmp   ah, 0x00                ; hex: 66 81FBFF00
  jz    short input_loop        ; hex: 74D8
  xchg  ax, bx
  stosb                         ; hex: aa
  jmp   short output_loop       ; hex: EBD2
%endif
%endif

%ifdef old
%ifdef linux32
  mov   EBX, ESI                ; hex: 89F0
%endif ; linux32
  shl   EBX, 3                  ; hex: C1E003
  or    bl, al                   ; hex: 08D3
%ifdef linux32
  mov   ESI, EAX                ; hex: 89D6
%endif ; linux32
  or    bh, bh                  ; hex: 08E4
  ;cmp   ah, 0x00                ; hex: 66 81FBFF00
  jz    short input_loop        ; hex: 74D8
  stosb                         ; hex: aa
  jmp   short output_loop       ; hex: EBD2
  ;jmp   short codeStart         ; hex EBFE , i.e. FE must be the first byte in the loaded code
                                ; as the bytes are reversed, it's actually the last octal code 376
%endif                          ; old
buffer  equ     $ - 1
%ifdef linux32
  resb bufferSize - 1

reservedToBeUsedByLoadedCode:
  resb 0x1000 * 0x20
%endif ; linux32

%ifdef bootSector
  bootSectorSize        equ 512
  resb  bootSectorSize - ($ - $$) - 2
  db    0x55, 0xAA
%endif ; bootSector
