; cipf - character indexed pre-forth
; Copyright 2022 Dr. Stefan Karrmann
; Licenced under GPL-3.

; Register usage:
; SP = parameter stack pointer (grows downwards)
; BP = return stack pointer (grows downwards)
; SI = execution pointer
; DI = indirection table
;
; This Forth, i.e. cipf, uses direct threaded code (DTC) with call
; type. It has as its dictionary a simple lookup table for words of
; one character length.

CPU i386

%ifdef elf
  %undef monitorBinary
  %undef bios16
%endif

%ifdef monitorBinary
  %undef standalone
%else
  %define standalone
%endif

%ifdef bios16
  %undef linux32
  %ifdef serial
    %undef keyboard
  %else
    %define keyboard
  %endif
%else
  %define linux32
%endif

%ifdef linux32
  BITS 32
  %define CellSizeShift   2
  %define CAX  eax     ; cell size register ax/eax, ff.
  %define CBX  ebx
  %define CCX  ecx
  %define CDX  edx
  %define CSP  esp
  %define CBP  ebp
  %define CSI  esi
  %define CDI  edi
  %define LODScell  lodsd
  %define STOScell  stosd
  %define Dcell dd
  %define WordCell dword
  %define saveLinuxStack        1
%endif ; linux32

%ifdef bios16
  BITS 16
  %define CellSizeShift   1
  %define CAX  ax
  %define CBX  bx
  %define CCX  cx
  %define CDX  dx
  %define CSP  sp
  %define CBP  bp
  %define CSI  si
  %define CDI  di
  %define LODScell  lodsw
  %define STOScell  stosw
  %define Dcell dw
  %define WordCell word
%endif ; bios16

%define CellSize  (1 << CellSizeShift)

PageSize        equ 0x1000 ; 4096 bytes
hereSize        equ 16 * PageSize

%define NEXT    jmp short next

  LinuxStart    equ 0x8048060
  BIOSStart     equ 0x7700      ; in segment 0050, in total 0x7c00
%ifdef bios16
  SizeOfMonitor equ 0x20
  StartMonitor  equ BIOSStart
%else
%ifdef linux32
  SizeOfMonitor equ 0x32
  StartMonitor  equ LinuxStart
%ifndef elf
global _start
%endif ; standalone

%else
%error "Unknown architecture"
%endif ; linux32
%endif ; bios16
  StartThis     equ StartMonitor + (SizeOfMonitor - 1)

%ifdef elf
;  Based on http://www.muppetlabs.com/~breadbox/software/tiny/revisit.html

                ;org     0x08048000
                org     0x4cb80000

  ehdr:
                db      0x7F, "ELF", 1, 1, 1    ; e_ident
        times 9 db      0
                dw      2                       ; e_type
                dw      3                       ; e_machine
                dd      1                       ; e_version
                dd      _start                  ; e_entry
                dd      phdr - $$               ; e_phoff
                dd      0                       ; e_shoff
                dd      0                       ; e_flags
                dw      ehdrsz                  ; e_ehsize
                dw      phdrsz                  ; e_phentsize
  phdr:         dd      1                       ; e_phnum       ; p_type
                                                ; e_shentsize
                dd      0                       ; e_shnum       ; p_offset
                                                ; e_shstrndx
  ehdrsz        equ     $ - ehdr
;                dd      $$                                      ; p_vaddr 000xyyyy
;                dd      $$                                      ; p_paddr xxxxyyyy
;                dd      filesize                                ; p_filesize
;                dd      filesize+hereSize                       ; p_memsz 00xxyyyy
                dw      0                                       ; p_vaddr 0000
_start:
                mov     eax, startStack                         ; p_vaddr yyyy p_addr xxxxyy
                cmp     eax, dword filesize                     ; p_addr yy    p_filesize
                jmp     part2                                   ; filesize
                dw      1 ; dd      filesize+hereSize           ; p_memsz 00xxyyyy at least 65k
                dd      7                                       ; p_flags = RWX
                dd      0x1000                                  ; p_align i.e. page align
  phdrsz        equ     $ - phdr

  ; your program here
%endif ; elf

%ifdef monitorBinary
SECTION .text   align=1
%ifdef ORG
ORG StartThis
%endif ; ORG

jumpOffset:                       ; from last instruction of the monitor
  ;db    0x00                     ; jump offset of next instruction to _start
  ;jmp   short _start     ; copy it from this if unsure but comment the next line
  db    CellSize * 7      ; 0x34 where 7 entries are on the startStack
%endif ; monitor Binary

%ifdef standalone
  %ifdef linux32

;SECTION  .data align=1 ; TODO, "wd" ; further TODO simplify sections
;SECTION  .text align=1 ; TODO, "wd" ; further TODO simplify sections
;lowestStack:
;  resb  PageSize

  %endif ; linux32
  %ifdef bios16
SECTION .text   write
        %ifdef ORG
  ORG   StartThis
        %endif ; ORG
  jmp   0x0050:_start           ; initialize cs
  %endif ; bios16
%endif ; standalone

startStack:

  Dcell   Code
  Dcell   PSPFetch
  Dcell   Fetch
  Dcell   Substract
  Dcell   Store
  ; Dcell   ~0      ; true or -1
  ; Dcell   HERE
  ; indirectionTable = Here + CellSize

  PSP0  equ  $ - CellSize
endOfStack:

%ifdef standalone
%ifdef bios16
SECTION .text   write align=1
%endif ; bios16
%endif ; standalone

%ifdef elf
part2:
%else
_start:
%endif ; elf

%ifdef bios16
  push  cs
  push  cs
%ifndef monitorBinary
  push  cs
  pop   es                      ; we need es for di in the monitor
%endif ; monitorBinary
  pop   ds
  pop   ss                      ; if ss is set, cli is active for the next instruction:
%endif ; bios16

  mov   CDI, indirectionTable

%ifdef saveLinuxStack
%ifndef elf
  mov   CAX, startStack
%endif ; elf
  xchg  CAX, CSP
  mov   [CDI], CAX
%else
  mov   CSP, startStack
%endif ; saveLinuxStack

%ifndef linux32
%ifndef standalone
  cld                           ; linux clears direction flag
%endif ; standalone
%endif ; linux32

%ifdef serialInit
   mov ax, 0x00e3        ; ah = 0xe3 = 0b111000111
   ; i.e. bit2 1 and 0: 8 bit word length
   ;      bit 2       : 1 stop bit
   ;      bits 3 and 4: no parity
   ;      bits 5 to 7: baud rate 9600
   xor dx, dx
   int 0x14
%endif ; serialInit

stackbottom equ $ & ~ (CellSize - 1) ; align down

; NB: We can copy call key, recalculate the offset and get the key
; word independent of the cell size.

; First, initialize the word code by itself and its character name in
; the input and its address on initial stack:
Code:
  call  key
  pop   CAX
; save xt to indirection table
%ifdef linux32
  mov   [edi+ebx*CellSize], eax
%endif ; linux32
%ifdef bios16
  shl   bx, CellSizeShift
  mov   [di+bx], ax
%endif ; bios16

; fall through:

InterpreterLoop:
  call  key
  mov   CSI, .return
%ifdef linux32
  jmp   [EDI+EBX*CellSize]
%endif ; linux32
%ifdef bios16
  shl   bx, CellSizeShift
  jmp   [di+bx]
%endif ; bios16

.return:
  Dcell   InterpreterLoop

; returns
; CBX = key
; clobbers CAX, CBX, CCX, CDX
key:

%ifdef linux32
  push  byte 3
  push  esp
  pop   ecx                     ; store byte directly onto the stack
  pop   eax                     ; linux syscall 3
  push  byte 1                  ; xor edx, edx; inc edx is not shorter
  pop   edx                     ; length is 1 byte
  xor   ebx, ebx                ; file descriptor 0 is stdin
  push  ebx
  int   0x80
  pop   ebx
%endif ; linux32

%ifdef bios16
%ifdef keyboard
  mov   ah, 0                   ; hex: b400
  int   0x16                    ; hex: cd16
%endif ; keyboard
%ifdef serial
  ; serial input
  mov   ah, 2                   ; hex: b402
  xor   dx, dx                  ; hex: 31d2
  int   0x14                    ; hex: cd14
%endif ; serial
  xor   ah, ah                  ; check if needed
  xchg  ax, bx
%endif ; bios16
  ret

; Define anonymous forth words:

PSPFetch:                       ; psp@ ( -- n )  3 bytes op-code
  push  CSP
  NEXT

Fetch:                          ; @ ( addr -- val ) 5 bytes op-code
  pop   CBX
  push  WordCell [CBX]
  NEXT

;;; We need a code size of the right number of bytes. We can achieve
;;; this for other cpu by additional nop-codes, rearranging next,
;;; etc.pp. Later, in cipf2f*, we can skip the nop-codes by adjusting
;;; the value of - in the character indirection table as soon as we
;;; can do arithmetic in cipf.
Substract:                      ; - ( n1 n2 -- n1-n2 ) 4 bytes op-code
%ifdef linux32
  pop   eax
  sub   [esp], eax
%endif ; linux32
%ifdef bios16
  pop   ax
  mov   bx, sp
  sub   [bx], ax
  ;; pop   bx
  ;; pop   ax
  ;; sub   ax, bx
  ;; push  ax
%endif ; bios16

;  NEXT                                             3 bytes op-code
next:
  LODScell
  jmp   CAX

Store:                          ; ! ( val addr -- ) 5 bytes op-code
  pop   CBX
  pop   WordCell [CBX]
  NEXT

  lastChar      equ '~'

  filesize      equ     $ - $$

%ifdef linux32
%ifdef standalone
%ifndef elf
;SECTION .bss align=1
%endif ; elf
%endif ; standalone
%endif ; linux32

%ifdef linux32
%ifdef standalone
%ifndef elf

%define FillArea 1
  resb (Store + 3*CellSize - $)
HERE:
  resb  CellSize                ; HERE
indirectionTable:
  resb  (lastChar + 1) * CellSize

startHERE:
  resb  hereSize
endHERE:

%endif ; elf
%endif ; standalone
%endif ; linux32

%ifndef FillArea
HERE                    equ (Store + 3*CellSize)
indirectionTable        equ HERE + CellSize
linuxStack              equ HERE + CellSize
startHERE equ   indirectionTable + (lastChar + 1) * CellSize
; startHERE equ   HERE + (lastChar + 2) * CellSize ; HERE + 0x80 * CellSize
%endif ; FillArea

%ifdef bootSector
  bootSectorSize        equ 512 ; 0x200
  resb  bootSectorSize - ($ - $$) - 2
  db 0x55, 0xAA
%endif ; bootSector
