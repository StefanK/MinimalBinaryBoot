(define (eval e env)
  ;(display (list 'eval e env))
  (cond
   ((symbol? e) (lookup0 e env))
   ((pair? e)   (apply (eval (car e) env) (cdr e) env))
   (else e)))

(define (apply f args env)
  ;(display (list 'apply f args env))
  (cond
   ((eq? (car f) 'vau) (eval (caddr f) ; body
                             (cons (cons (cadr f) ; params incl. env
                                         (cons env args)) (cdddr f))))
   ((eq? (car f) 'vprim) ((cdr f) args env))
   (else "Not yet implemented.")
   ))

(define (vau-prim args env)
  (let ((as (car args))
        (e  (cadr args))
        (b  (caddr args)))
    (cons* 'vau (cons e as) b env))
  )
(define (eval-prim args env)
  (let ((expr (car args))
        (env0 (cadr args)))
    (eval (eval expr env) (eval env0 env))))
(define (define-prim args env)
  (let* ((sym (car args))
         (expr (cadr args))
         (val (eval expr env))
         (vars (caar env))
         (vals (cdar env)))
    (set-car! (cons (cons sym vars)
                    (cons val vals)) env)))

(define (lookup0 s e)
  ;(display (list 'lookup0 s e))
  ;(display (list (symbol? s) s))
  ;(display e)
  (define (l vars vals e)
    (cond
     ((null? vars)
      (if (null? e)
          (error "not found:" sym e)
          (l (caar e) (cdar e) (cdr e))))
     ((eq? (car vars) s) (car vals))
     ((eq? (cdr vars) s) (cdr vals))
     (else (l (cdr vars) (cdr vals) e))))
  (l #nil #nil e))
(define initial-env
  `((vau    . (vprim . ,vau-prim))
    (eval   . (vprim . ,eval-prim))
    (define . (vprim . ,define-prim))
    ))

(use-modules (system vm trace))
(define rexpr0 '(vau x e e))
(define (test0) (eval rexpr0 initial-env))
(trace-calls-to-procedure lookup0)
(trace-calls-to-procedure eval)
(trace-calls-to-procedure apply)
(test0)
(call-with-trace test0)
(define (repl) (display (eval (read) initial-env)) (repl))
(call-with-trace repl)
;; c.f. https://matt.might.net/articles/cesk-machines/ ff

; eval takes an expression and an environment to a value
(define (eval e env)      (cond
  ((symbol? e)            (cadr (assq e env)))
  ((eq? (car e) 'lambda)  (cons e env))
  (else                   (apply (eval (car e) env) (eval (cadr e) env)))))

; apply takes a function and an argument to a value
(define (apply f x)
  (eval (cddr (car f)) (cons (list (cadr (car f)) x) (cdr f))))
; read and parse stdin, then evaluate:
(define (repl) (let loop () (display (eval (read) '())) (newline) (loop)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-modules (ice-9 receive))
(use-modules (srfi srfi-9))
(use-modules (ice-9 pretty-print))
; (use-modules (ice-9 match)) if we replace values etc. by match
;(use-modules (srfi srfi-1)) ; has take

(define (atom? x) (not (pair? x)))
(define (copy-tree e)
  (define (cpy e al)
    (let ((e0 (assoc e al)))
      (cond
       (e0 (values (cdr e0) al))
       ;; todo: deep copy atoms
       ((atom? e) (values e (acons e e al)))
       (else ; pair
        (receive (na al0)
            (cpy (car e) al)
          (cpy (cdr e) al0))))))
  (receive (c al) (cpy e #nil) c))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (cek-loop expr val env k)
  (pretty-print (list expr val))
  (pretty-print (list "env:" env))
  (pretty-print (list "cont:" k))
  (if (and (noEval? expr) (null? k))
      (begin
        (pretty-print (list "result: " val))
        (pretty-print (list "final env:" env))
        (cons val env))
      (receive (expr0 val0 env0 k0)
          (cek-step expr val env k)
        (cek-loop expr0 val0 env0 k0))))

(define (init expr env) (values expr 0 env #nil))

(define noEval (lambda () #nil))
(define (noEval? v) (eq? noEval v))

(define args0# 0)
(define args# 1)
(define if# 2)
(define (if? k) (eq? (car k) if#))

(define-record-type <closure>
  (make-closure type args body env)
  closure?
  (type closure-type)
  (args closure-args)
  (body closure-body)
  (env  closure-env))

(define vau# 0)
(define vprim# 1)
(define lambda# 2)
(define lprim# 3)
(define (vau? v) (and (closure? v) (eq? (closure-type v) vau#)))
(define (vprim? v) (and (closure? v) (eq? (closure-type v) vprim#)))
(define (lambda? v) (and (closure? v) (eq? (closure-type v) lambda#)))
(define (lprim? v) (and (closure? v) (eq? (closure-type v) lprim#)))
(define (make-vprim g) (make-closure vprim# #nil g #nil))
(define (make-lprim f) (make-closure vprim# #nil f #nil))

(define (args0 expr env k) (cons* args0# expr env k))
(define (args0-drop k) (cdddr k))
(define (args0? k) (eq? (car k) args0#))
(define (args0-args k) (cadr k))
(define (args0-env k) (caddr k))
(define (args0-env-args k) (cons (args0-env k) (args0-args k)))

(define (args0to* k) (cons* (cons #nil (args0-args k)) (cddr k)))
(define (args? k) (pair? (car k)))
(define (args-final? k) (null? (cdar k)))
(define (args-drop k) (cddr k))

(define (args-next k) (cadar k))
(define (args-env  k) (cadr k))
(define (args-vals k) (caar k))
(define (args-args k) (cdar k))
(define (args-new v k)
  (cons* (cons (cons v (args-vals k))
               (cdr (args-args k)))
         (cdr k)))

(define (if-cont te env k) (cons* if# te env k))
(define (if-true k) (caadr k))
(define (if-false k) (cadadr k))
(define (if-env k) (caddr k))
(define (if-drop k) (cdddr k))

; todo: lookup-adr for set!
(define (lookup sym env)
  (define (lf vars vals env)
    (cond
     ((null? vars)
      (if (pair? env) (lf (caar env) (cdar env) (cdr env)) #f))
     ((eq? (car vars) sym) (car vals))
     ((eq? (cdr vars) sym) (cdr vals))
     (else (lf (cdr vars) (cdr vals) env))))
  (lf #nil #nil env))
#;(define (lookup sym env) (car (lookup-adr sym env)))

(define (new-env-frame args vals env)
  (cons (cons args vals) env))

(define (bind-tree args vals env)
  (display (list "bind:" args vals env "\n"))
  (define (tree-match args vals as vs)
    (cond
     ((and (null? args) (null? vals)) (values as vs))
     ((eq? '_ args) (values as vs))     ; #ignore
     ((and (pair? args) (pair? vals))
      (receive (as0 vs0)
          (tree-match (car args) (car vals) as vs)
        (tree-match (cdr args) (cdr vals) as0 vs0)))
     (else (error "bind-tree failed" args vals))))
  (receive (as vs)
      (tree-match args vals #nil #nil)
    (cons (cons as vs) env)))

(define (cek-step expr val env k)
  (cond
   ;; E(sym,0,e,k)                    = (0,e(sym),0,k)
   ((symbol? expr)
    (display "lookup\n")
    (values noEval (lookup expr env) #f k))
   ;; E((M . Ns),0,e,k)                = (M,0,e,arg0(Ns,e,k))
   ((pair? expr)
    (display "apply\n")
    (values (car expr) noEval env (args0 (cdr expr) env k)))
   ((noEval? expr)
    (cond
     ((args0? k)
      (cond
       ;; E(0,(vau x e.M,eV),e0,args0(Ns,e1,k)) = (M,0,eV[x e:= Ns e1],k)
       ((vau? val)
        (display val)
        (display "\nvau\n")
        (values (closure-body val) noEval
                (new-env-frame (closure-args val)
                               (args0-env-args k)
                               (closure-env val))
                #; (bind-tree (closure-args val)
                (args0-args-env k)
                (closure-env val))
                (args0-drop k)))
       ;; E(0,vprim g,e0,k0@args0(Ns,e1,k))   = (g e0 k0)
       ((vprim? val)
        (display "vprim\n")
        ((closure-body val) env k))
       ;; E(0,v,e0,args0(Ns,e1,k))   = (0,v,e0,args(()Ns,e1,k))
       (else ; lambda or lprim
        (display "lambda|lprim\n")
        (values 0 val env (args0to* k)))))
     ((args? k)
      (pretty-print (list "args:" (cdar k)))
      (cond
       ((args-final? k)
        (display "final arg eval\n")
        (let* ((form (reverse (cons val (args-vals k))))
               (clo  (car form))
               )
          (if (lprim? clo)
              ;; E(0,v,e0,k0@args((vs)()),e1,k)) = (f vs' k0)
              ;;   where ((lprim f) vs') = (reverse (cons v vs))
              ((closure-body clo) (cdr form) k)
              ;; E(0,v,e0,args((vs)()),e1,k)) = (M,0,eL[x:=vs'],k)
              ;;   where ((\x.M,eL) vs') = (reverse (cons v vs))
              (values (closure-body clo) noEval
                      (bind-tree (closure-args clo)
                                 (cdr form)
                                 (closure-env clo))
                      (args-drop k)))))
       ;; E(0,v,e0,args(vs.N.Ns),e1,k)) = (N,0,e1,args((v.vs).Ns,e1,k))
       (else
        (display "next arg\n")
        (values (args-next k) noEval (args-env k) (args-new val k)))))
     ((if? k)
      (values (if val (if-true k) (if-false k)) noEval (if-env k)
              (if-drop k)))
     ))
   ;; E(v,0,e,k)                      = (0,v,e,k)
   (else
    (display "atom\n")
    (values noEval expr env k))))

(define (vprim-vau env k) ; k = args0(Ns,e1,k)
  (let ((args (args0-args k)))
    (values noEval (make-closure
                    vau#
                    (cons (cadr args) (car args))
                    (caddr args)
                    (args0-env k)) #f (args0-drop k))))

(define (vprim-if env k) ; k = args0(Ns,e1,k)
  (pretty-print (list "vprim-if" env k))
  (let ((args (args0-args k))
        (env0 (args0-env k)))
    (pretty-print args)
    (pretty-print env)
    (pretty-print (list "car" (car args)))
    (values (car args) noEval env0
            (if-cont (cdr args) env0 (args0-drop k)))))
(define (lprim-eval vs k ) ; k = args((vs)()),e1,k)
  (values (car vs) noEval (cadr vs) (args-drop k)))
(define (lprim-contTo vs k) ; k = args((vs)()),e1,k)
  (let ((v (car vs))
        (k0 (cadr vs)))
    (values noEval v #f k0)))

(define (lprim-call/cc vs k ) ; k = args((vs)()),e1,k)
  #; (call/cc f)
  #; (lambda (v) (contTo k v))
  (let ((f (car vs))
        (doit (make-closure lambda# '(v) '(contTo k v)
                            (new-env-frame '(k) k #nil))))
    (values noEval doit #nil
            (args-new f (args0t* (args0 #f #nil k))))))
(define (vprim-display env k) ; k = args0(Ns,e1,k)
  (display (list env k))
  (values noEval #f env (args0-drop k)))

(define initial-env
  (let ((plist `((vau . ,(make-closure vprim# 0 vprim-vau #nil))
                 (eval . ,(make-closure lprim# 0 lprim-eval #nil))
                 (call/cc . ,(make-closure lprim# 0 lprim-call/cc #nil))
                 (display . ,(make-closure vprim# 0 vprim-display #nil))
                 (if . ,(make-closure vprim# 0 vprim-if #nil))
                 (#t . ,#t)
                 (#f . ,#f))))
    (define (convert ps vars vals)
      (if (null? ps)
          (list (cons vars vals))
          (convert (cdr ps)
                   (cons (caar ps) vars)
                   (cons (cdar ps) vals))))
    (convert plist #nil #nil)))

#;(display initial-env) (newline)
(define expr1 '(vau x e e))
(define expr1 '((vau x e e) (vau (x) x)))
(define expr3 '((vau (x) e x) #t))
(define expr4 '((vau (x) e x) (x y z #t))) ; quote
(define expr5 '(if #t #t #f))
(define expr0 expr5)
#;(display (call-with-values (lambda () (init expr0 initial-env)) list))
(receive (expr val env k) (init expr0 initial-env)
  (cek-loop expr val env k))
