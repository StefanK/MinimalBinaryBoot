\ ;;\ meta-circular evaluator for vau calculus
\ ;; 1. recursive
\ ;; page 52 in https://web.wpi.edu/Pubs/ETD/Available/etd-090110-124904/unrestricted/jshutt.pdf
\ (define (eval-rec exp env)
\   (cond
\    ((symbol? exp) (lookup-value exp env))
\    ((pair? expr) (apply (eval (car exp) env) (cdr exp) env))
\    (else exp)))

\ (define (eval-cps exp env cont)
\   (cond
\    ((symbol? exp) (lookup-value exp env cont))
\    ((atom?   exp) (cont exp))
\    (else (eval (car exp) env
\                (lambda (f)
\                  (apply f (cdr exp) env cont))))))
\ ; with trampoline, global state and gc-safe
\ (define (eval-loop)
\   (if (current>state)
\       (begin (eval-step) (eval-loop))
\       current))

\ ;; CESK-machine https://legacy.cs.indiana.edu/ftp/techreports/TR202.pdf
\ ;; state is (expr,env,store,k) with start (expr0,env0,0,stop)
\ ;; E(x,env,s,k) = (_,0,s,k:ret(s(env(x)))) ; return value of variable
\ ;; E(\x.M,env,s,k) = (_,0,s,k:ret(\x.M,env)) ; return closure
\ ;; E(M N,env,s,k) = (M,env,s,k:arg N env)
\ ;; E(_,0,s,k:arg N env:ret F) = (N,env,s,k:fun N env)
\ ;; E(_,0,s,k:fun \x.M  env:ret V) = (M,env[x:=n],s[n:=V],k) ; n new
\ ;; E(!x.M,env,s,k) = (_,0,s,k:ret(!x.M,env))
\ ;; E(_,0,s,k:fun(!x.M,env):ret V) = (M,env,s[env(x) := V],k)


\ ;; adapted to vau-calculus
\ ;; E(x,env,s,k) = (_,0,s,k:ret(s(env(x)))) ; return value of variable
\ ;; E(v,env,s,k) = (_,0,s,k:ret(v)=         ; return (immediate) value
\ ;; app: E(\x.M,env,s,k) = (_,0,s,k:ret(\x.M,env)) ; return lambda-closure
\ ;; app: E(vau x e.M,env,s,k) = (_,0,s,k:ret(vau x e.M,env)) ; return vau-closure

\ ;; E(M N,env,s,k) = (M,env,s,k:(arg N env))
\ ;; E(_,0,s,k:arg N env:ret(\x.M,env0)) = (N,env,s,k:fun(\x.M,env0)
\ ;; E(_,0,s,k:fun(\x.M,env0):ret N)     = (M,env0[x:=n],s[n:=N],k)
\ ;; E(_,0,s,k:arg N env:ret(vau x e.M,env0) = (M,env0[x:=n0,e:=n1],s[n0:=N,n1:=env],k)

\ ;; E(!x.M,env,s,k) = (_,0,s,k:ret(!x.M,env))
\ ;; E(_,0,s,k:arg N env:ret(!x.M,env0)) = (N,env,s,k:fun(!x.M,env0)
\ ;; E(_,0,s,k:fun(!x.M,env):ret V) = (M,env,s[env(x) := V],k)

\ ;; eval by E, better as a lambda-primitive:
\ ;; E(eval M N,env,s,k) = (M,env,s,k:earg N env)
\ ;; E(_,0,s,k:earg N env:ret e0) = (N,env,s,k:eval e0)
\ ;; E(_,0,s,k:eval e0:ret env0) = (e0,env0,s,k)

\ ;; P:(eval '(M N) env s k) = (M,env,s,k:arg N env)
\ ;; E(_,0,s,k:arg N env:ret M) = (N,env,s,k:fun(\ ().M)
\ ;; E(_,0,s,k:(fun \().M):ret N ) = (M,N,s,k)

\ ;; ;; lambda-primitives: eval (optionally: wrap) + - etc.pp.
\ ;; ;; vau-primitives: vau define if
\ ;; E(_,0,s,k:if (t e) env:ret #t) = (t,env,s,k)
\ ;; E(_,0,s,k:if (t e) env:ret #f) = (e,env,s,k)
\ ;; E(_,0,s,k:arg N env:ret (vau-primitive g)) = (g N env s k)
\ ;; E(_,0,s,k:arg (c t e) env:ret (vprim if)) = (c,env,d,k:(if (t e),env))
\ ;; E(_,0,s,k:arg N env:ret (lambda-primitive f)) = (N,env,s,k:(lprim f))
\ ;; E(_,0,s,k:lprim f:ret N) = (f) with global access, e.g. N s k
\ ;; E(_,0,s,k:lprim eval:ret (M N)) = (M,N,s,k)
\ ;; E(_,0,s,k:arg (c t e) env:vprim if) = (c,env,s,k:if (t e) env)
\ ;; continuations:
\ ;; ret value
\ ;; ret value env where value is one of lambda, vau, vprim, lprim
\ ;; arg expr  env
\ ;; fun expr  env
\ ;; if  expr  env
\ ;; lprim expr

\ (define (eval-step)
\   (match (current>state)
\    (state-eval
\     (cond
\      ((symbol? (current>exp)) (lookup-value))
\      ((atom?   exp) (state>value! (state>exp)) (state>state! cont))
\      (else (let ((cont (new-continuation)))
\              (cont>cont! current)
\              (cont>expr! (car (current>expr)))
\              (cont>pf! nil)
\              (cont>state! state-eval)
\              (current! cont)))

\       (let ((e (state-expr)))
\              (state-expr! (car e))
\              (state-cont! (lambda (f)
\                             (apply f (cdr e) env cont))))))
\    (state-apply
\     (bind/pt (current>value>pt) (cdr current>next>expr ))
\    ((eq? (currrent>tate) state-continue)
\     )
\    ))
\ (define (lookup-value)
\    (define (lf vars vals env)
\       (if (null? vars)
\             (if (null? env)
\                   (begin (current>cont! sym-not-found-cont)
\                          (current>value! (current>cont))
\                          (current>state! state-continue))
\                (lf (caar env) (cdar env) (cdr env)))
\          (if (eq? sym (car vars))
\                  (begin (state-value! (car vals))
\                         (state-state! state-continue))
\              (lf (cdr vars) (cdr vals) env))))
\     (lf '() '() (state-env)))



\ 2023 Copyright (C) Dr. Stefan Karrmann
\ A vau interpreter in forth, either gforth or mbbforth
\ It works on machines with 16 bit words or bigger.

\ We use pointer tagging with only two tag bits:
\ +n--------10+n--------10+
\ | car     00| cdr     00|
\ | imm-int  1| cdr     00| immediate data in car
\ | infoPtr 10| data0..n  | n in 2*N
\ | fwdPtr  10| Ptr     00|
\ | valueP  10| xxxxxxxxxx| one char
\ +n--------10+n--------10+
\ and additional immediates for nil #t #f at top of infoPtr

\ First, we use the Cheney garbage collector.
\ We could also use the garbage collector of Baker for serial real time gc.

\ With only two bits for tagging we have no space in a word for the mark
\ bit for a mark-and-X garbage collector. Of course, we could use a
\ bitmap block. For n double words, we need n/bitsPerWord words.

\ The root set is the continuation stack cs and the list of internals.
\ For simplicity internals is a list of the form (atom0 atom1 ...)


[IFDEF] scm-code
    scm-code
[ENDIF]

marker scm-code
[IFUNDEF] nil
    ' 0 alias nil
[ENDIF]
[IFUNDEF] 3drop
    : 3drop ( x0 x1 x2 -- )
        2drop drop ;
[ENDIF]
[IFUNDEF] ndrop
    : ndrop ( xu .. x1 u -- ) \ drop u+1 stack items
     1+ cells sp@ + sp! ;
[ENDIF]
[IFUNDEF] cells/
    : cells/ cells / ;
[ENDIF]
[IFUNDEF] ptr%
    ' cell% alias ptr%
[ENDIF]
[IFUNDEF] int%
    ' cell% alias int%
[ENDIF]
[IFUNDEF] uint%
    ' cell% alias uint%
[ENDIF]
[IFUNDEF] alloc
    : alloc ( u -- a )
        allocate throw ;
[ENDIF]
[IFUNDEF] 0!
    : 0! ( a -- )
        0 swap ! ;
[ENDIF]
[IFUNDEF] cell/
    : cell/ ( u0 -- u1 )
        cell / ;
[ENDIF]
[IFUNDEF] =?
    : =? ( x0 x1 -- t | x0 f )
        over = dup IF nip THEN ;
[ENDIF]

#3 constant tag-mask
#0 constant tag-cxr
#1 constant tag-immediate-int
#2 constant tag-info
-1 tag-mask invert and tag-info or constant immediate#t
immediate#t tag-mask 1+ - constant immediate#f
immediate#f tag-mask 1+ - constant immediate#inert
immediate#inert tag-mask 1+ - constant noEval

struct
    ptr% field info>scan     ( a:toSpace -- ; mem0 -- mem1 )
    ptr% field info>evacuate ( a:fromSpace -- a:toSpace )
    ptr% field info>apply    ( i*x a:obj env cont -- value cont )
    ptr% field info>display  ( a:obj -- )
    uint% field info>size    ( -- u:size )
end-struct info%

struct
   ptr% field mem>toSpace-max
   ptr% field mem>toSpace
   ptr% field mem>fromSpace
   ptr% field mem>fromSpace-max
   ptr% field mem>scan
   ptr% field mem>free
end-struct mem%

create mem mem% %size allot
create info-forward info% %size allot

\ CESK state
variable expr                           \ aka control
variable val                            \ replaces return continuation
variable env
variable cs                             \ continuation stack, i.e. list
\ additional roots:
variable expr0
variable expr1
variable expr2
variable error-cont
variable interned                       \ interned values mostly symbols

[IFDEF] 0
    ' 0 alias enum-start
[ELSE]
    : enum-start 0 ;
[ENDIF]
: enum dup constant 1+ ;
' drop alias enum-end

enum-start
enum cont-exit                          \ (exit)
enum cont-args0                         \ (args0 Ns env k)
enum cont-argsN                         \ (argsN vs k)
                                        \ args: (((vs).Ns) env k)
enum cont-if                            \ (if (t e) env k)
enum cont-contTo                        \ (contTo k0 k)
enum cont-eva0                          \ (eval0 N env k)
enum cont-eval1                         \ (eval1 Mv k)
enum-end
\ todo: filename, lineno, column

: get-tag ( a:adr -- u:tag )
    @ #3 and ;

: mask-tag ( a:adr0 -- a:adr1 )
    [ #3 invert ]l and ;

( === 1. The garbage collector === )

#42 constant OOM                        \ out of memory

defer cheney-gc

: mem>hasSpace ( u -- t/f )
    2* cells  mem mem>free @ +   mem mem>toSpace-max < ;

: mem>alloc ( u:#pairs -- a:toSpace )
    2* cells
    dup mem>hasSpace 0= IF
        cheney-gc
        dup mem>hasSpace 0= IF OOM throw THEN
    THEN
    mem mem>free @ swap over + mem mem>free !
;

: evacuate-pair ( a:fromSpace0 -- a:toSpace )
    1 mem>alloc ( a:fromSpace0 a:toSpace )
    tuck [ 2 cells ]l cmove
    \ over @ over !
    \ swap cell+ @ over cell+ !
;

: evacuate ( a:?fromSpace -- a:toSpace )
    dup mem mem>fromspace @  mem mem>fromSpace-max @  within
    IF
        dup get-tag
        2 = IF \ infoPtr
            dup @ info>evacuate @ execute
        ELSE \ pair
            evacuate-pair
        THEN
    THEN
;
: scan-cxr ( a:adr0 -- a:adr1 )
    dup @ get-tag
    0= IF \ not integer but pointer
        dup @ ( a:adr0 a:adr-cxr )
        dup mem mem>fromSpace @ mem mem>fromSpace-max @
        within ( x a b -- f )
        IF \ fromSpace
            evacuate
        THEN
    THEN
    cell+
;

: evacuate-root ( a -- )
    \ evacuate and use forward pointer for update
    dup @ dup evacuate cell+ @ swap ! ;

:noname \ cheney-gc ( -- )
    \ flip
    mem mem>toSpace @
    mem mem>fromSpace @
    dup mem mem>free !
    dup mem mem>scan !
    mem mem>toSpace !
    mem mem>fromSpace !
    mem mem>toSpace-max @
    mem mem>fromSpace-max @
    mem mem>toSpace-max !
    mem mem>fromSpace-max !
    0 expr env cs expr0 expr1 expr2 interned
    BEGIN dup WHILE evacuate-root REPEAT
    BEGIN
        mem>scan @ dup mem>free @ < WHILE
            dup get-tag 2 = IF \ infoPtr
                @ mask-tag info>scan @ execute
            ELSE scan-cxr scan-cxr info>scan ! \ pair
            THEN
    REPEAT
; IS cheney-gc

: make-scheme ( u.mem -- )
    dup alloc
    dup mem mem>free !
    dup mem mem>toSpace !
    over + mem mem>toSpace-max !
    dup alloc
    dup mem mem>fromSpace !
    + mem mem>fromSpace-max !
    0 expr val env cs expr0 expr1 expr2 interned
    BEGIN dup WHILE 0! REPEAT
;

: scan-info-default ( a:toSpace -- )
    \g toSpace: | info | ptr0 | ... | ptrN | N in 2*N
    \g                   +--- | info | data... |
    \g              or   +--- | car  | cdr |
    dup @ info>size @ ( a u )
    BEGIN dup WHILE
            1- swap cell+ dup @ evacuate
            over !                      \ update to toSpace
            swap
    REPEAT
;

: evacuate-info-default ( a:fromSpace -- a:toSpace )
    dup
    mem mem>free @  dup >r over @ info>size @ cells
    2dup + mem mem>free !               \ todo: use mem>alloc with
                                        \ cells not pairs
    ( a:fromSpace a:fromSpace a:toSpaceFree u:bytes ; R: a:toSpace )
    cmove
    info-forward over !
    r@ swap cell+ !  r>
;

: evacuate-info-forward ( a:fromSpace -- a:toSpace )
    cell+ @ ;
' evacuate-info-forward info-forward info>evacuate !

: scan-info-forward ( a:toSpace -- )
    ." scan forward in toSpace: Not allowed with Cheney's stop and go gc!" bye ;
' scan-info-forward info-forward info>scan !

( === basic pair functions  === )

'  @ alias car ( pair -- car )
: cdr      ( pair -- cdr )
    cell+ @ ;
' ! alias car! ( car pair -- )
: cdr! ( cdr pair -- )
    cell+ ! ;
: new-pair ( -- pair )
    1 mem>alloc ;
: cons ( -- pair )
    new-pair
    expr0 @ over car!
    expr1 @ over cdr!
;
: caar ( pair -- caar )
    @ @ ;
: cdar ( pair -- cdar )
    @ cell+ @ ;
: cadr ( pair -- cdar )
    cell+ @ @ ;
: cddr ( pair -- cdar )
    cell+ @ cell+ @ ;

' 0= alias null? ( lisp -- t/f )
' = alias eq? ( lisp0 lisp1 -- t/f )

: pair? ( lisp -- t/f )
    get-tag #2 <> ;
: atom? ( lisp -- t/f )
    get-tag 0<> ." todo!" ;

( ==== continuations suppot ==== )

: cont-args? ( -- t/f )
    cs @ car pair? ;
: cont-is? ( u -- t/f )
    cs @ car = ;
: cont-exit?  cont-exit  cont-is? ;
: cont-args0? cont-args0 cont-is? ;
: cont-argsN? cont-argsN cont-is? ;
: cont-if? cont-if cont-is? ;

: args0-drop
    cs @ cddr cdr cs ! ;
' args0-drop alias if-drop
: args0-args cs @ cadr ;               \ ( cs: args0 Ns env k -- Ns )
: args0-env cs @ cddr car ;            \ ( cs: args0 Ns env k -- env )
: eval0to1 ( cs: eval0 N env k -- eval1 Mv k )
    cs @ cddr cdr expr1 !   val @ expr0 !
    cons expr1 !   cont-eval1 expr0 !
    cons cs !
;
: args0to* \ ( cs: args0 Ns env k -- ((nil.Ns) env k)
    nil expr0 !  cs @ cadr expr1 !  cons expr0 !
    cs @ cddr expr1 ! cons cs !
;
: args-args cs @ cdar ;                   \ ( cs: args (vs.Ns) env k -- Ns
: args-next \ ( val: v; cs: ((vs).N.Ns) env k -- ((v.vs).Ns) env k ; expr: N; env: env )
    cs @ cdar car expr !  cs @ cadr env ! \ N env
    val @ expr0 !  cs @ caar expr1 !  cons expr0 ! \ val vs
    cs @ cdar cdr expr1 !  cons expr0 !            \ (v.vs) Ns
    cs @ cdr expr1 !  cons cs !
;
: args-final? \ ( cs: ((vs).Ns) env k -- Ns null)? )
    cs @ cdar null? ;

: reverse-args \ ( cs: ((vn-1..v0).nil) env k -- cs: k; expr1: (v0 ... vn) )
    nil expr1 !  val @ expr0 !  cons expr1 !
    cs @ caar  val !
    BEGIN val @ dup WHILE
            car expr0 ! cons expr1 !
            val @ cdr val !
    REPEAT
    expr0 0! val 0!                   \ avoid memory leaks
    cs @ cddr cs !
;

( === environment === )
\ quick and dirty as a rib without encapsulating, i.e. info structure

' nil alias new-environment ( -- env )
' cons alias new-frame ( expr0:vars expr1:vals -- frame )
' cons alias add-frame ( expr0:frame expr1:parent-env -- new-env )

\ todo: make it gc safe !
: extend-env ( expr0:var expr1:val env -- )
    cons dup                            \ (cons var val) dup
    env @ caar swap cdr!                \ (cons var vars)
    env @ car car!                      \ env: ((cons vars' val) parent)
    expr1 @ expr0 !                     \ val
    cons dup                            \ (cons val val) dup
    env @ cdar swap cdr!                \ (cons val vals)
    env @ car cdr!                      \ env: ((cons vars' val') parent)
;

: lookup-frame ( sym vars vals env -- a | 0 )
    BEGIN
        over null?
        IF dup null?
            IF 4 ndrop 0 THEN             \ not found
            -rot 2drop dup caar over cdar rot
        ELSE ( sym vars vals env )
            3 pick 3 pick cdr eq?
            IF drop nip nip cell+ exit THEN
            3 pick 3 pick car eq?
            IF drop nip nip exit THEN
            rot cdr rot cdr rot
        THEN
    AGAIN
;
\ : lookup-frame ( sym vars vals env -- a xt-get xt-set | sym 0 )
\     BEGIN
\         over null?
\         IF dup null?
\             IF 3drop 0 THEN             \ not found
\             -rot 2drop dup caar over cdar rot
\         ELSE ( sym vars vals env )
\             3 pick 3 pick cdr eq?
\             IF drop nip nip ['] cdr ['] cdr! exit THEN
\             3 pick 3 pick car eq?
\             IF drop nip nip ['] car ['] car!  exit THEN
\             rot cdr rot cdr rot
\         THEN
\     AGAIN
\ ;

: lookup ( sym -- a|0 )
    nil nil env @ lookup-frame ;
\ : lookup ( sym -- a xt-get xt-set )
\     nil nil env @ lookup-frame ;

: define! ( sym val env cont -- a:val cont )
    ." todo: define!"
    \ Hm.. todo: set error>cont to handle not found
;

( === closure === )
\ layout: | info-closure | formal-parameters | env | body |
struct
    ptr% field closure>info
    ptr% field closure>formals
    ptr% field closure>env
    ptr% field closure>body
end-struct closure%

create info-closure-vprim  info% %size allot
create info-closure-vau    info% %size allot
create info-closure-lambda info% %size allot
create info-closure-lprim  info% %size allot

: clo-type? swap car = ; ( closure u:type -- t/f )
: vprim?  info-closure-vprim  clo-type? ;
: vau?    info-closure-vau    clo-type? ;
: lambda? info-closure-lambda clo-type? ;
: lprim?  info-closure-lprim  clo-type? ;

( === symbol === )

\ layout: | info-symbol | octet-len | octets ... | pad |

create info-symbol info% %size allot

: symbol-len ( symbol -- u )
    cell+ @ ;

: symbol-scan ( a:toSpace -- )
    cell+ dup @ cell+ 1- cell/ 1+ cells + mem mem>scan !
; ' symbol-scan info-symbol info>scan !

: symbol-evacuate ( a:fromSpace -- a:toSpace )
    mem mem>free @ dup >r over
    symbol-len cell+ 1- cell/ 1+ 1+ cells dup >r cmove>
    r> mem mem>free +!  r>
; ' symbol-evacuate info-symbol info>evacuate !

: symbol? ( lisp -- t/f )
    @ info-symbol tag-info or = ;

( === eval === )

\ Beware don't reuse continuations we need them for call/cc !
\ check if <env 0!> needed to avoid memory leaks.
: eval-step ( -- )
    expr @
    noEval =? IF \ inspect cs
        cs @ car
        dup cont-args? IF
            \ E(0,v,e0,args(((vs).N.Ns),e1,k) = (N,0,e1,args(((v.vs).Ns,e1,k)
            args-args pair? IF args-next exit THEN
            reverse-args
            expr1 @ car
            dup lambda? IF
                \ E(0,v,e0,args(((vs).#nil),e1,k) = (b,0,eL[xs:(tail vs')],k)
                \   where vs' = (reverse (cons* v vs)), lambda xs eL b = (head vs')
                dup closure>body @ expr !
                dup closure>env  @ env !
                closure>formals @ expr0 !  expr1 @ cdr expr1 ! cons expr0 !
                env @ expr1 ! cons env ! exit
            THEN
            dup lprim? IF
                \ E(0,v,e0,args(((vs).#nil),e1,k) = (f (tail fs') k)
                \   where vs' = (reverse (cons* v vs)), lprim f = (head vs')
                \ cs == k, expr1 == vs'
                closure>body @ execute exit
            THEN
            ." unknown lambda like closure" bye
        THEN
        dup cont-args0? IF
            val @
            dup vau? IF
                \ E(0,(vau (e.ps)e0.b),e1,(args0 Ns e2 k)) = (b,0,e0[(e.ps):=(e2.Ns)],k)
                val @
                closure>body @ expr !
                cs @ cddr car  expr0 !  \ e2
                cs @ cadr      expr1 ! cons expr1 ! \ (e2.Ns)
                val @ closure>formals expr0 ! cons expr0 ! \ ((e.ps).(e2.Ns))
                val @ closure>env @ expr1 ! cons env !
                exit
            THEN
            dup vprim? IF
                \ E(0,(vprim.f),e0,(args0 Ns e1 k)) = (f Ns e1 k)
                \ expr0 == Ns, expr1 == e1, cs == k
                cs @ cdr dup car expr0 !       \ Ns
                cdr dup expr1 !                \ e1
                cdr cs !                       \ k
                closure>body @ execute exit
            THEN
            \ E(0,fv,e0,(args0 Ns e1 k)) = (0,fv,e0,((().Ns) e1 k))
            cs @ cadr expr1 !  nil expr0 !  cons expr0 !
            cs @ cddr expr1 !  cons cs ! exit
        THEN
        dup cont-if? IF
            \ E(0,#t,e0,('if (t e) e1 k)) = (t,0,e1,k)
            \ E(0,#f,e0,('if (t e) e1 k)) = (e,0,e1,k)
            cs @ cdr dup car  val @ immediate#t =
            IF car ELSE cadr THEN  expr !
            cdr dup car env !
            cdr cs ! exit
        THEN
        dup cont-contTo IF
            \ E(0,v,e0,(contTo k0 k)) = (0,v,0,k0)
            env 0!                      \ avoid memory leaks
            cs @ cadr  cs !  exit
        THEN
        dup cont-exit? IF exit THEN

        hex. ." unknown continuation" bye
    THEN
    dup symbol? IF \ E(s,0,e,k) = (0,e(s),0,k)
        expr @
        lookup @  val !
        noEval expr ! ." todo: not found"
        nil env !
    THEN
    dup atom? IF  \ E(v,0,e,k) = (0,v,0,k)
        expr @ val !
        noEval expr !
        nil env !
    THEN
    dup pair? IF            \ apply - eval function
        \ E((M.Ns),0,e,k) = (M,0,e,arg0(Ns,e,k))
        cs @ expr1 !  env @ expr0 !  cons expr1 !
        expr @ cdr expr0 ! cons expr1 !
        cont-args0 expr0 ! cons cs !
    THEN
    ." unknown expr" bye
;

: eval-loop ( -- )     \ trampoline for eval
    BEGIN eval-step cont-exit? UNTIL ;

: prim-read-char ;
: prim-read ( -- )
    \ (_,v,s,k)
;

: read
;

: print
;

: repl
    BEGIN
        \ global-env env !
        read expr !
        eval-loop
        print
    AGAIN
;

( === basic library === )

: lprim->closure ( xt -- lisp )
    \ [ closure% %size cells / 2 / ]l
    closure% %size cells/ 2/
    mem>alloc
    info-closure-lprim over closure>info !
    dup closure>formals 0!
    dup closure>env 0!
    over closure>body !
;

: vprim->closure ( xt -- lisp )
    \ [ closure% %size cells / 2 / ]l
    closure% %size cells/ 2/
    mem>alloc
    info-closure-vprim over closure>info !
    dup closure>formals 0!
    dup closure>env 0!
    dup closure>body !
;
S" is it needed?" type
: lambda->closure ( expr0:lbd expr1:env -- lisp )
    \ [ closure% %size cells / 2 / ]l
    closure% %size cells/ 2/
    mem>alloc
    info-closure-vprim over closure>info !
    expr1 @ over closure>env !
    expr0 @ cadr over closure>formals !
    expr0 @ cddr car over closure>body !
;

( ==== vau primitives === )
\ vprim: expr0 == Ns, expr1 == e1, cs == k

: vprim-vau
    \ E(_,_,e0,arg0(Ns,e1,k)) = (_,clo-vau((e2.x) e0 M),0,k) where Ns = (x e2 M)
    closure% %size mem>alloc
    info-closure-vprim  over closure>info !
    expr1 @ over closure>env !
    expr0 @ cddr car  over closure>body !
    val !
    expr0 @ car expr1 !  expr0 @ cadr expr0 ! cons
    val @ closure>formals !
;

( ==== lambda primitives === )
\ lprim: cs == k, expr1 == vs' == (v0 .. vn)

: lprim>arg1 expr1 @ cadr ;
: lprim>arg2 expr1 @ cddr car ;
: lprim>arg3 expr1 @ cddr cadr ;

' str= alias octet= ( d-txt0 d-txt1 -- t/f )

: equal-sym? ( l0 l1 -- t/f )
    2dup = IF true exit THEN
    over @ over @ 2dup <> IF 2drop false exit THEN
    \ dup info-symbol <> IF 2drop false exit THEN
    cell+ swap cell+
    over @ swap  dup @ octet=
;
: intern ( l0:val -- l1:val )
    interned @
    BEGIN dup pair? WHILE
            over over car equal-sym?
            IF car nip exit THEN
            cdr
    REPEAT
    expr0 ! interned @ expr0 ! cons interned !
;

\ NB: we don't need no education ... that's Pink Floyd
\ we don't need no explicit environment for define, because of:
\ (define define-env (vau (sym val e0) e1
\    (eval (list define sym (eval val e1)) (eval e0 e1)))
\ (qdefine ((vau (x) e x) quote) (vau (x) e x))
\ (qdefine (quote get-env) (vau x e e))
\ ... (qdefine (quote define) (vau (ps vals) e ...))
: lprim-qdefine
    \ (qdefine sym value)
    lprim>arg1 expr0 !
    lprim>arg2 expr1 !
    extend-env  immediate#inert val !
;
: lprim-qset!
    \ (qset! sym val)
    lprim>arg2 lprim>arg1 lookup dup IF ! exit THEN
    ." todo: handle not found" cr
;
: lprim-qdefined?
    \ (qdefined? sym)
    lprim>arg1 lookup
    IF immediate#t         \ found
    ELSE immediate#f
    THEN val !
;
: lprim-eval
    \ E(_,_,e0,args(((cs.#nil),e1,k)) = (M,0,N,k)
    \ (eval expr env)
    lprim>arg2 env !  lprim>arg1 expr ! exit ;

: lprim-contTo
    \ ( E(0,k0,e,args(((v lprim-contTo)),e1,k) = (0,v,0,k0) )
    expr1 @ cdr dup cadr cs !  car val !  ;
    \ old( E(0,k0,e,args(((lprim contTo)),e1,k) = (0,v,0,(contTo k0 k)) )
\    expr1 @ cadr val !
\    expr1 @ cddr car  cs !

: lprim-call/cc \ (call/cc f _k_) => (f (lambda (v _k0_) (contTo v _k_))
    \ E(0,fv,0,args((lprim call/cc).#nil,e1,k))
    \ = (0,(<lambda> v (env: k:=k), contTo v k),0,args((fv),0,k))
    \ vs' = (reverse (cons* v vs)), f = (cadr vs')
    \ (call/cc f) => (f <special lambda>)
    lprim>arg1 expr0 !  nil expr1 ! cons expr0 !
    cons val !
    nil expr0 !  cs @ expr1 !  cons expr1 !
    val @ expr0 !  cons cs !

    \ nil expr1 !  val @ expr0 ! cons expr0 ! cons val !
    \ nil expr0 !  cs @ cddr expr1 ! cons expr1 !
    \ val @ expr0 ! cons cs !

    \ (lambda (v) (contTo v k0))
    closure% %size cells/ 2/ mem>alloc
    info-closure-lambda  over closure>info !
    dup closure>formals 0!
    dup closure>env 0!
    dup closure>body 0! val !
    S" v" 2dup 2>r intern expr0 !  nil expr1 ! cons
    val @ closure>formals !
    nil env !
    S" k" 2dup 2>r intern expr0 !  cs @ cddr expr1 ! extend-env
    val @ closure>env !
    2r> intern expr0 !  nil expr1 !  cons expr1 !
    2r> intern expr0 !  cons expr1 !
    ['] lprim-contTo lprim->closure expr0 ! cons
    val @ closure>body !
;


: lprim-+-int ;
: lprim---int ;
: lprim-*-int ;
: lprim-/-int ;


\ Local Variables:
\ mode: forth
