/* gcc -m32 -S -fomit-frame-pointer -O6 -Os -fno-pic pre-vau.c */
/* gcc -m32 -O -ggdb3 pre-vau.c */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

typedef intptr_t int_t;         /* integer with same size as all pointer types */
#define WordSize (sizeof(int_t))
#define MemSize  (sizeof(int_t)*10000)

// cf. https://bernsteinbear.com/blog/small-objects/
typedef uintptr_t uword;
typedef intptr_t word;

struct Object;                  /* opaque */
typedef struct Object Object;
typedef Object * ObjectP;

struct infoVar;
typedef struct infoVar infoVar;

typedef struct {
  infoVar * info;
} memObj;

typedef struct {
  memObj h;
  ObjectP car, cdr;
} pair;

const int kBitsPerByte = 8;                        // bits
const int kWordSize = sizeof(word);                // bytes
const int kBitsPerWord = kWordSize * kBitsPerByte; // bits

/* Tagging scheme of ObjectP:
0bxxxx_xxxx_xxxx_xxxx_xxx1 :: ptr to memObj
0bxxxx_xxxx_xxxx_xxxx_xx00 :: int with (kBitsPerWord - kIntegerShift) bits
0bxxxx_xxxx_xxxx_xxxx_xx10 :: immediate value i.e.
0b0000_0000_0000_0000_0010 :: immediate #nil
0b0000_0000_0000_0000_0110 :: immediate #t
0b0000_0000_0000_0000_1010 :: immediate #f
0b0000_0000_0000_0000_1110 :: immediate #inert
0b0000_0000_0000_0001_0010 :: immediate #ignore
0bcccc_cccc_0000_0001_0110 :: immediate char

tagging of memObj:
0bxxxx_xxxx_xxxx_xxxx_xxx0 :: ptr to info
0bxxxx_xxxx_xxxx_xxxx_xxx1 :: ptr to info marked by garbage collector

 */
const unsigned int kIntegerTag = 0b00;
const unsigned int kIntegerTagMask = 0b11;
const unsigned int kIntegerShift = 2;
const unsigned int kIntegerBits = kBitsPerWord - kIntegerShift;
const word kIntegerMax = (((1LL << (kIntegerBits - 1)) - 1));
const word kIntegerMin = ((-(1LL << (kIntegerBits - 1))));

word Object_encode_integer(word value) {
  assert(value < kIntegerMax && "too big");
  assert(value > kIntegerMin && "too small");
  return value << kIntegerShift;
}

typedef int_t word;
bool object_is_int(Object* obj) {
  return ((uword)obj & kIntegerTagMask) == kIntegerTag;
}
word object_as_int(Object* obj) {
  assert(object_is_int(obj) && "Not an integer");
  return (word)obj >> kIntegerShift;
}

typedef struct {
  infoVar * info;
} memObj;

typedef struct {
  memObj m;
  ObjectP formals, env, body;
} ClosureLambda;

typedef struct {
  memObj m;
  void (*primitive)();
} ClosurePrim;


struct infoClosure;
struct infoVar;
typedef union VAL * OBJ;

typedef union VAL { // immediate value or heap/static OBJ
  int_t num;
  OBJ ptr;
  struct infoClosure * closure;
  struct infoVar * info;
  char cs[WordSize];
} VAL __attribute__ ((aligned (4)));

#define tagLen          (2)
#define tagMask         (0b11)
#define tagPtr          (0b00)
#define tagInfo         (0b01)
#define tagNum          (0b10)
#define tagConst        (0b11)


#define nil             ((VAL)(tagConst | tagMask + 0))
#define TRUE            ((VAL)(tagConst | tagMask + 1))
#define FALSE           ((VAL)(tagConst | tagMask + 2))


inline VAL unTag(VAL v)             { return (VAL)(~tagMask & v.num); }
inline VAL doTag(VAL v, int_t t)    { return (VAL)(t | v.num); }
// #define DoTag(v,t)      ((VAL) (t | v.num))
inline VAL injImm(int_t n, int_t t) { return (VAL)(((n)<<tagLen) | t) ; }
inline int_t projImm(VAL v)         { return ((v.num)>>tagLen) ; }
#define InjImm(n)                   ((VAL)(((n)<<tagLen) | tagNum))
#define ProjImm(v)                  ((v.num)>>tagLen)

inline bool numImmQ(VAL v) { return (v.num & tagNum); }
inline bool ptrQ(VAL v)    { return !(v.num & tagNum); }
inline bool infoQ(VAL v)   { return ptrQ(v) && (tagInfo == (unTag(v).ptr[0].num & tagMask)); }
inline bool numQ(VAL v)    { return tagNum == (v.num & tagMask); }
inline bool immQ(VAL v)    { return tagConst == (v.num & tagMask); }
inline bool pairQ(VAL v)   { return ptrQ(v) && !infoQ(v); }
inline bool atomQ(VAL v)   { return numImmQ(v) | infoQ(unTag(v).ptr[0]); }
inline bool nilQ(VAL v)    { return nil.num == v.num; }
inline bool valEQ(VAL v, VAL w)   { return v.num == w.num; }
inline void setVAL(OBJ v, VAL w)   { v->num = w.num; }

inline VAL car(VAL v) { return v.ptr[0]; }
inline VAL cdr(VAL v) { return v.ptr[1]; }

typedef void (*thunk_ft)();
typedef void (*scan_ft)(VAL);
typedef VAL (*evacuate_ft)(VAL);
typedef void (*print_ft)(VAL);
typedef bool (*equal_ft)(VAL,VAL);

typedef struct infoVar {
  struct infoConst * info;
  scan_ft scan;
  evacuate_ft evacuate;
  print_ft print;
  equal_ft equal;
} infoVar __attribute__ ((aligned (4)));

typedef struct infoConst {
  infoVar parent;
  int_t size;
} infoConst_t __attribute__ ((aligned (4)));

void symbol_print(VAL);
bool symbol_equal(VAL,VAL);

infoConst_t infoConst  = { &infoConst, 0, 0, 0, 0, sizeof(infoConst_t) };
infoConst_t infoVar    = { &infoConst, 0, 0, 0, 0, sizeof(infoVar) };
infoVar   infoSymbol = { &infoVar, 0, 0, &symbol_print, &symbol_equal};

typedef struct infoClosure {
  infoConst_t * info;
  VAL formals, env, body;
} closure_t __attribute__ ((aligned (4)));

void closure_print(VAL);
infoConst_t infoClosureVau    = { &infoConst, 0, 0, &closure_print, 0, sizeof(closure_t) };
infoConst_t infoClosureVprim  = { &infoConst, 0, 0, &closure_print, 0, sizeof(closure_t) };
infoConst_t infoClosureLambda = { &infoConst, 0, 0, &closure_print, 0, sizeof(closure_t) };
infoConst_t infoClosureLprim  = { &infoConst, 0, 0, &closure_print, 0, sizeof(closure_t) };

__attribute__((noinline)) OBJ alloc(int_t bytes);

/****************
 *** machine ****
 ****************/
typedef enum { state_eval, state_reverse, state_continuation,
               state_final } state_t;

state_t state;
VAL expr[3], env, pc = nil, mc = nil;
// E(state,expr,env,pc,mc)

void contWithPrompt();

inline void doLookup() {
  while(!nilQ(env))
    {
      expr[1] = car(env);         /* frame */
      expr[2] = cdr(expr[1]);       /* fals */
      expr[1] = car(expr[1]);       /* vars */
      while(pairQ(expr[1]))
        {
          if(valEQ(expr[0], car(expr[1])))
            {                   /* found */
              state = state_continuation;
              env.num = nil.num;
              expr[0] = car(expr[2]);
              return;
            }
          expr[2] = cdr(expr[2]);
          expr[1] = cdr(expr[1]);
        }
      if(valEQ(expr[0], expr[1]))
        {                       /* found argument tail */
          state = state_continuation;
          setVAL(&env, nil);       /* ??? */
          expr[0] = expr[2];
          return;
        }
      env = cdr(env);
    }
  contWithPrompt();             /* not found */
}

void contWithPrompt() {
  char msg[] ="Not yet implemented!";
  write(1,msg,sizeof(msg));
}

void evalStep() {
  switch(state)
    {
    case state_eval:
      {
        if(valEQ(nil, expr[0]))
          { // nil atom
            printf("TODO %s", __LINE__);
          }
        if((OBJ)&infoSymbol == expr[0].ptr)
          {
            // (state_eval,sym,env,pc,mc) -> (state_continuation,lookup(sym,env),0,pc,mc) if found
            doLookup();
            return;
          }
        else if(pairQ(expr[0]))
          {
            // (state_eval,(M.Ns),env,pc,mc) -> (state_eval,M,env,'(0 Ns env . pc),mc)
            OBJ new = alloc(3*2*WordSize);
            new[0] = injImm(0, tagNum);
            new[1].ptr = new + 2;
            new[2] = cdr(expr[0]);
            new[3].ptr = new + 4;
            new[4] = env;
            new[5] = pc;
            pc.ptr = new;
            expr[0] = car(expr[0]);
            return;
          }
        state = state_continuation;
      }
      // atom evaluates to itself. Therfore, we fall trough to the
      // partial continuation step(s).
    case state_continuation:
      if(pairQ(pc))
        {
          switch(pc.num)
            {
            case InjImm(0).num:
              if(&infoClosureVau == expr[0].closure->info)
                {
                  // (state_continuation,v,env0,'(0 Ns env1 . pc),mc) ->
                  // (state_eval,(clo>body a),(env-extend (new-frame
                  // (clo>formals a) (cons env1 Ns)) (clo>env a)),pc,mc) if (vau? v)
                  OBJ new = alloc(3*2*WordSize);
                  new[0] = car(cdr(cdr(pc)));
                  new[1] = car(cdr(pc));
                  new[2] = expr[0].closure->formals;
                  new[3].ptr = new;
                  new[4].ptr = &(new[2]);
                  new[5] = expr[0].closure->env;
                  env.ptr = &(new[4]);
                  expr[0] = expr[0].closure->body;
                  state = state_eval;
                  pc = cdr(cdr(cdr(pc)));
                  return;
                }
              else if(&infoClosureVprim == expr[0].closure->info)
                {
                  // (state_continuation,v,env0,'(0 Ns env1 . pc),mc) ->
                  // (clo>body a) if (vau-primitive? a)
                  state = state_eval;
                  ((void (*)())expr[0].closure->body.ptr)();
                  return;
                }
              else
                {
                  // lambda or lambda-primitive
                  // (state_continuation,v,env0,'(0 Ns env1 . pc),mc) ->
                  // (state_continuation,v,0,((().Ns) env1 . pc),mc)
                  env = nil;       /* ??? */
                  OBJ new = alloc(2*2*WordSize);
                  new[0] = nil;
                  new[1] = car(cdr(pc));
                  new[2].ptr = new;
                  new[3] = cdr(cdr(pc));
                  pc.ptr = new;
                  return;
                }
              return;
            case InjImm(1).num:
              if(pairQ(expr[1]))
                {
                  // (state_continuation,as,expr[1]=(v.vs),'(1 . pc),mc) ->
                  // (state_continuation,(v.as),expr[1]=vs,'(1 . pc),mc)
                  OBJ new = alloc(1*2*WordSize);
                  new[0] = car(expr[1]);
                  new[1] = expr[0];
                  expr[0].ptr = new;
                  expr[1] = cdr(expr[1]);
                  return;
                }
              else if(&infoClosureLambda == expr[0].closure->info)
                {
                  // (state_continuation,(a.as),(),'(1 . pc),mc) ->
                  // (state_eval,(clo>body a),
                  // (env-extend (new-frame (clo>formals a) as) (clo>env a)),
                  // pc,mc) if (lambda? a)
                  OBJ new = alloc(2*2*WordSize);
                  new[0] = car(expr[0]).closure->formals;
                  new[1] = cdr(expr[0]);
                  new[2].ptr = new;
                  new[3] = car(expr[0]).closure->env;
                  env.ptr = &(new[2]);
                  expr[0] = car(expr[0]).closure->body;
                  pc = cdr(pc);
                  state = state_eval;
                  return;
                }
              else
                {
                  assert(&infoClosureLprim == expr[0].closure->info);
                  // (state_continuation,(a.as),(),'(1 . pc),mc) ->
                  // (clo>body a) if (lambda-primitive? a)
                  pc = cdr(pc);
                  state = state_eval;
                  ((thunk_ft)expr[0].closure->body.ptr)();
                  return;
                }
            default:
              // (state_continuation,v,env0,'((vs.Ns) env1 . pc),mc)
              assert(pairQ(car(pc)));
              if(pairQ(cdr(car(pc))))
                {
                  // (state_continuation,v,env0,'((vs.N.Ns) env1 . pc),mc) ->
                  // (state_eval,N,env1,'(((v.vs).Ns) env1 . pc),mc)
                  OBJ new = alloc(3*2*WordSize);

                  new[0] = expr[0];
                  new[1] = car(car(pc));
                  new[2].ptr = new;
                  new[3] = cdr(cdr(car(pc)));
                  new[4].ptr = &(new[2]);
                  new[5] = cdr(pc);
                  expr[0] = car(cdr(car(pc)));
                  pc.ptr = new;
                  return;
                }
              else
                { // start reverse continuation
                  // (state_continuation,v,env0,'((vs.()) env1 . pc),mc) ->
                  // (state_reverse,(v),0,pc,mc) expr[1] = vs
                  assert(nilQ(cdr(car(pc))));
                  state = state_reverse;
                  OBJ new = alloc(2*WordSize);
                  new[0] = expr[0];
                  new[1] = nil;
                  expr[0].ptr = new;
                  expr[1] = car(car(pc));
                  pc = cdr(cdr(pc));
                  return;
                }
            }
        }

      /* pc is not a pair, i.e. it's nil */
      // (#f,v,env0,nil,mc) -> ...
      if(nilQ(mc))
        {
          // (#f,v,env0,nil,nil) -> done
        }
      else if(pairQ(car(mc)))
        {
          // (#f,v,env0,nil,(pc.mc)) -> (#f,v,env0,pc,mc)
          pc = car(mc);
          mc = cdr(mc);
          return;
        }
      else
        {
          // (#f,v,env0,nil,(prompt.mc)) -> (#f,v,env0,nil,mc)
          mc = cdr(mc);
          return;
        }
      break;
    case state_reverse:
      { // reverse continuation
        // (state_reverse,vs,env0,pc,mc), expr[1] = (v.vs') ->
        // (state_reverse,(v.vs),0,pc,mc), expr[1] = vs'
        while(!nilQ(expr[1]))
          {
            OBJ new = alloc(2*WordSize);
            new[0] = car(expr[1]);
            new[1] = expr[0];
            expr[1] = cdr(expr[1]);
          }
        return;
      }
    }
}

void evalLoop () {
  while(state_final != state) evalStep();
}

void evalPrim() {
  // (#f,(evalPrim exp env),(),'(1 . pc),mc) ->
  // (clo>body a) = (state_eval,exp,env,pc,mc)
  state = state_eval;
  env = car(cdr(cdr(expr[0])));
  expr[0] = car(cdr(expr[0]));
  pc = cdr(pc);
  evalLoop();
}
/****************
 *** parser *****
 ****************/

#define DefaultSymLen	 (32)

void parse() {
  // (#f,(a=parse.as),(),'(1 . pc),mc) -> (clo>body a) = (#f,result,(),pc,mc)
  pc = cdr(pc);
  int_t c;
  int_t balance = 0;
  expr[2].ptr = alloc(2*2*WordSize);  /* ??? */
  expr[2].ptr[0].ptr = &(expr[3]);    /* object to read */
  expr[2].ptr[1] = nil;           /* enclosing objects */
  expr[2].ptr[2] = nil;           /* car points to head */
  expr[2].ptr[3] = nil;           /* cdr points to tail */

 WHITESPACE:

  do
    c = getchar();
  while (c <= ' ');
  if(';' == c)
    {
      do
        c = getchar();
      while('\n' != c);
      goto WHITESPACE;
    }

  if('(' == c)
    {
      /* open par */
      balance ++;
      OBJ new = alloc(2*2*WordSize);
      new[0].ptr = &(new[2]);
      new[1] = expr[2];
      new[2] = nil;
      new[3] = nil;
      expr[2].ptr = new;
    }
  else if (')' == c)
    {
      /* close par */
      /* expr[2] = ((head.tail).enclosing) */
      balance --;
      if(nilQ(cdr(expr[2])))
        {
          expr[0] = car(car(expr[2]));
          return;
        }
      OBJ append = alloc(2*WordSize);
      append[0] = car(car(expr[2]));
      append[1] = nil;
      cdr(car(cdr(expr[2]))).ptr[1].ptr = append;
      car(cdr(expr[2])).ptr[1].ptr = append;
      expr[2] = cdr(expr[2]);
    }
  else
    {
      /* read symbol */
      OBJ new = alloc(2*WordSize+DefaultSymLen);
      char * buf = (char *) &(new[2]);
      int_t size = 1;
      int_t maxSize = DefaultSymLen;
      new[0].ptr = (OBJ) & infoSymbol;
      do
        {
          *buf++ = c; size++;
          c = getchar();
          switch(c)
            {
            case '(':
            case ')':
            case ';':
              goto LOOP_END;
            default:
              if(size >= maxSize)
                {
                  expr[1].ptr = new;
                  OBJ renew = alloc(2*WordSize+DefaultSymLen+maxSize);
                  new = expr[1].ptr;
                  memcpy(renew, new, 2*WordSize+maxSize);
                  maxSize += DefaultSymLen;
                  new = renew;
                }
              *buf++ = c; size++;
            }
        }
      while(1);

    LOOP_END:

      new[1].num = size;
      OBJ append = alloc(2*WordSize);
      append[0].ptr = new;
      append[1] = nil;
      if(nilQ(car(car(expr[2]))))
        {
          /* first element in list */
          car(expr[2]).ptr[0].ptr = append;
          car(expr[2]).ptr[1].ptr = append;
        }
      else
        {
          /* expr[2] = ((head.tail).enclosing) */
          cdr(car(expr[2])).ptr[0].ptr = append;
          car(expr[2]).ptr[1].ptr = append;
        }
      if(nilQ(cdr(expr[2])))
        {
          /* symbol only */
          assert(0 == balance);
          expr[0].ptr = new;
          return;
        }
    }
}

#define fdStdOut (1)
void printObject(VAL o);
void printList(VAL l) {
  char c;
  bool first=true;
  c='(';
  write(fdStdOut, &c, 1);
  c=' ';
  while(pairQ(l))
    {
      if(!first)
        write(fdStdOut, &c, 1);
      else
        first = false;
      printObject(car(l));
      l = cdr(l);
    }
  if(!nilQ(l))
    {
#define LEN  (3)
      char s[LEN] = " . ";
      write(fdStdOut, s, LEN);
      printObject(l);
    }
  c=')';
  write(fdStdOut, &c, 1);
}

void printObject(VAL o) {
  if(pairQ(o))
    printList(o);
  else if(infoQ(o))
    (unTag(o.ptr[0]).info->print)(o);
  else if(numQ(o))
    printf("%d", projImm(o));
  else if(nilQ(o))
    printf("()");
  else
    printf("TODO");
}
void print() {
  // (#f,(print exp),(),'(1 . pc),mc) ->
  // (clo>body a) = (#f,(),(),pc,mc)
  pc = cdr(pc);
  printObject(car(cdr(expr[0])));
  expr[0] = nil;
  env = nil;
}

void print2(){
  char sep[3];
  expr[1] = nil;
  bool done = false;

  do {
    if(pairQ(expr[0]))
      {
        // push to expr[1]:
        OBJ new = alloc(2*WordSize);
        new[0] = cdr(expr[0]);
        new[1] = expr[1];
        expr[1].ptr = new;
        sep[0] = '(';
        write(fdStdOut, &sep, 1);
        expr[0] = car(expr[0]);
      }
    else
      {
        // print atom:
        if(nilQ(expr[0]))
          {
            sep[0] = '('; sep[1] = ')';
            write(fdStdOut, &sep, 2);
          }
        else if(infoQ(expr[0]))
          {
            (unTag(expr[0].ptr[0]).info->print)(expr[0]);
          }
        else                    /* todo: add tagConst */
          {
            assert(numQ(expr[0]));
            printf("%i", ProjImm(expr[0]));
          }
        // pop work:

        while(pairQ(expr[1]))
          { // pop work:
            expr[0] = car(expr[1]);
            expr[1] = cdr(expr[1]);

            if(nilQ(expr[0]))
              { // end of current list
                sep[0] = ')';
                write(fdStdOut, &sep, 1);
              }
            else
              {
                OBJ new = alloc(2*WordSize);
                int_t len;
                if(pairQ(expr[0]))
                  { // list continues
                    sep[0] = ' ';
                    len = 1;
                    new[0] = cdr(expr[0]);
                    expr[0] = car(expr[0]);
                  }
                else
                  { // final atom
                    sep[0] = sep[2] = ' '; sep[1] = '.';
                    len = 3;
                    new[0] = nil;
                  }
                new[1] = expr[1];
                expr[1].ptr = new;
                write(fdStdOut, &sep, len);
                break;
              }
          }
        done = nilQ(expr[1]);
      }
  } while(!done);
}


void print3(){
  char sep[3];
  expr[1] = nil;

 doObj:
  if(pairQ(expr[0]))
    {
      OBJ new = alloc(2*WordSize);
      new[0] = cdr(expr[0]);
      new[1] = expr[1];
      expr[1].ptr = new;
      sep[0] = '(';
      write(fdStdOut, sep, 1);
      expr[0] = car(expr[0]);
      goto doObj;
    }
  else
    { // print atom:
      if(nilQ(expr[0]))
        {
          sep[0] = '('; sep[1]=')';
          write(fdStdOut, sep, 2);
        }
      else if(infoQ(expr[0]))
        {
          unTag(expr[0].ptr[0]).info->print(expr[0]);
        }
      else
        {                       /* todo: add tagConst */
          assert(numQ(expr[0]));
          printf("%i", ProjImm(expr[0]));
        }

      // pop work:
    pop:
      if(pairQ(expr[1]))
        {
          expr[0] = car(expr[1]);
          expr[1] = cdr(expr[1]);

          if(nilQ(expr[0]))
            { // end of current list
              sep[0] = ')';
              write(fdStdOut, &sep, 1);
              goto pop;
            }
          // prepare next object in current list:
          assert(!nilQ(expr[0]));
          OBJ new = alloc(2*WordSize);
          int_t len;
          if(pairQ(expr[0]))
            { // list continues
              sep[0] = ' ';
              len = 1;
              new[0] = cdr(expr[0]);
              expr[0] = car(expr[0]);
            }
          else
            { // final atom
              sep[0] = sep[2] = ' '; sep[1] = '.';
              len = 3;
              new[0] = nil;
            }
          new[1] = expr[1];
          expr[1].ptr = new;
          write(fdStdOut, &sep, len);
          goto doObj;
        }
    }
}

void code_vau();

void vauORlambda(infoConst_t * info) {
  // (state_continuation,v,env0,'(0 Ns env1 . pc),mc) ->
  // (clo>body a) and NS = (args body)
  closure_t * clo = (closure_t *)alloc(sizeof(closure_t)/WordSize);
  clo->info = (infoConst_t *)doTag((VAL)(infoVar *)info, tagInfo).info;
  clo->formals = car(car(cdr(pc)));
  clo->env = car(cdr(cdr(pc)));
  clo->body = car(cdr(car(cdr(pc))));
  expr[0] = (VAL) clo;
  pc = cdr(cdr(cdr(pc)));
  env = nil;
}
void lambda() {
  // (state_continuation,v,env0,'(0 Ns env1 . pc),mc) ->
  // (clo>body a) and NS = (args body)
  vauORlambda(&infoClosureLambda);
}
void vau() {
  // (state_continuation,v,env0,'(0 Ns env1 . pc),mc) ->
  // (clo>body a) and NS = (args body)
  vauORlambda(&infoClosureVau);
}

#define doTag2(type,tag,val) ((type)(tag | (int_t)val))

 VAL intern_f[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("f") - 1 },
     { .cs = "fZZZ"}} ;
 VAL intern__[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("_") - 1 },
     { .cs = "_ZZZ"}} ;
 VAL intern_current_env[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("current-env") - 1 },
     { .cs = "curr"},
     { .cs = "ent-"},
     { .cs = "env"}} ;
 VAL intern_vau[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("vau") - 1 },
     { .cs = "vauZ"}} ;
 VAL intern_lambda[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("lambda") - 1 },
     { .cs = "lamb"},
     { .cs = "daZZ"}} ;
 VAL intern_print[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("print") - 1 },
     { .cs = "prin"},
     { .cs = "tZZZ"}};
 VAL intern_eval[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("eval") - 1 },
     { .cs = "eval"}};
 VAL intern_read[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("read") - 1 },
     { .cs = "read"}};
 VAL intern_code_vau[] =
   { { .info = (infoVar *)(((char*)& infoSymbol)+tagInfo) },
     { sizeof("code-vau") - 1 },
     { .cs = "code"},
     { .cs = "-vau"}};
 VAL intern[] = { { .ptr = intern_print},
                  { .ptr = intern+2 },
                  { .ptr = intern_read},
                  { .ptr = intern+4 },
                  { .ptr = intern_eval},
                  { .ptr = intern+6 },
                  { .ptr = intern_lambda},
                  { .ptr = intern+8 },
                  { .ptr = intern_vau},
                  { .ptr = intern+10 },
                  { .ptr = intern_current_env},
                  { .ptr = intern+12 },
                  { .ptr = intern_f},
                  { .ptr = intern+14 },
                  { .ptr = intern__},
                  { .ptr = intern+16 },
                  { .ptr = intern_code_vau},
                  nil};
VAL interned = (VAL)intern;
closure_t clo_print        = { (infoConst_t *)(((char*) &infoClosureLprim)+tagInfo), nil, nil, (VAL)(OBJ)&print};
 closure_t clo_eval         = { (infoConst_t *)(((char*) &infoClosureLprim)+tagInfo), nil, nil, (VAL)(OBJ)&evalPrim};
 closure_t clo_read         = { (infoConst_t *)(((char*) &infoClosureLprim)+tagInfo), nil, nil, (VAL)(OBJ)&read};
 closure_t clo_lambda       = { (infoConst_t *)(((char*) &infoClosureVprim)+tagInfo), nil, nil, (VAL)(OBJ)&lambda};
 closure_t clo_vau       = { (infoConst_t *)(((char*) &infoClosureVprim)+tagInfo), nil, nil, (VAL)(OBJ)&vau};
 closure_t clo_code_vau       = { (infoConst_t *)(((char*) &infoClosureVprim)+tagInfo), nil, nil, (VAL)(OBJ)&code_vau};

VAL global_vars[] = { { .ptr = intern_read},
                      { .ptr = global_vars+2},
                      { .ptr = intern_eval},
                      { .ptr = global_vars+4},
                      { .ptr = intern_print},
                      { .ptr = global_vars+6},
                      { .ptr = intern_lambda},
                      { .ptr = global_vars+8},
                      { .ptr = intern_vau},
                      nil};
VAL global_vals[] = { { .closure = &clo_read},
                      { .ptr = global_vars+2},
                      { .closure = &clo_eval},
                      { .ptr = global_vars+4},
                      { .closure = &clo_print},
                      { .ptr = global_vars+6},
                      { .closure = &clo_lambda},
                      { .ptr = global_vars+8},
                      { .closure = &clo_vau},
                      nil};
VAL global_frame[] = { { .ptr = global_vars},
                       { .ptr = global_vals} };
VAL global_env[]   = { { .ptr =  global_frame},
                       nil};
/*
  read-eval-print-loop in scheme:
  ((lambda (f env) (f f f env))
  (lambda (f _ env) (f f (print (eval (read) env)) env))
  ((vau _ e e)))
*/
VAL iStartRecFormals[] =
  { { .ptr = intern_f},
    { .ptr = iStartRecFormals + 2},
    { .ptr = intern__},         /* env */
    nil};
VAL iStartRecBody[] =
  { { .ptr = intern_f},
    { .ptr = iStartRecFormals + 2},
    { .ptr = intern_f},
    { .ptr = iStartRecFormals + 4},
    { .ptr = intern_f},
    { .ptr = iStartRecFormals + 6},
    { .ptr = intern__},
    nil};
VAL iStartRec[] =
  { { .ptr = intern_lambda},
    { .ptr = iStartRec + 2},
    { .ptr = iStartRecFormals},
    { .ptr = iStartRec + 4},
    { .ptr = iStartRecBody},
    nil};

VAL iREPread[] =
  { { .ptr = intern_read},
    { .ptr = iREPread + 2},
    nil};
VAL iREPeval[] =
  { { .ptr = intern_eval},
    { .ptr = iREPeval + 2},
    { .ptr = iREPread},
    { .ptr = iREPeval + 4},
    { .ptr = intern_vau},       /* env */
    { .ptr = iREPeval + 6},
    nil};
VAL iREPprint[] =
  { { .ptr = intern_print},
    { .ptr = iREPprint + 2},
    { .ptr = iREPeval},
    { .ptr = iREPprint + 4},
    nil};
VAL iREPbody[] =
  { { .ptr = intern_f},
    { .ptr = iREPbody + 2},
    { .ptr = intern_f},
    { .ptr = iREPbody + 4},
    { .ptr = iREPprint},
    { .ptr = iREPbody + 6},
    { .ptr = intern_vau},
    nil};
VAL iREPformals[] =
  { { .ptr = intern_f},
    { .ptr = iREPformals + 2},
    { .ptr = intern__},
    { .ptr = iREPformals + 4},
    { .ptr = intern_vau},       /* is env */
    { .ptr = iREPformals + 6},
    nil};
VAL iREP[] =
  { { .ptr = intern_lambda},
    { .ptr = iREP + 2},
    { .ptr = iREPformals},
    { .ptr = iREP + 4},
    { .ptr = iREPbody},
    nil};

VAL iEnvVau[] =                 /* (vau _ e e) with f instead of e */
  { { .ptr = intern_vau},
    { .ptr = iEnvVau + 2},
    { .ptr = intern__},
    { .ptr = iEnvVau + 4},
    { .ptr = intern_f},
    { .ptr = iEnvVau + 6},
    { .ptr = intern_f},
    { .ptr = iEnvVau + 8},
    nil};
VAL iEnv[] =
  { { .ptr = iEnvVau},
    nil};
VAL initialExpr[] =
  { { .ptr = iStartRec }, // 0 -> (lambda (f _) (f f f _))
    { .ptr = initialExpr + 16 }, // TODO: remove next pair
    { .ptr = initialExpr + 16 }, // 2 -> (lambda (f _) (f f (print (eval (read) (current-env)))))
    nil};
VAL expr[] = { { .ptr = initialExpr}};

void main () {
   print3();
   evalLoop();
 }

 /****************
  *** memory *****
  ****************/

 char buffer0[MemSize], buffer1[MemSize];
 void * to = buffer0;
 void * from = buffer1;
 void * toMax = buffer0 + MemSize;
 void * freeMem = buffer0;
 void * scan;
 /*void * to, * toMax, * freeMem, * from, * scan;*/


VAL evacuate(VAL o) {
  if((to <= (void *)o.ptr) && ((void *)o.ptr < toMax)) return o; /* already in to space */

   if(tagInfo == (o.num & tagMask))
     ((o.info))->evacuate(o);
   else
     {
       ((OBJ)freeMem)[0].num = o.ptr[0].num;
       ((OBJ)freeMem)[1].num = o.ptr[1].num;
       OBJ res = freeMem;
       freeMem += 2*WordSize;
       return (VAL)(OBJ)freeMem;
     }
 }

 inline void scan_cxr(VAL o) {
   o = evacuate(o);
 }

 void scanObj(VAL o) {
   if(tagInfo == (o.num & tagMask))
     ((infoVar *)(o.ptr))->scan(o);
   else
     {
       scan_cxr(o);
       scan_cxr(o.ptr[1]);
       scan += 2*WordSize;
    }
}

void doGC() {
  // switch memory regions
  freeMem = from;
  from = to;
  to = freeMem;
  toMax = to + MemSize;
  // scan roots
  interned = evacuate(interned);
  for(int_t i=0; i < sizeof(expr)/sizeof(expr[0]); i++)
    expr[i] = evacuate(expr[i]);
  env  = evacuate(env);
  pc   = evacuate(pc);
  mc   = evacuate(mc);
  // scan evacuated objects
  while(scan<freeMem)
    scanObj((VAL)(OBJ)scan);
}

void oom() {
  exit(111);
}

inline bool hasSpace(int_t bytes) {
  return (freeMem + bytes) < toMax;
}

__attribute__((noinline)) OBJ alloc(int_t bytes) {
  if(!hasSpace(bytes))
    {
      doGC();
      if(!hasSpace(bytes))
        oom();
    }
  void * res = freeMem;
  freeMem += bytes;
  return res;
}

void symbol_print(VAL o) {
  write(fdStdOut, &(o.ptr[2].cs), o.ptr[1].num);
}

bool symbol_equal(VAL a, VAL b) {
  if(a.ptr[1].num != b.ptr[1].num) return false;
  return !memcmp(&(a.ptr[2].cs), &(b.ptr[2].cs), a.ptr[1].num);
}

void closure_print(VAL v) { // todo: formals and body
  int_t info = v.ptr[0].num;
  if(info == (int_t)(tagInfo + &infoClosureVau)) {
    printf("<vau-closure:");
    printf(">");
  } else if(info == (int_t)(tagInfo + &infoClosureLambda)) {
    printf("<lambda-closure:");
    printf(">");
  } else if(info == (int_t)(tagInfo + &infoClosureVprim))
    printf("<vau-primitive at %x>", v.ptr[3].num);
  else if(info == (int_t)(tagInfo + &infoClosureLprim))
    printf("<lambda-primitive at %x>", v.ptr[3].num);
  else
    printf("<unknown closure>");
}

void code_vau() {
  print("NYI\n");
}

/*

  ((lambda (def-lam env)
     ((lambda rest '())
       (def-lam ((vau (q) _ q) quote) env (vau (q) _ q))
       (def-lam (quote begin) env begin)
       (def-lam (quote define) env
         (vau (name value) e0
            (def-lam name e0 (eval value e0))))
       ))
   (code-vau ...) ; def-lam
   ((vau _ e e)))  ; current-env


   (define begin (lambda bodys
      (if (null? bodys)
          (error ...)
          ((lambda (last) (last last (car bodys) (cdr bodys)))
           (lambda (last fst rest)
             (if (null? rest)
                 fst
               (last last  (car rest) (cdr rest))))))))
*/
