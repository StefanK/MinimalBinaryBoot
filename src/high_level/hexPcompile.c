/****************************************
   Copyright (C) 2023 Dr. Stefan Karrmann
   Licence: GPL-3
*****************************************/
/* gcc -m32 -S -fomit-frame-pointer -O6 -Os -fno-pic hexPcompile.c */

#include <stdio.h>
#include <stdbool.h>

#define NibbleShift (4)

void main() {
  bool line_comment = false;
  char c;
  int val = 1;

  while(true)
    {
      c = getchar();

      if(EOF == c) return;

      if(line_comment)
        {
          if('\n' == c)
            {
              line_comment = false;
            }
        }
      else
        {
          if('Z' == c) return;
          if(c>'P')
            {
              line_comment = 1;
            }
          else if(c >= 'A')
            {
              val = (val << NibbleShift) | (c - 'A');
              if(val>0xFF)
                {
                  putchar(val);
                  val = 1;
                }
            }
        }
    }
}
